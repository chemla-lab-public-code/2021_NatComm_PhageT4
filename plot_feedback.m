function trapData = plot_feedback(trapData)
% trapData = plot_feedback(trapData)

f = figure();
set(f,'Name',['Fb_' trapData.file(1:end-4)]);


f1 = subplot(3,1,1);
plot(trapData.time, trapData.TrapSep)
legend('TrapSep')
title(['Feedback\_'  trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' Trap Separation(nm) vs Time(s)']);
ylim([min(trapData.TrapSep)-50 max(trapData.TrapSep)+50])


f2 = subplot(3,1,2);
plot(trapData.time, trapData.DNAext_X)
hold on
plot(trapData.time, trapData.DNAext_Y)
legend('DNAext_X','DNAext_Y')
title(['Feedback\_'  trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' DNA Extension(nm) vs Time(s)']);
if trapData.scandir == 1
ylim([min(trapData.DNAext_X)-50 max(trapData.DNAext_X)+50])
elseif trapData.scandir == 0
ylim([min(trapData.DNAext_Y)-50 max(trapData.DNAext_Y)+50]) 
end

f3 = subplot(3,1,3);
plot(trapData.time, (trapData.force_AX+trapData.force_BX)/2)
hold on
plot(trapData.time, (trapData.force_AY+trapData.force_BY)/2)
legend('force_X','force_Y')
title(['Feedback\_'  trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' Force(pN) vs Time(s)']);
% ylim([min(trapData.DNAext_Y)-0.5 max(trapData.DNAext_Y)+0.5]) 

linkaxes([f1,f2,f3],'x')

end