function trapData = plot_force_extension(trapData, construct, fitrange)

%% load construct parameters
switch construct
    case 'T4'
        hairpin = 0;
        dsDNA1 = 4319;
        ssDNA1 = 0;
        fitoffset = 1;
        fake_offset = 0;
        stdF = [2 15];
    case 'PCR+Linker'
        hairpin = 0;
        dsDNA1 = 3400+906; % ispace and the protein arm is not taken into account.
        ssDNA1 = 0;
        fitoffset = 1;
        fake_offset = 0;
        stdF = [2 15];
    case 'PCR'
        hairpin = 0;
        dsDNA1 = 3.4e3;
        ssDNA1 = 0;
        fitoffset = 1;
        fake_offset = 0;
        stdF = [2 15];
    case '1575'
        hairpin = 0;
        dsDNA1 = 1575;
        ssDNA1 = 0;
        fitoffset = 1;
        fake_offset = 0;
        stdF = [2 15];
    case '10kb' 
        hairpin = 0;
        dsDNA1 = 10e3;
        ssDNA1 = 0;
        fitoffset = 1;
        fake_offset = 0;
        stdF = [2 15];
    case 'WLC'
        hairpin = 0;
        dsDNA1 = 9995;
        ssDNA1 = 0;
        fitoffset = 1;
        fake_offset = 0;
        stdF = [2 15];
        n = 2;
    case 'ssDNA'
        hairpin = 0;
        dsDNA1 = 0;
        ssDNA1 = 3.4e3;
        fitoffset = 1;
        fake_offset = 0;
        stdF = [2 15];
end

%% create the WLC model
max_force = max([trapData.force_AX, trapData.force_AY, trapData.force_BX, trapData.force_BY]);
F = 0:0.5:max_force;
[WLC_param] = WLC_parameters;
switch construct
    case {'T4','PCR','1575','PCR+Linker'}
        bp = 1*WLC_param.hds*XWLCContour(F,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext = dsDNA1.*bp;
        bp2 = 1*WLC_param.hds*XWLCContour(F/2,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext2 = dsDNA1.*bp2;
        bp3 = 1*WLC_param.hds*XWLCContour(F/3,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext3 = dsDNA1.*bp3;
        bp4 = 1*WLC_param.hds*XWLCContour(F/4,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext4 = dsDNA1.*bp4;
        ntss = 1*WLC_param.hss*XWLCContour(F/2,WLC_param.Pss,WLC_param.Sss,WLC_param.kT); % nm/nt
        WLC_extss = dsDNA1.*ntss;
     case '10kb'
        bp = 1*WLC_param.hds*XWLCContour(F,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext = dsDNA1.*bp;
        bp2 = 1*WLC_param.hds*XWLCContour(F/2,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext2 = dsDNA1.*bp2;
        bp3 = 1*WLC_param.hds*XWLCContour(F/3,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext3 = dsDNA1.*bp3;
        bp4 = 1*WLC_param.hds*XWLCContour(F/4,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext4 = dsDNA1.*bp4;
        ntss = 1*WLC_param.hss*XWLCContour(F/2,WLC_param.Pss,WLC_param.Sss,WLC_param.kT); % nm/nt
        WLC_extss = dsDNA1.*ntss;
    case 'WLC'
        bp = 1*WLC_param.hds*XWLCContour(F,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext = dsDNA1.*bp;
        bp2 = 1*WLC_param.hds*XWLCContour(F/2,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext2 = dsDNA1.*bp2;
        bp3 = 1*WLC_param.hds*XWLCContour(F/3,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext3 = dsDNA1.*bp3;
        bp4 = 1*WLC_param.hds*XWLCContour(F/4,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext4 = dsDNA1.*bp4;
        ntss = 1*WLC_param.hss*XWLCContour(F/2,WLC_param.Pss,WLC_param.Sss,WLC_param.kT); % nm/nt
        WLC_extss = dsDNA1.*ntss;
    case 'ssDNA' % to be done
        bp = 1*WLC_param.hds*XWLCContour(F/2,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        nt = 1*WLC_param.hss*XWLCContour(F/2,WLC_param.Pss,WLC_param.Sss,WLC_param.kT); % nm/nt
        WLC_ext = dsDNA1.*bp;
end


%% plot the data
% Filter TrapSep
% TrapSepFilt  = movmean(trapData.TrapSep,30);
% The 2 rows above is replaced with the following home-made code because
% function movmean not exist in my matlab

l = length(trapData.TrapSep);
TrapSepFilt=zeros(1,l);
gauge = 14;
for ii = 1:l
    windowsize = length(max(1,ii-gauge):min(l,ii+gauge));
    for jj = max(1,ii-gauge):min(l,ii+gauge)
        TrapSepFilt(ii) = TrapSepFilt(ii) + trapData.TrapSep(jj);
    end
    TrapSepFilt(ii)=TrapSepFilt(ii)/windowsize;
end

% remove jumps in trap position
TrapSepFilt_diff = diff(TrapSepFilt);
normalized_diff = diff(TrapSepFilt)/mean(TrapSepFilt_diff(TrapSepFilt_diff>0));
TrapSepFiltClean = TrapSepFilt(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.force_AX = trapData.force_AX(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.force_BX = trapData.force_BX(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.force_AY = trapData.force_AY(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.force_BY = trapData.force_BY(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.DNAext_X = trapData.DNAext_X(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.DNAext_Y = trapData.DNAext_Y(normalized_diff > 0.8 | normalized_diff < -0.8);
% find the top peaks
[numMAX, indMAX] = findpeaks(TrapSepFiltClean);
% find the bottom peaks
TrapSepInverse = [-max(numMAX) -TrapSepFiltClean -max(numMAX)];
[numMIN, indMIN] = findpeaks(TrapSepInverse);
indMIN = indMIN-1;
% count number of raster scans
rasterNUM = (max([length(indMIN) length(indMAX)]))/2;
% get the force array
if trapData.scandir == 1
    FA = trapData.force_AX;
    FB = trapData.force_BX;
    FM = (trapData.force_AX+trapData.force_BX)./2;
    DNAext = trapData.DNAext_X;
elseif trapData.scandir == 0
    FA = trapData.force_AY;
    FB = trapData.force_BY;
    FM = (trapData.force_AY+trapData.force_BY)./2;
    DNAext = trapData.DNAext_Y;
end

% get fake offset
if ~isnan(fitrange)
    DNAext_offset = [];
    DNAext_offset = mean(DNAext(FM > fitrange(1) & FM < fitrange(2)));
    WLC_offset = mean(WLC_ext(F > fitrange(1) & F < fitrange(2)));
    fake_offset = DNAext_offset - WLC_offset;
    % substract the fake offset
%     DNAext = DNAext-(0);
    DNAext = DNAext-fake_offset;
end
trapData.fake_offset = fake_offset;
trapData.fitrange = fitrange;
trapData.stdF = stdF;

%% plot
figure
plot(WLC_ext, F, '--r', 'LineWidth', 2)
% switch construct
%     case 'T4'
%         hold on
%         plot(WLC_ext2+34, F, '--b', 'LineWidth', 0.5)
%         hold on
%         plot(WLC_ext3+58, F, '--b', 'LineWidth', 0.5)
%         hold on
%         plot(WLC_ext4+79, F, '--b', 'LineWidth', 0.5)
%         hold on
%         plot(WLC_extss + 447.3, F, '--m', 'LineWidth', 0.5)
%     case 'WLC'
%         hold on
%         plot(WLC_ext2, F, '--b', 'LineWidth', 0.5)
%         hold on
%         plot(WLC_ext3, F, '--b', 'LineWidth', 0.5)
%         hold on
%         plot(WLC_ext4, F, '--b', 'LineWidth', 0.5)
%         hold on
%         plot(WLC_extss, F, '--m', 'LineWidth', 0.5)
% end

for ii = 1:1:rasterNUM
    indxS = indMIN(ii); % far point
    indxE = indMAX(ii); % close point
    indxEE = indMIN(ii+1);
    hold on
    plot(DNAext(indxS:indxE),FM(indxS:indxE),'color', [0.9100    0.4100    0.1700], 'LineWidth', 1.5)
    hold on
    plot(DNAext(indxE:indxEE),FM(indxE:indxEE), 'color', [0 0.4470 0.7410], 'LineWidth', 1.5)
end
if ~isnan(DNAext)
    xlim([min(DNAext)-50 max(DNAext)+50])
end
ylim([min(0,min(FM)) max(FM)*1.2])
xlabel('DNA extension (nm)')
ylabel('Force (pN)')

switch construct
    case {'T4','PCR','1575','PCR+Linker'}
%         legend('WLC model - single','WLC model - double+34','WLC model - triple+58','WLC model - quadra+79','WLC model - ssDNA+447.3','pull','restore','Location','NorthWest')
            legend('WLC model','pull','restore','Location','NorthWest')
    case '10kb'
        legend('WLC model','pull','restore','Location','NorthWest')
    case 'WLC'
        legend('WLC model - single','WLC model - double','WLC model - triple','WLC model - quadra','WLC model - ssDNA','pull','restore','Location','NorthWest')
    otherwise
    legend('WLC model - single tether','pull','restore','Location','NorthWest')
end

if trapData.oldtrap == 1
    instrument = 'Old-Trap';
elseif trapData.oldtrap == 0
    instrument = 'Fleezers';
end
% title({[ construct ' construct, ' instrument] ['data file: ' trapData.file(1:end-4)]})
title([ 'construct: ' construct ', ' instrument ', fext: ' trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ', fakeoffset: ' num2str(fake_offset) 'nm, cal: ' num2str(trapData.calnumber,'%03d')])
set(gcf, 'Name',[ 'Fext_' trapData.file(1:end-4)])

end