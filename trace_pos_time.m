function trapData = trace_pos_time(filenum,calnum,directory,Date)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

trapData = load_trapData(directory,Date,[2000 2000 200 10000 5000 300],filenum,calnum,nan);
figure()
set(gcf, 'Name',trapData.file(1:end-4))
subplot(3,1,1:2)
plot(trapData.time,(trapData.force_AX+trapData.force_BX)/2,trapData.time,(trapData.force_AY+trapData.force_BY)/2);
legend('X','Y')
subplot(3,1,3)
plot(trapData.time,trapData.trappos1,trapData.time,trapData.trappos2);
legend('trappos1','trappos2')


end

