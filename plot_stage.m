function AxeHandle = plot_stage(AxeHandle,trapData)

directory = trapData.path(1:end-7);
Date = str2num(trapData.file(1:6));
stgTimeFilenum = str2num(trapData.file(8:10));
ifstage = exist([trapData.path trapData.file(1:end-4) '_stagetime.txt'],'file');

if ifstage
    [stgTime, stgPos] = StageMoveTime2(directory, Date, stgTimeFilenum);
end


% Plot vertical lines
plot([stgTime(:),stgTime(:)],[AxeHandle.YLim(1),AxeHandle.YLim(2)], 'Color', [.6,.6,.6]); 

