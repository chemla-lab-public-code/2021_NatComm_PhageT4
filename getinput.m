function info = getinput(str)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
global datadirectory
global calparams

prompts = {'datadirectory:','Date: (yymmdd)','Filenumber1 (001): ','Calnumber2 (001)','Offsetnumber2 (001)',...
            'Bead A diameter (nm)', 'Bead B diameter (nm)', 'Averaging window',...
            'f_{XY High} (Hz)', 'f_{Sum High} (Hz)', 'f_{Low} (Hz)'};
defaults = {datadirectory,'180521','25','22','23'...
            num2str(calparams(1)),num2str(calparams(2)),num2str(calparams(3)),...
            num2str(calparams(4)),num2str(calparams(5)),num2str(calparams(6))};

field={'path','day','file','cal','offset'...
        'params1','params2','params3',...
        'params4','params5','params6'};
options.Resize = 'on';
options.Interpreter = 'tex';

cells = inputdlg(prompts,str,[1 60],defaults,options);

info=cell2struct(cells,field);
end

% function [numbers]=getnumbers(title,prompts,defaults)
% 
% field={'a'};
% options.Resize = 'on';
% options.Interpreter = 'tex';
% cells=inputdlg(prompts,title,[1 40],defaults,options);
% 
% N=max(size(prompts));
% for i=1:N	
%    info=cell2struct(cells(i),field);
% 	numbers(i)=str2num(info.a);
% end