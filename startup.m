blue = [0 0.4470 0.7410];
red = [0.8500 0.3250 0.0980];
yellow = [0.9290 0.6940 0.1250];
global datadirectory
datadirectory = '/Users/WanderingCorn/Experiment Diary/Old Trap/';
% datadirectory = '/Users/WanderingCorn/Experiment Diary/Fleezers/';
Fleezers = '/Users/WanderingCorn/Experiment Diary/Fleezers/';
OT = '/Users/WanderingCorn/Experiment Diary/Old Trap/';
SamsungOT = '/Volumes/SUOANG/Old Trap/';
SamsungFL = '/Volumes/SUOANG/Fleezers/';
global calparams
              %beadA  beadB  avwin   fxyhi  fsumhi   flo    
% calparams = [900    880     100   40000  10000    1000    ];
% calparams = [900    880     200   8000  8000    600    ];
calparams = [845    818     200   40000  10000    1000    ];
cal800 = [845    818     200   40000  10000    1000    ];
cal2000 = [2000 2000 200   40000  10000    1000];

set(0,'DefaultFigureWindowStyle','docked');
set(0,'defaultAxesFontName', 'Myriad Pro') % Change from Helvetica, since Adobe Illustrator can't recognize that font.
set(0,'defaultTextFontName', 'Myriad Pro')
set(0,'defaultTextFontSize', 22)
set(0,'defaultAxesFontSize', 20)

display('--------------------------------------------------------------------------------------------');
display('|                COMMAND LIST                                                              |');
display('|      data = Calibrate(datadirectory,Date,calparams,calnumber,showplots,writecal)         |');
display('|      data = ReadMattFile_Wrapper(startpath,rootfile)                                     |');
display('|      data = force_offset_subtraction(trapData, calData, offsetData)                      |');
display('|      data = plot_force_extension(trapData, construct)                                    |');
display('|      data = load_data(datadirectory, Date, [filenumber calnumber offsetnumber], calparams)    |');
display('|      data = trap_data(traceData, calData)                                                |');
display('|      data = trap_data_nmConversion(trapData)                                             |');
display('|      FigList = save_all_fig(dir)                                                         |');
display('--------------------------------------------------------------------------------------------');
display('|               beadA  beadB    avwin   fxyhi  fsumhi   flo                                |');
display(['|  calparams = [ ',num2str(calparams(1)),'    ',num2str(calparams(2)),'     ',...
         num2str(calparams(3)),'     ',num2str(calparams(4)),'  ',num2str(calparams(5)),'    ',...
         num2str(calparams(6)),']    	              |']);
display('----------------------------------------------------------------------------------');

% global analysisPath
% analysisPath = '/Users/WanderingCorn/Experiment Diary/MATLAB by Suoang/';

global fbslash
fbslash = '/';

addpath(['/Users/WanderingCorn/Experiment Diary/Suoang' '''' 's Matlab code']);
