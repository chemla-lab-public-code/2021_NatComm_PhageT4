function meanmap = reshape_mean_apdimages(data,scanlines,nsteps)
%take linear array of data from raster scan image (back and forth) and
%reshape it and take the mean of the forward and backwards images.
%mjc 100324

map1 = reshape(data,[],scanlines)';
map1 = flipud(map1);
map1l = fliplr(map1(:,1:nsteps));
map1r = map1(:,nsteps+1:end);

meanmap = (map1l+map1r)/2;

end