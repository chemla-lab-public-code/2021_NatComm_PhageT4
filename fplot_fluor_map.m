function map_data = fplot_fluor_map(directory, Date, datafnumber)
%plot MCL map of APD 1
%note that for now, the code does not work if the fast scan direction is in
%y rather than x (091016, mjc)

%plot apd1 and apd2 now (100921 mjc)

global datadirectories;
global laptop;

if nargin == 0
    Date = '210701';
    datafnumber = 93;    
    rootfile = [Date '_' num2str(datafnumber,'%03d') '.dat'];
end

if laptop == 1
    figposition = [142          93        1350         885];
    figposition2 = [142          93        1350         885];
else
    figposition = [560    93   801   855];
    figposition2 = [564   145   801   358];
end

Avefactor = 1;
plot_real_units = 1;
FIT = 0;

Savefigdir = [datadirectories Date '/'];

mclxcal = 1.077;%um/V   % once was 1.07
mclycal = 0.72;

startpath = [directory num2str(Date) '/'];
rootfile = [num2str(Date) '_' num2str(datafnumber,'%03d') '.dat'];
datafilename = ['data_' rootfile(1:(end-4))];
% [startpath rootfile]
data = ReadMattFile_Wrapper(startpath,rootfile);

if data.datatype == 3 %map
    %fix for older data sets where napdsampstep wasn't recorded and = 0
    if data.napdsampstep == 0
        data.napdsampstep = 1;
    end
    
    %if more than 1 sample per step, integrate
    if data.napdsampstep > 1
        data.apd1 = apd_integrate(data.apd1,data.napdsampstep);
        data.apd2 = apd_integrate(data.apd2,data.napdsampstep);
    end        

    if data.version <= 8
        scanlines = data.nsteps;
        length(data.apd1)
        if length(data.apd1) == data.nsteps^2*2
            ndim = 1;
        else
            ndim = 0;
        end
    else
        scanlines = data.nscanfb;
        ndim = data.scan2dor1d;
    end
         
    %scale data to be in Hz
    data.apd1 = data.apd1/(data.napdsampstep*(data.apdtime(2)-data.apdtime(1)));
    data.apd2 = data.apd2/(data.napdsampstep*(data.apdtime(2)-data.apdtime(1)));
    
   if ~any(data.apd1) && ~any(data.apd2) % If no APD data, don't plot a figure
       disp(['No APD data for ' rootfile])
   else
    vfig = figure;
    %et(vfig,'Position',figposition)
    set(gcf,'FileName',[Savefigdir rootfile(1:10)]);
    set(gcf,'Name',[rootfile(1:10) ' APD map']);
    
    Vstep = data.scanrange/(data.nsteps-1);
    
    Vx = fliplr((0:data.nsteps-1)*Vstep + data.initialx);

    if ndim == 1
        %image
            meanmap1 = reshape_mean_apdimages(data.apd1,scanlines,data.nsteps);
            meanmap2 = reshape_mean_apdimages(data.apd2,scanlines,data.nsteps);
            
            if plot_real_units
                xscale = mclxcal;
                yscale = mclycal;
                ylab = 'MCL Y (\mum)';
                xlab = 'MCL X (\mum)';
               
            else
                xscale = 1;
                yscale = 1;
                ylab = 'MCL Y (V)';
                xlab = 'MCL X (V)';

            end

            if 1 %just show the apd intensity
                Vy = fliplr((0:data.nscanfb-1)*Vstep + data.initialy);

                subplot(2,1,1)
                I = imagesc(meanmap1);

                map_data.image1 = meanmap1;
                map_data.image2 = meanmap2;
                map_data.xscale = Vx*xscale;
                map_data.yscale = Vy*yscale;
                set(I,'XData',Vx*xscale)
                set(I,'YData',Vy*yscale)
                axis image
                set(gca,'XDir','reverse')
                set(gca,'YDir','normal')
                ylabel(ylab)
                title(['APD 1 Image (' rootfile(1:6) '\_' rootfile(8:10) ')'])
                colorbar
                
                subplot(2,1,2)
                I = imagesc(meanmap2);
                set(I,'XData',Vx*xscale)
                set(I,'YData',Vy*yscale)
                axis image
                set(gca,'XDir','reverse')
                set(gca,'YDir','normal')
                ylabel(ylab)
                title(['APD 2 Image (' rootfile(1:6) '\_' rootfile(8:10) ')'])
                colorbar
                
            else %show the apd intensity plus the detection laser x and y
                set(gcf,'Position',[109          40        1519         935]);

                trapfactor = length(data.C_Y)/length(data.apd1);
                
                C_Y = downsample(data.C_Y,trapfactor);
                C_X = downsample(data.C_X,trapfactor);
                
                 ymap = reshape_mean_apdimages(C_Y,scanlines,data.nsteps);
                psdxmap = reshape_mean_apdimages(C_X,scanlines,data.nsteps);
                
                Vy = fliplr((0:data.nscanfb-1)*Vstep + data.initialy);

                subplot(3,2,1)
                I = imagesc(meanmap1);  
                set(I,'XData',Vx)
                set(I,'YData',Vy)
                axis image
                set(gca,'XDir','reverse')
                set(gca,'YDir','normal')
                ylabel('MCL Y (V)')
                title(['APD 1 Image (' rootfile(1:6) '\_' rootfile(8:10) ')'])
                colorbar

                subplot(3,2,2)
                plot(Vx,sum(meanmap1))
                set(gca,'XDir','reverse')
                axis tight

                subplot(3,2,3)
                I = imagesc(psdymap);  
                set(I,'XData',Vx)
                set(I,'YData',Vy)
                axis image
                set(gca,'XDir','reverse')
                set(gca,'YDir','normal')
                ylabel('MCL Y (V)')
                title('PSD Y Image')
                colorbar
                
                subplot(3,2,4)
                plot(Vy,mean(psdymap')-mean(psdymap(:)),'k')
                hold on
                
                plot(Vy,mean(psdymap(:,1:data.nsteps/2)')-mean(psdymap(:)),'b')
                plot(Vy,mean(psdymap(:,data.nsteps/2+1:end)')-mean(psdymap(:)),'r')
                axis tight
                legend({'whole image' 'left half' 'right half'})

                subplot(3,2,5)
                I = imagesc(psdxmap);  
                set(I,'XData',Vx)
                set(I,'YData',Vy)
                axis image
                set(gca,'XDir','reverse')
                set(gca,'YDir','normal')
                ylabel('MCL Y (V)')
                title('PSD X Image')
                colorbar
                xlabel('MCL X (V)')
                
                subplot(3,2,6)
                plot(Vx,abs(mean(psdxmap)-mean(psdxmap(:))))
                set(gca,'XDir','reverse')
                axis tight
                
            end
    
    else
        if plot_real_units
                xscale = mclxcal;
                yscale = mclycal;
                ylab = 'MCL Y (\mum)';
                xlab = 'MCL X (\mum)';
         else
                xscale = 1;
                yscale = 1;
                ylab = 'MCL Y (V)';
                xlab = 'MCL X (V)';
        end
            
        line1 = data.apd1(1:data.nsteps);
        line2 = fliplr(data.apd1(data.nsteps+1:end));
        
        mline = (line1+line2)/2;
        
        [~, i1] = max(mline(1:floor(data.nsteps/2)));
        [~, i2] = max(mline(floor(data.nsteps/2)+1:end));    
        i2 = i2 + floor(data.nsteps/2);
        
        i3 = floor((i1+i2)/2);
        
        ii = [i1 i2 i3];
        
        fVx = fliplr(Vx*xscale);
        
        plot(fVx,mline)
        hold on
%         plot(fVx,line1,'r')
%         plot(fVx,line2,'k')
%         plot(fVx(ii),mline(ii),'+r','MarkerSize',10)

        set(gca,'XDir','reverse')

        ylabel('APD Intensity (Hz)')
    end
%         xlabel(xlab)

    if 0 %plot map scaled in length units
        hfig = figure;
        set(hfig,'Position',figposition2)
        set(gcf,'FileName',[Savefigdir rootfile(1:10)]);
        set(gcf,'Name',[rootfile(1:10) ' APD map']);
        %     subplot(1,2,1)
        I = imagesc(meanmap);
        x = mclxcal * Vx;
        y = mclycal * Vy;
        set(I,'XData',x)
        set(I,'YData',y)
        axis image
        set(gca,'XDir','reverse')
        set(gca,'YDir','normal')
        ylabel('MCL Y (\mum)')
        xlabel('MCL X (\mum)')
        title('APD 1 Map')
        colorbar
    end
    
%     figure('Position',[603    39   560   420])
%     set(gcf,'FileName',[Savefigdir Date '_' num2str(datafnumber,'%03d')]);
%     set(gcf,'Name',[Date '_' num2str(datafnumber,'%03d') ' APD map - 3D']);
%     surf(map1l)   
    
%     s = fitoptions('Method','LeastSquares',...
%         'Lower',[0,0],...
%         'Upper',[Inf,max(cdate)],...
%         'Startpoint',[1 1]);

    if FIT

    a1 = max(map1l(:));
    a1u = a1*1.5;
    a1l = a1*.5;
    b1 = x(floor(length(x)/2));
    b1u = b1+2;
    b1l = b1-2;
    b2 = y(floor(length(y)/2));
    b2u = b2+2;
    b2l = b2-2;
    c1 = 0.5;
    c1u = 2;
    c1l = 0.1;
    c2 = 0.5;
    c2u = 2;
    c2l = 0.1;
    startpoint = [a1,b1,b2,c1,c2];
    upper = [a1u,b1u,b2u,c1u,c2u];
    lower = [a1l,b1l,b2l,c1l,c2l];
    
    s = fitoptions('Method','NonlinearLeastSquares','StartPoint',startpoint,'Lower',lower,'Upper',upper);
    f = fittype('a1*exp(-( ((x-b1)/c1).^2+((y-b2)/c2).^2 ))','indep',{'x' 'y'},'depend','z','options',s)
    [X,Y] = meshgrid(x,y);
    cfun = fit([X(:),Y(:)],map1l(:),f)
    
    hold on
    plot(cfun.b1,cfun.b2,'wx','LineWidth',2,'MarkerSize',10)
    
    figure(vfig)
    hold on
    plot(cfun.b1/mclxcal,cfun.b2/mclycal,'wx','LineWidth',2,'MarkerSize',10)
    title(['APD1 Map (center: x = ' num2str(cfun.b1/mclxcal,'%0.2f') ' V, y = ' num2str(cfun.b2/mclycal,'%0.2f') ' V)']);
    
    %figure;
    %plot(cfun)
    set(gca,'XDir','reverse')
    set(gca,'YDir','normal')
    
    figure(hfig)
    subplot(1,2,2)
    [Xfine,Yfine] = meshgrid(xfine,yfine);
    J = imagesc(cfun(Xfine,Yfine))
    set(J,'XData',x)
    set(J,'YData',y)
    axis image
    set(gca,'XDir','reverse')
    set(gca,'YDir','normal')
    ylabel('MCL Y (\mum)')
    xlabel('MCL X (\mum)')
    title('APD 1 Map')
    colorbar
    hold on
    plot(cfun.b1,cfun.b2,'wx','LineWidth',2,'MarkerSize',10)

    data.map1l = map1l;
    data.map1r = map1r;
    end

% datacursormode on
% dcm_obj = datacursormode(vfig);
% 
% set(dcm_obj,'DisplayStyle','datatip','SnapToDataVertex','off','DisplayStyle','Window')
    end
end
% assignin('base',datafilename,data);


end