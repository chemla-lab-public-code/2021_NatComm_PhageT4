load('/Users/WanderingCorn/Documents/2020 paper/threshold_100/stat_100.mat')
set(0,'DefaultLineMarkerSize',3);
set(0,'defaultAxesFontSize',15)
set(0, 'DefaultAxesLineWidth', 1.5)

figure()
set(gcf,'Name','Paper Fig1')

%% plot 6 sample traces
fig_traces = subplot(6,6,1:12);
hold on
samples_wt = [14 28 3];%[80 108 23];
samples_Q163A = [13 55 3 40 8];%[23 77 5];
shift = 4;
directory = OT;
for i = 1:length(samples_Q163A)
    Date = str2num(trace_mp{1,samples_Q163A(i)}.file(1:6));
    stgTimeFilenum = str2num(trace_mp{1,samples_Q163A(i)}.file(8:10));
    [stgTime, stgPos] = StageMoveTime(directory, Date, stgTimeFilenum);
    time = trace_mp{1,samples_Q163A(i)}.time - stgTime(2)-shift;
    h_samples(2*i)=plot(time,trace_mp{1,samples_Q163A(i)}.DNAbp_mod,'Color',red,'LineWidth',1.5);
end
for i = 1:length(samples_wt)
    Date = str2num(trace_wt{1,samples_wt(i)}.file(1:6));
    stgTimeFilenum = str2num(trace_wt{1,samples_wt(i)}.file(8:10));
    [stgTime, stgPos] = StageMoveTime(directory, Date, stgTimeFilenum);
    time = trace_wt{1,samples_wt(i)}.time - stgTime(2)-4;
    h_samples(2*i-1)=plot(time,trace_wt{1,samples_wt(i)}.DNAbp_mod,'Color',blue,'LineWidth',1.5);
end

xlim([-8 35])
ylim([0 5000])
h_move = plot([-shift -shift], [0 5000],'LineStyle','--','Color','k','LineWidth',1.25);
h_stop = plot([stgTime(3)-stgTime(2)-shift stgTime(3)-stgTime(2)-shift], [0 5000],'LineStyle','--','Color','k','LineWidth',1.25);
%legend([h_samples([1 4]) h_move],'WT','MD','move/stop');
legend([h_samples([1 4])],'WT','MD');
xlabel('Time (s)')
ylabel('DNA Extension (kb)')
grid on
% grid minor
set(gca,'YTick',[0 1000 2000 3000 4000 5000],...
        'YTickLabel',{'0','1','2','3','4','5'});

 

%% plot restart time

% fig_restart = subplot(6,6,[13 14 19 20]);
% hold on
% defaultAxePos = fig_restart.Position;
% L = defaultAxePos(1);
% B = defaultAxePos(2);
% W = defaultAxePos(3);
% H = defaultAxePos(4);
% fig_restart.Position = [L B 0.9*W H];
%     
% temp = find([stat_mp_100.restart]>15);
% catIdx = [ones(62,1);3*ones(44,1)];
% catIdx(temp)=2;
% catCol=[0.85 0.325 0.098; 0 0 0; 0 0.447 0.741];
% catMark = num2cell('o*o',1);
% 
% plotSpread({[stat_mp_100.restart]-4,[stat_wt_100.restart]-4}, 'xNames', {'MD','WT'},...
%     'categoryIdx',catIdx,...
% 'categoryColors',catCol,...
% 'categoryMarkers',catMark)
% 
% ax1=gca;ylabel('Restart Time (s)')
% set(gca, 'yscale','linear')
% ylim([-5 55])


fig_restart = subplot(6,6,[13 14 19 20]);
hold on
defaultAxePos = fig_restart.Position;
L = defaultAxePos(1);
B = defaultAxePos(2);
W = defaultAxePos(3);
H = defaultAxePos(4);
fig_restart.Position = [L B+0.5*H 0.9*W 0.5*H];

h_restart_wt = histogram([stat_wt_100.restart]-4,'Normalization','probability');
h_restart_mp = histogram([stat_mp_100.restart]-4,'Normalization','probability');
h_restart_wt.BinEdges = (-1):5:63-4; 
h_restart_mp.BinEdges = h_restart_wt.BinEdges;
% h_restart_mp.BinWidth = h_restart_wt.BinWidth;
xlim([-3 55])
set(gca,'xtick',[],'xticklabel',{[]})
ylabel('Frequency')
set(gca, 'xscale','linear')
%% plot scattered restart time
fig_restart2 = subplot(6,6,36);
fig_restart2.Position = [L B 0.9*W 0.5*H];
    
temp = find([stat_mp_100.restart]>15);
catIdx = [ones(62,1);3*ones(44,1)];
catIdx(temp)=2;
catCol=[0.85 0.325 0.098; 0 0 0; 0 0.447 0.741];
catMark = num2cell('o*o',1);

plotSpread({[stat_mp_100.restart]-4,[stat_wt_100.restart]-4}, 'xNames', {'MD','WT'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,...
'xyor','flipped','categoryMarkers',catMark,'spreadWidth',1.25)

ax1=gca;xlabel('Restart Time (s)')
xlim([ -3 55])
ylim([0 3])
set(gca, 'xscale','linear')


%% plot packaging velcoity
% vel_wt = [stat_wt(idx_good_wt).mean_vel_pack];
% vel_mp = [stat_Q163A(idx_good_Q163A).mean_vel_pack];
vel_wt = [stat_wt_100.mean_vel_pack];
vel_mp = [stat_mp_100.mean_vel_pack];

fig_vel = subplot(6,6,[15 16 21 22]);
defaultAxePos = fig_vel.Position;
L = defaultAxePos(1);
B = defaultAxePos(2);
W = defaultAxePos(3);
H = defaultAxePos(4);
fig_vel.Position = [L B+0.5*H 0.9*W 0.5*H];
hold on
h_vel_wt = histogram(vel_wt,'Normalization','probability');
h_vel_mp = histogram(vel_mp,'Normalization','probability');
h_vel_wt.BinWidth = 300; h_vel_mp.BinWidth = h_vel_wt.BinWidth;
[kde_wt, x_wt] = ksdensity(vel_wt,'bandwidth',h_vel_wt.BinWidth);
[kde_mp, x_mp] = ksdensity(vel_mp,'bandwidth',h_vel_wt.BinWidth);
plot(x_wt,kde_wt,'Color',blue);
plot(x_mp,kde_mp,'Color',red);
legend('WT','MD')
xlim([-100 1900])
ylim([0 0.5])
%xlabel('Mean Packaging Velocity (bp/s)')
ylabel('Frequency')
set(gca,'xtick',[],'xticklabel',{[]})
set(gca,'YTick',0:0.1:0.5,...
        'YTickLabel',{'0','0.1','0.2','0.3','0.4','0.5'});
defaultAxePos_vel = fig_vel.Position;
% Probability density function estimate: The height of each bar is, 
% (number of observations in bin)/ (total number of observations * width of bin). 
% The area of each bar is the relative number of observations, 
% and the sum of the bar areas is 1.
%% plot scattered packaging vel
fig_vel2 = subplot(6,6,36);
fig_vel2.Position = [L B 0.9*W 0.5*H];

%find([stat_mp_100.restart]>15);
temp = find([stat_mp_100.restart]>15);
catIdx = [ones(62,1);3*ones(44,1)];
catIdx(temp)=2;
catCol=[0.85 0.325 0.098; 0 0 0; 0 0.447 0.741];
catMark = num2cell('o*o',1);

plotSpread({[stat_mp_100.mean_vel_pack],[stat_wt_100.mean_vel_pack]}, 'xNames', {'MD','WT'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,...
'xyor','flipped','categoryMarkers',catMark,'spreadWidth',1.8)

ax1=gca;xlabel('Mean Packaging Velocity (bp/s)')
xlim([-100 1900])
ylim([0 3])
set(gca, 'xscale','linear')
set(gca,'XTick',[0      600     1200    1800],...
        'XTickLabel',{'0','600','1200','1800'});

%% plot pause frequency
% pf_wt = [stat_wt(idx_good_wt).num]./[stat_wt(idx_good_wt).sum_pack_length]*1000;
% pf_mp = [stat_Q163A(idx_good_Q163A).num]./[stat_Q163A(idx_good_Q163A).sum_pack_length]*1000;
pf_wt = [stat_wt_100.pause_freq];
pf_mp = [stat_mp_100.pause_freq];
FakeInf = 10^(-1.75);
pf_wt(pf_wt == 0) = FakeInf;
pf_mp(pf_mp == 0) = FakeInf;

fig_pf = subplot(6,6,[17 18 23 24]);
defaultAxePos = fig_pf.Position;
L = defaultAxePos(1);
B = defaultAxePos(2);
W = defaultAxePos(3);
H = defaultAxePos(4);
fig_pf.Position = [L B+0.5*H W 0.5*H];
hold on
h_pf_wt = histogram(log10(pf_wt),'Normalization','probability');
h_pf_mp = histogram(log10(pf_mp),'Normalization','probability');
h_pf_wt.BinEdges = [-2 -1.5 -0.75 -0.25 0.25 0.75 1.25 1.75];
h_pf_mp.BinEdges = h_pf_wt.BinEdges;
% BinWidth = 0.5; 
% h_pf_mp.BinWidth = h_pf_wt.BinWidth;

% [kde_wt, x_wt] = ksdensity(pf_wt);
% [kde_mp, x_mp] = ksdensity(pf_mp);
% plot(log10(x_wt),kde_wt,'Color',blue);
% plot(log10(x_mp),kde_mp,'Color',red);

xlabel('Number of Pauses per kb')
ylabel('Frequency')
legend('WT','MD')
set(gca,'xtick',[],'xticklabel',{[]})
xlim([-2.2 1.8])
ylim([0 0.5])
set(gca,'YTick',0:0.1:0.5,...
        'YTickLabel',{'0','0.1','0.2','0.3','0.4','0.5'});
%% plot scattered pause freq
fig_pf2 = subplot(6,6,36);
fig_pf2.Position = [L B W 0.5*H];
    
%find([stat_mp_100.restart]>15);
temp = find([stat_mp_100.restart]>15);
catIdx = [ones(62,1);3*ones(44,1)];
catIdx(temp)=2;
catCol=[0.85 0.325 0.098; 0 0 0; 0 0.447 0.741];
catMark = num2cell('o*o',1);

plotSpread({log10(pf_mp),log10(pf_wt)}, 'xNames', {'MD','WT'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,...
'xyor','flipped','categoryMarkers',catMark)

ax1=gca;xlabel('Number of Pauses per kb')
xlim([-2.2 1.8])
ylim([0 3])
set(gca, 'xscale','linear')
set(gca,'XTick',[-1.75 -0.5 0 0.5 1 1.5],...
        'XTickLabel',{'No Pause','10^{-0.5}','10^0','10^{0.5}','10^1','10^{1.5}'});

    
    
    
    
    
    
    
    
    
    





%% Supplementay Figs
set(0,'DefaultLineMarkerSize',6);
set(0,'defaultAxesFontSize',20)
set(0, 'DefaultAxesLineWidth', 1.5)
figure('Name','Supp Pause')



%% plot relationship between packaging velocity and pause frequency
pf_wt = [stat_wt_100.pause_freq];
pf_mp = [stat_mp_100.pause_freq];

fig_velvspf = subplot(1,3,1);

hold on
FakeInf = 10^(-1);
pf_wt(pf_wt == 0) = FakeInf;
pf_mp(pf_mp == 0) = FakeInf;%eps = sthE-18;
h_velvspf_wt = plot(vel_wt,pf_wt,'o','Color',blue);
h_velvspf_mp = plot(vel_mp,pf_mp,'o','Color',red);
set(gca,'yscale','log','xscale','log');
legend('WT','MD')
xlabel('Mean Packaging Velocity (bp/s)')
ylabel('Number of Pauses per kb')
xlim([200 3000])
ylim([FakeInf 100])
set(gca,'YTick',[FakeInf 10^(-0.5) 10^0 10^(0.5) 10^1 10^(1.5)],...
        'YTickLabel',{'0','10^{-0.5}','10^0','10^{0.5}','10^1','10^{1.5}'},...
        'XTick',[200 600 1000 1800],...
        'XTickLabel',{'200','600','1000','1800'});
    xlim([200 1800])
    ylim([10^(-1.1) 10^(1.6)])

    
%% plot pause vel
fig_vpause = subplot(1,3,2);
    
plotSpread({nonzeros([stat_mp_100.mean_vel_pause]),nonzeros([stat_wt_100.mean_vel_pause])},...
    'xNames', {'MD', 'WT'}, 'distributionColors',{[0.85 0.325 0.098],[0,0.447,0.741]},...
    'xyor','flipped','distributionMarkers',{'o','o'})

ax1=gca;xlabel('Mean Pausing Velocity (bp/s)')
set(gca, 'xscale','linear')

%% plot pause duration
fig_duration = subplot(1,3,3);
    
plotSpread({nonzeros([stat_mp_100.mean_pause_duration]),nonzeros([stat_wt_100.mean_pause_duration])},...
    'xNames', {'MD', 'WT'}, 'distributionColors',{[0.85 0.325 0.098],[0,0.447,0.741]},...
    'xyor','flipped','distributionMarkers',{'o','o'})

ax1=gca;xlabel('Mean Pause Duration (s)')
set(gca, 'xscale','log')


%%
temp = find([stat_mp_100.mean_vel_pause]<-100);
dynamics = [stat_mp_100(temp).mean_vel_pause];
maxV = max(dynamics);
minV = min(dynamics);

[sortDynamics,sortIdx] = sort([stat_mp_100(temp).mean_vel_pause],"ascend");
sortIdx = temp(sortIdx);
catIdx = 7*ones(62,1);
catIdx(sortIdx)=deal(1:6);

catCol = [];
catCol(:,3)=deal((sortDynamics-minV)/(maxV-minV));
catCol(:,2)=0;
catCol(:,1)=deal((maxV-sortDynamics)/(maxV-minV));
catCol(7,:) = [0 0 1];
catCol = num2cell(catCol,2);
catMark = num2cell('oooooo.',1);
figure('Name','Supp VelPause')
subplot(2,4,[1 5])
plotSpread({[stat_wt_100.mean_vel_pause]}, 'distributionColors',[1 0 0],'xNames', {'WT Vel Pause'})
set(gca, 'yscale','linear')
ax1 = gca;
subplot(2,4,[2 6])
plotSpread({[stat_mp_100.mean_vel_pause]}, 'xNames', {'MP Vel Pause'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,'categoryMarkers',catMark)
set(gca, 'yscale','linear')
ax2 = gca;
linkaxes([ax1,ax2],'y')
subplot(2,4,3)
plotSpread({[stat_mp_100.mean_vel_pack]}, 'xNames', {'Vel Pack'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,'categoryMarkers',catMark)
set(gca, 'yscale','linear')
subplot(2,4,4)
plotSpread({[stat_mp_100.restart]}, 'xNames', {'Restart'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,'categoryMarkers',catMark)
set(gca, 'yscale','linear')
subplot(2,4,7)
plotSpread({[stat_mp_100.pause_freq]}, 'xNames', {'Pause Freq'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,'categoryMarkers',catMark)
set(gca, 'yscale','log')
subplot(2,4,8)
plotSpread({[stat_mp_100.mean_pause_duration]}, 'xNames', {'Pause Duration'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,'categoryMarkers',catMark)
set(gca, 'yscale','log')


temp = find([stat_mp_100.restart]>15);
dynamics = [stat_mp_100(temp).restart];
maxV = max(dynamics);
minV = min(dynamics);

[sortDynamics,sortIdx] = sort([stat_mp_100(temp).restart],"descend");
sortIdx = temp(sortIdx);
catIdx = 13*ones(62,1);
catIdx(sortIdx)=deal(1:12);

catCol = [];
catCol(:,1)=deal((sortDynamics-minV)/(maxV-minV));
catCol(:,2)=0;
catCol(:,3)=deal((maxV-sortDynamics)/(maxV-minV));
catCol(13,:) = [0 0 1];
catCol = num2cell(catCol,2);
catMark = num2cell('oooooooooooo.',1);
figure('Name','Supp Restart')
subplot(2,4,[1 5])
plotSpread({[stat_wt_100.restart]}, 'distributionColors',[1 0 0], 'xNames', {'WT Restart'})
set(gca, 'yscale','linear')
ax1 = gca;
subplot(2,4,[2 6])
plotSpread({[stat_mp_100.restart]}, 'xNames', {'MP Restart'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,'categoryMarkers',catMark)
set(gca, 'yscale','linear')
ax2 = gca;
linkaxes([ax1 ax2])
subplot(2,4,3)
plotSpread({[stat_mp_100.mean_vel_pack]}, 'xNames', {'Vel Pack'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,'categoryMarkers',catMark)
set(gca, 'yscale','linear')
subplot(2,4,4)
plotSpread({[stat_mp_100.mean_vel_pause]}, 'xNames', {'Vel Pause'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,'categoryMarkers',catMark)
set(gca, 'yscale','linear')
subplot(2,4,7)
plotSpread({[stat_mp_100.pause_freq]}, 'xNames', {'Pause Freq'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,'categoryMarkers',catMark)
set(gca, 'yscale','log')
subplot(2,4,8)
plotSpread({[stat_mp_100.mean_pause_duration]}, 'xNames', {'Pause Duration'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,'categoryMarkers',catMark)
set(gca, 'yscale','log')




