function DynamicCorrelation(struct1,struct2,field1,unit1,field2,unit2,varargin)
%
if ~isempty(varargin)
    option = varargin{1};
else
    option = [0 0];
end

title1 = line2space(field1);
title2 = line2space(field2);
% plot_one_field(struct1,struct2,field1,unit1,option(1))
% plot_one_field(struct1,struct2,field2,unit2,option(2))
% script
plot_data_bin([struct1.(field1)],[struct2.(field1)],...
    [struct1.(field2)],[struct2.(field2)],...
    title1,unit1,title2,unit2,option)
% plot_data_bin([struct1.(field2)],[struct2.(field2)],...
%     [struct1.(field1)],[struct2.(field1)],...
%     title2,unit2,title1,unit1,[option(2) option(1)])


%% local func    


function plot_data_bin(Ax,Ay,Bx,By,label01,unit01,label02,unit02,option)

if option(1)
    Ax(Ax==0)=[];
    Ay(Ay==0)=[];
end
if option(2)
    Bx(Bx==0)=[];
    By(By==0)=[];
end
% bin Ax and Bx
figure()
plot(Ax, Bx,'o','DisplayName','WT')
hold on
plot(Ay, By,'o','DisplayName','MP')
xlabel([label01 ' (' unit01 ')'])
ylabel([label02 ' (' unit02 ')'])
set(gcf,'Name',[label01 ' vs.' label02 ' Scatter'])
legend('show')

clear bin_x
[~,i_x]=sort(Ax);
Ax = Ax(i_x);
Bx = Bx(i_x);
% f=figure(); hold on
for i = 1:ceil(length(Ax)/9)
%     plot(Ax(9*i-8:min(9*i,end)),Bx(9*i-8:min(9*i,end)),'o')
    temp = bootstrp(10000, @mean, Bx(9*i-8:min(9*i,end)));
    bin_x(1,i) = mean(temp);
    bin_x(3,i) = prctile(temp,5)-bin_x(1,i);
    bin_x(4,i) = prctile(temp,95)-bin_x(1,i);
    
    bin_x(2,i) = std(Bx(9*i-8:min(9*i,end)));
    
    temp = bootstrp(10000, @mean, Ax(9*i-8:min(9*i,end)));
    bin_x(5,i) = mean(Ax(9*i-8:min(9*i,end)));
    bin_x(6,i) = prctile(temp,5)-bin_x(5,i);
    bin_x(7,i) = prctile(temp,95)-bin_x(5,i);
end

clear bin_y
[~,i_y]=sort(Ay);
Ay = Ay(i_y);
By = By(i_y);
for i = 1:ceil(length(Ay)/9)
%     plot(Ay(9*i-8:min(9*i,end)),By(9*i-8:min(9*i,end)),'*')
    temp = bootstrp(10000, @mean, By(9*i-8:min(9*i,end)));
    bin_y(1,i) = mean(temp);
    bin_y(3,i) = prctile(temp,5)-bin_y(1,i);
    bin_y(4,i) = prctile(temp,95)-bin_y(1,i);
    
    bin_y(2,i) = std(By(9*i-8:min(9*i,end)));
    
    temp = bootstrp(10000, @mean, Ay(9*i-8:min(9*i,end)));
    bin_y(5,i) = mean(Ay(9*i-8:min(9*i,end)));
    bin_y(6,i) = prctile(temp,5)-bin_y(5,i);
    bin_y(7,i) = prctile(temp,95)-bin_y(5,i);
    
end

figure(); hold on
clear boots_Ax
clear boots_Ay
for i = 1:ceil(length(Ax)/9)
%     plot(A_wt(9*i-8:min(9*i,end)),B_wt(9*i-8:min(9*i,end)),'o')
    [~,bootsamp] = bootstrp(1000, @mean, 9*i-8:min(9*i,length(Ax)));
    bootsamp = bootsamp+9*i-9;
    boots_Ax(i,:) = mean(Ax(bootsamp)); % get 1000 xpos = mean vel
    boots_Bx(i,:) = mean(Bx(bootsamp)); % get 1000 ypos = man restart
end
for i = 1:1000
    plot(boots_Ax(:,i),boots_Bx(:,i),'Color',[0.75 0.9 1])
end

clear boots_Ay
clear boots_By
for i = 1:ceil(length(Ay)/9)
    [~,bootsamp] = bootstrp(1000, @mean, 9*i-8:min(9*i,length(Ay)));
    bootsamp = bootsamp+9*i-9;
    boots_Ay(i,:) = mean(Ay(bootsamp)); % get 1000 xpos = mean vel
    boots_By(i,:) = mean(By(bootsamp)); % get 1000 ypos = man restart
end
for i = 1:1000
    plot(boots_Ay(:,i),boots_By(:,i),'Color',[1 0.8 0.7])
end

s1=scatter(Ax,Bx,100,...
    'o','MarkerFaceColor',[0,0.447,0.741],'LineWidth',2);
s2=scatter(Ay,By,100,...
    'o','MarkerFaceColor',[0.85 0.325 0.098],'LineWidth',2);
errorbar(bin_x(5,:),bin_x(1,:),bin_x(3,:),bin_x(4,:),bin_x(6,:),bin_x(7,:),'Color',[0,0.447,0.741],'LineWidth',2)
errorbar(bin_y(5,:),bin_y(1,:),bin_y(3,:),bin_y(4,:),bin_y(6,:),bin_y(7,:),'Color',[0.85 0.325 0.098],'LineWidth',2)
xlabel([label01 ' (' unit01 ')'])
ylabel([label02 ' (' unit02 ')'])
legend([s1 s2],{'WT','MP'})
set(gcf,'Name',[label01 ' vs.' label02 ' Bin'])
end

function str = line2space(str)
    str(find(str == '_'))=' ';
end
end

