% load('/Users/WanderingCorn/Documents/2020 paper/idx_paper.mat')
% load('/Users/WanderingCorn/Documents/2020 paper/stat_paper.mat')

%% initiate
% idx_SL_wt = idx_wt(idx_good_wt,:);
% idx_SL_mp = idx_Q163A(idx_good_Q163A,:);
% idx_SL_wt = [idx_SL_wt zeros(45,6)];
% idx_SL_mp = [idx_SL_mp zeros(62,6)];
% 
% for i = 1:length(idx_SL_wt)
%     data = load_trapData(OT,idx_SL_wt(i,1),calparams,idx_SL_wt(i,4),idx_SL_wt(i,2),idx_SL_wt(i,3));
%     ifstage = exist([data.path data.file(1:end-4) '_stagetime.txt'],'file');
%     if ifstage
%         [stgTime, stgPos] = StageMoveTime(OT, str2num(data.file(1:6)), str2num(data.file(8:10)));
%         idx_SL_wt(i,7)=stgTime(2);
%         idx_SL_wt(i,8)=stgTime(3);
%     else
%         idx_SL_wt(i,7)=nan;
%         idx_SL_wt(i,8)=nan;
%     end
% end
% idx_SL_wt(:,9) = idx_SL_wt(:,5)-idx_SL_wt(:,7);
% 
% for i = 1:length(idx_SL_mp)
%     data = load_trapData(OT,idx_SL_mp(i,1),calparams,idx_SL_mp(i,4),idx_SL_mp(i,2),idx_SL_mp(i,3));
%     ifstage = exist([data.path data.file(1:end-4) '_stagetime.txt'],'file');
%     if ifstage
%         [stgTime, stgPos] = StageMoveTime(OT, str2num(data.file(1:6)), str2num(data.file(8:10)));
%         idx_SL_mp(i,7)=stgTime(2);
%         idx_SL_mp(i,8)=stgTime(3);
%     else
%         idx_SL_mp(i,7)=nan;
%         idx_SL_mp(i,8)=nan;
%     end
% end
% idx_SL_mp(:,9) = idx_SL_mp(:,5)-idx_SL_mp(:,7);

temp = num2cell(index_wt(:,5)'-[stat_wt_100.move]);
[stat_wt_100(:).('restart')]=deal(temp{:});
temp = num2cell(index_mp(:,5)'-[stat_mp_100.move]);
[stat_mp_100(:).('restart')]=deal(temp{:});
%% plot
figure()
subplot(1,2,1)
plotSpread({[stat_wt_100.restart],[stat_mp_100.restart]}, ...
    'xNames', {'WT', 'Q163A'}, 'distributionColors',{blue,red},...
    'distributionMarkers', {'o', 'o'});
ax1=gca;
set(gcf,'Name','restarting time distribution')
ylabel('Restarting Time (s)')
% [H,P]=kstest2(idx_SL_wt(:,9),idx_SL_mp(:,9))
% [H,P]=kstest2(idx_SL_wt(:,9),idx_SL_mp(:,9),'tail','smaller')
% [H,P]=kstest2(idx_SL_wt(:,9),idx_SL_mp(:,9),'tail','larger')
[kde_wt, x_wt,band_wt] = ksdensity([stat_wt_100.restart]);
[kde_mp, x_mp,band_mp] = ksdensity([stat_mp_100.restart]);
[kde_wt, x_wt] = ksdensity([stat_wt_100.restart],'Bandwidth',min(band_wt,band_mp));
[kde_mp, x_mp] = ksdensity([stat_mp_100.restart],'Bandwidth',min(band_wt,band_mp));
subplot(1,2,2)
plot(kde_wt,x_wt,kde_mp,x_mp)
ax2=gca;
linkaxes([ax1,ax2],'y');
xlabel('Probability Density')
title('Kernel Density Estimate')

%% mean&median
mwt = bootstrp(1000, @mean, [stat_wt_100.restart]);
mmp = bootstrp(1000, @mean, [stat_mp_100.restart]);
figure;
h1=histogram(mwt,'Normalization','pdf');
hold on
h2=histogram(mmp,'Normalization','pdf');
h2.BinWidth=h1.BinWidth;
%[mean([stat_wt_100.restart]) mean([stat_mp_100.restart]) mean(mwt) mean(mmp)]
% ans = 8.8265   10.9968    8.8351   10.9492
grid on
legend({['WT ' num2str(mean([stat_wt_100.restart])) 's'], ...
    ['Q163A ' num2str(mean([stat_mp_100.restart])) 's']})
title('restarting time mean')
set(gcf,'Name','restarting time mean')
xlabel('Restarting Time (s)')
mwt = bootstrp(1000, @median, [stat_wt_100.restart]);
mmp = bootstrp(1000, @median, [stat_mp_100.restart]);
figure;
h1=histogram(mwt,'Normalization','pdf');
hold on
h2=histogram(mmp,'Normalization','pdf');
h2.BinWidth=h1.BinWidth;
%[median([stat_wt_100.restart]) median([stat_mp_100.restart]) median(mwt) median(mmp)]
%ans =   7.5318    7.1305    7.5318    7.1305
legend({['WT ' num2str(median([stat_wt_100.restart])) 's'], ...
    ['Q163A ' num2str(median([stat_mp_100.restart])) 's']})
title('restarting time median')
set(gcf,'Name','restarting time median')
xlabel('Restarting Time (s)')


%% script
% plot_data_bin([stat_wt_100.mean_vel_pack],[stat_mp_100.mean_vel_pack],...
%         [stat_wt_100.restart],[stat_mp_100.restart],...
%         'Mean Packaging Velocity (bp/s)','Restart Time (s)','WT','MP','Restart vs.Vpack')
%     
% 
% plot_data_bin([stat_wt_100.mean_vel_pack],[stat_mp_100.mean_vel_pack],...
%         [stat_wt_100.pause_freq],[stat_mp_100.pause_freq],...
%         'Mean Packaging Velocity (bp/s)','Pause Freq (kb^-1)','WT','MP','PauseFreq vs.Vpack')

% plot_data_bin([stat_wt_100.restart],[stat_mp_100.restart],...
%     [stat_wt_100.pause_freq],[stat_mp_100.pause_freq],...
%     'Restart Time(s)','Pause Freq (kb^-1)','WT','MP','PauseFreq vs.Restart')
plot_data_bin([stat_wt_100.pause_freq],[stat_mp_100.pause_freq],...
    [stat_wt_100.restart],[stat_mp_100.restart],...
    'Pause Freq (kb^-1)','Restart Time(s)','WT','MP','Restart vs.PauseFreq')
%% local func    
function plot_data_bin(Ax,Ay,Bx,By,Param_A,Param_B,Catag_x,Catag_y,StrTitle)
% bin Ax and Bx
figure()
plot(Ax, Bx,'o','DisplayName',Catag_x)
hold on
plot(Ay, By,'o','DisplayName',Catag_y)
xlabel(Param_A)
ylabel(Param_B)
set(gcf,'Name',[StrTitle ' Scatter'])
legend('show')

clear bin_x
[~,i_x]=sort(Ax);
Ax = Ax(i_x);
Bx = Bx(i_x);
% f=figure(); hold on
for i = 1:ceil(length(Ax)/9)
%     plot(Ax(9*i-8:min(9*i,end)),Bx(9*i-8:min(9*i,end)),'o')
    temp = bootstrp(10000, @mean, Bx(9*i-8:min(9*i,end)));
    bin_x(1,i) = mean(temp);
    bin_x(2,i) = std(Bx(9*i-8:min(9*i,end)));
    bin_x(3,i) = prctile(temp,5)-bin_x(1,i);
    bin_x(4,i) = prctile(temp,95)-bin_x(1,i);
    bin_x(5,i) = mean(Ax(9*i-8:min(9*i,end)));
end

clear bin_y
[~,i_y]=sort(Ay);
Ay = Ay(i_y);
By = By(i_y);
for i = 1:ceil(length(Ay)/9)
%     plot(Ay(9*i-8:min(9*i,end)),By(9*i-8:min(9*i,end)),'*')
    temp = bootstrp(10000, @mean, By(9*i-8:min(9*i,end)));
    bin_y(1,i) = mean(temp);
    bin_y(2,i) = std(By(9*i-8:min(9*i,end)));
    bin_y(3,i) = prctile(temp,5)-bin_y(1,i);
    bin_y(4,i) = prctile(temp,95)-bin_y(1,i);
    bin_y(5,i) = mean(Ay(9*i-8:min(9*i,end)));
end

figure(); hold on
clear boots_Ax
clear boots_Ay
for i = 1:ceil(length(Ax)/9)
%     plot(A_wt(9*i-8:min(9*i,end)),B_wt(9*i-8:min(9*i,end)),'o')
    [~,bootsamp] = bootstrp(1000, @mean, 9*i-8:min(9*i,length(Ax)));
    bootsamp = bootsamp+9*i-9;
    boots_Ax(i,:) = mean(Ax(bootsamp)); % get 1000 xpos = mean vel
    boots_Bx(i,:) = mean(Bx(bootsamp)); % get 1000 ypos = man restart
end
for i = 1:1000
    plot(boots_Ax(:,i),boots_Bx(:,i),'Color',[0.75 0.9 1])
end

clear boots_Ay
clear boots_By
for i = 1:ceil(length(Ay)/9)
    [~,bootsamp] = bootstrp(1000, @mean, 9*i-8:min(9*i,length(Ay)));
    bootsamp = bootsamp+9*i-9;
    boots_Ay(i,:) = mean(Ay(bootsamp)); % get 1000 xpos = mean vel
    boots_By(i,:) = mean(By(bootsamp)); % get 1000 ypos = man restart
end
for i = 1:1000
    plot(boots_Ay(:,i),boots_By(:,i),'Color',[1 0.8 0.7])
end

s1=scatter(Ax,Bx,100,...
    'o','MarkerFaceColor',[0,0.447,0.741],'LineWidth',2);
s2=scatter(Ay,By,100,...
    'o','MarkerFaceColor',[0.85 0.325 0.098],'LineWidth',2);
errorbar(bin_x(5,:),bin_x(1,:),bin_x(3,:),bin_x(4,:),'Color',[0,0.447,0.741],'LineWidth',2)
errorbar(bin_y(5,:),bin_y(1,:),bin_y(3,:),bin_y(4,:),'Color',[0.85 0.325 0.098],'LineWidth',2)
xlabel(Param_A)
ylabel(Param_B)
legend([s1 s2],{Catag_x,Catag_y})
set(gcf,'Name',[StrTitle 'Bin'])
end