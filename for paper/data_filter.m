%% load Suoang's 134 wt data index, 85 Q163A data indx
% [~,stat_Q163A]= main(OT,calparams,idx_Q163A);
% [~,stat_wt]= main(OT,calparams,idx_wt);

% construct = ones(1,134)*3400;
% construct(110:128) = 4319; % 110th to 128th data is 4319 long
% for i = 1:134
% close(gcf)
% [~,fexts_wt{i},traces_wt{i}]=plot_cal_bp_F_vs_Time_fext(OT, idx_wt(i,1), calparams, idx_wt(i,2), idx_wt(i,3),idx_wt(i,4),idx_wt(i,4)-1,construct(i),[3 5]);
% max_distance(i)=max(fexts_wt{i}.distance);
% std_distance(i) = std(fexts_wt{i}.distance)/sqrt(length(fexts_wt{i}.distance));
% traces_wt{1,i}.packbp = [traces_wt{1,i}.time(traces_wt{1,i}.time<idx_wt(i,6) & traces_wt{1,i}.time>idx_wt(i,5));...
% traces_wt{1,i}.DNAbp(traces_wt{1,i}.time<idx_wt(i,6) & traces_wt{1,i}.time>idx_wt(i,5))];
% traces_wt{1,i}.packbp(1,:) = traces_wt{1,i}.packbp(1,:)-traces_wt{1,i}.packbp(1,1);
% traces_wt{1,i}.packbp_mod = [traces_wt{1,i}.time(traces_wt{1,i}.time<idx_wt(i,6) & traces_wt{1,i}.time>idx_wt(i,5));...
% traces_wt{1,i}.DNAbp_mod(traces_wt{1,i}.time<idx_wt(i,6) & traces_wt{1,i}.time>idx_wt(i,5))];
% traces_wt{1,i}.packbp_mod(1,:) = traces_wt{1,i}.packbp_mod(1,:)-traces_wt{1,i}.packbp_mod(1,1);
% end
% 
% for i = 1:85
% [~,fexts_Q163A{i},traces_Q163A{i}]=plot_cal_bp_F_vs_Time_fext(OT, idx_Q163A(i,1), calparams, idx_Q163A(i,2), idx_Q163A(i,3),idx_Q163A(i,4),idx_Q163A(i,4)-1,3400,[3 5]);
% max_distance_Q163A(i)=max(fexts_Q163A{i}.distance);
% std_distance_Q163A(i) = std(fexts_Q163A{i}.distance)/sqrt(length(fexts_Q163A{i}.distance));
% traces_Q163A{1,i}.packbp = [traces_Q163A{1,i}.time(traces_Q163A{1,i}.time<idx_Q163A(i,6) & traces_Q163A{1,i}.time>idx_Q163A(i,5));...
% traces_Q163A{1,i}.DNAbp(traces_Q163A{1,i}.time<idx_Q163A(i,6) & traces_Q163A{1,i}.time>idx_Q163A(i,5))];
% traces_Q163A{1,i}.packbp(1,:) = traces_Q163A{1,i}.packbp(1,:)-traces_Q163A{1,i}.packbp(1,1);
% traces_Q163A{1,i}.packbp_mod = [traces_Q163A{1,i}.time(traces_Q163A{1,i}.time<idx_Q163A(i,6) & traces_Q163A{1,i}.time>idx_Q163A(i,5));...
% traces_Q163A{1,i}.DNAbp_mod(traces_Q163A{1,i}.time<idx_Q163A(i,6) & traces_Q163A{1,i}.time>idx_Q163A(i,5))];
% traces_Q163A{1,i}.packbp_mod(1,:) = traces_Q163A{1,i}.packbp_mod(1,:)-traces_Q163A{1,i}.packbp_mod(1,1);
% close(gcf)
% end
% 
% 
% idx_good_wt = [];
% for i = 1:134
%     if fexts_wt{i}.fake_offset <100 && fexts_wt{i}.fake_offset >-50 && max_distance(i)<10 && std_distance(i)<0.05 && fexts_wt{i}.newton3<10
%         idx_good_wt = [idx_good_wt i];
%     end
% end
% idx_good_Q163A = [];
% for i = 1:85
%     if fexts_Q163A{i}.fake_offset <100 && fexts_Q163A{i}.fake_offset >-50 && max_distance_Q163A(i)<10 && std_distance_Q163A(i)<0.05 && fexts_Q163A{i}.newton3<10
%         idx_good_Q163A = [idx_good_Q163A i];
%     end
% end
% 
% 
% 
figure()
hold on
for i = idx_good_Q163A
plot(traces_Q163A{i}.packbp(1,:),traces_Q163A{i}.packbp(2,:)-traces_Q163A{i}.packbp(2,1)+3400,'Color',red,'LineWidth',1,'DisplayName',num2str(i))
end
for i = idx_good_wt
plot(traces_wt{i}.packbp(1,:),traces_wt{i}.packbp(2,:)-traces_wt{i}.packbp(2,1)+3400,'Color',blue,'LineWidth',1,'DisplayName',num2str(i))
end

% figure()
% hold on
% for  i = 1:85
%     if ~isempty(idx_good_Q163A==i)
%         plot(fexts_Q163A{i}.DNAext,fexts_Q163A{i}.force,'Color','b','LineWidth',1)
%     end
% end
% for  i = 1:134
%     if ~isempty(idx_good_wt==i)
%         plot(fexts_wt{i}.DNAext,fexts_wt{i}.force,'Color','r','LineWidth',1)
%     end
% end

for  i = idx_good_Q163A
plot(fexts_Q163A{i}.DNAext,fexts_Q163A{i}.force,'Color',blue,'LineWidth',1)
end
for  i = idx_good_wt
plot(fexts_wt{i}.DNAext,fexts_wt{i}.force,'Color',red,'LineWidth',1)
end




% load('/Users/WanderingCorn/Documents/Research Diary/200204/data.mat')
% load('/Users/WanderingCorn/Documents/Research Diary/200121/variables.mat')
mean([stat_Q163A(idx_good_Q163A).mean_vel_pack])
mean([stat_wt(idx_good_wt).mean_vel_pack])
std([stat_Q163A(idx_good_Q163A).mean_vel_pack])/sqrt(length(idx_good_Q163A))
std([stat_wt(idx_good_wt).mean_vel_pack])/sqrt(length(idx_good_wt))
 
temp = [stat_Q163A(idx_good_Q163A).num]./[stat_Q163A(idx_good_Q163A).sum_pack_length]*1000;
[mean(temp) std(temp)/sqrt(length(temp))]
temp = [stat_wt(idx_good_wt).num]./[stat_wt(idx_good_wt).sum_pack_length]*1000;
[mean(temp) std(temp)/sqrt(length(temp))]
% 
% pf_wt = [stat_SL_wt_191003(idx_good_wt).num]./[stat_SL_wt_191003(idx_good_wt).sum_pack_length];
% pf_Q163A = [stat(idx_good_Q163A).num]./[stat(idx_good_Q163A).sum_pack_length];
% figure()
% subplot(1,2,1)
% plotSpread({[stat_SL_wt_191003(idx_good_wt).mean_vel_pack],[stat(idx_good_Q163A).mean_vel_pack]}, 'xNames', {'WT','Q163A'}, 'distributionColors',{blue,red},'distributionMarkers', {'o','o'});
% subplot(1,2,2)
% plotSpread({pf_wt,pf_Q163A}, 'xNames', {'WT','Q163A'}, 'distributionColors',{blue,red},'distributionMarkers', {'o','o'});
% 
% 
% figure();
% plot_kde([stat_SL_wt_191003(idx_good_wt).mean_vel_pack],gcf)
% hold on
% plot_kde([stat(idx_good_Q163A).mean_vel_pack],gcf)
% 
% 
% 
% 
% myfunc = @(x) [x.mean_vel_pack];
% data_1_1 = myfunc(stat_SL_wt_191003(idx_good_wt));
% data_1_2 = myfunc(stat(idx_good_Q163A));
% data_1_2 = data_1_1(data_1_1~=0);
% data_2_2 = data_2_1(data_2_1~=0);
% data_2_1 = myfunc(stat(idx_good_Q163A));
% data_1_2 = data_1_1(data_1_1~=0);
% data_2_2 = data_2_1(data_2_1~=0);
% 
% clear YData
% YData = [mean(data_1_2) mean(data_2_2)];
% colorlist = {[94 151 189]/255,[217 141 108]/255};figure()
% for i = 1:2
% hBar = bar(i,YData(i),'FaceColor',colorlist{i});
% hold on
% end
% % set(gca, 'XTickLabel', xtickstr)
% legend({'WT','Q163A'})
% title('Mean Packaging velocity')
% set(gcf,'Name','V')
% plotSpread({data_1_2,data_2_2}, ...
% 'xNames', {'WT', 'Q163A'}, 'distributionColors',{[0 0.447 0.741],[0.85 0.325 0.0980]},...
% 'distributionMarkers', {'o', 'o'});
% textstr = {num2str(YData(1),'%.3g'),num2str(YData(2),'%.3g')};
% text(1:2,YData,textstr,'HorizontalAlignment','center','VerticalAlignment','bottom');
% 
% 
% myfunc = @(x) 1000*[x.num]./[x.sum_pack_length];
% data_1_1 = myfunc(stat_SL_wt_191003(idx_good_wt));
% data_2_1 = myfunc(stat(idx_good_Q163A));
% data_1_2 = data_1_1(data_1_1~=0);
% data_2_2 = data_2_1(data_2_1~=0);
% clear YData
% YData = [mean(data_1_2) mean(data_2_2)];
% colorlist = {[94 151 189]/255,[217 141 108]/255};figure()
% for i = 1:2
% hBar = bar(i,YData(i),'FaceColor',colorlist{i});
% hold on
% end
% % set(gca, 'XTickLabel', xtickstr)
% legend({'WT','Q163A'})
% title('Mean Packaging velocity')
% set(gcf,'Name','V')
% plotSpread({data_1_2,data_2_2}, ...
% 'xNames', {'WT', 'Q163A'}, 'distributionColors',{[0 0.447 0.741],[0.85 0.325 0.0980]},...
% 'distributionMarkers', {'o', 'o'});
% textstr = {num2str(YData(1),'%.3g'),num2str(YData(2),'%.3g')};
% text(1:2,YData,textstr,'HorizontalAlignment','center','VerticalAlignment','bottom');




% uiopen('/Users/WanderingCorn/Documents/Research Diary/200204/Filtered WT fext.fig',1)
% 
% idx_bad_wt = 1:134;idx_bad_Q163A = 1:85;
% idx_bad_wt = idx_bad_wt(~ismember(idx_bad_wt,idx_good_wt));
% idx_bad_Q163A = idx_bad_Q163A(~ismember(idx_bad_Q163A,idx_good_Q163A));
% 
% figure()
% hold on
% for i = idx_bad_Q163A
% plot(fexts_Q163A{1,i}.DNAext_Y,(fexts_Q163A{1,i}.force_AY+fexts_Q163A{1,i}.force_BY)/2,'Color',yellow)
% end
% for i = idx_good_Q163A
% plot(fexts_Q163A{1,i}.DNAext_Y,(fexts_Q163A{1,i}.force_AY+fexts_Q163A{1,i}.force_BY)/2,'Color',blue)
% end
% figure()
% hold on
% for i = idx_bad_wt
% plot(fexts_wt{1,i}.DNAext_Y,(fexts_wt{1,i}.force_AY+fexts_wt{1,i}.force_BY)/2,'Color',yellow)
% end
% for i = idx_good_wt
% plot(fexts_wt{1,i}.DNAext_Y,(fexts_wt{1,i}.force_AY+fexts_wt{1,i}.force_BY)/2,'Color',blue)
% end

% figure()
% subplot(1,2,1);myfunc = @(x) 1000*[x.num]./[x.sum_pack_length];
% data_1_1 = myfunc(stat_SL_wt_191003(idx_good_wt));data_1_2=data_1_1;
% data_2_1 = myfunc(stat(idx_good_Q163A));data_2_2 = data_2_1;
% clear YData
% YData = [mean(data_1_2) mean(data_2_2)];
% colorlist = {[94 151 189]/255,[217 141 108]/255};subplot(1,2,2);
% for i = 1:2
% hBar = bar(i,YData(i),'FaceColor',colorlist{i});
% hold on
% end
% % set(gca, 'XTickLabel', xtickstr)
% legend({'WT','Q163A'})
% title('Mean Packaging velocity')
% set(gcf,'Name','V')
% plotSpread({data_1_2,data_2_2}, ...
% 'xNames', {'WT', 'Q163A'}, 'distributionColors',{[0 0.447 0.741],[0.85 0.325 0.0980]},...
% 'distributionMarkers', {'o', 'o'});
% textstr = {num2str(YData(1),'%.3g'),num2str(YData(2),'%.3g')};
% text(1:2,YData,textstr,'HorizontalAlignment','center','VerticalAlignment','bottom');
% 
% 
% uiopen('/Users/WanderingCorn/Documents/Prelim/matlab figures/analysis.fig',1)
% figure();hold on;
% plot_kde([stat(idx_good_Q163A).mean_vel_pack],gcf,140)
% plot_kde([stat_SL_wt_191003(idx_good_wt).mean_vel_pack],gcf,140)
% 
% figure();plot_kde([stat(idx_good_Q163A).num]./[stat(idx_good_Q163A).sum_pack_length,gcf)
% figure(); plot_kde([stat(idx_good_Q163A).num]./[stat(idx_good_Q163A).sum_pack_length],gcf)
% 
% [f,xi] = ksdensity([stat(idx_good_Q163A).num]./[stat(idx_good_Q163A).sum_pack_length]);
% figure();plot(xi,f);
% 
% [f,xi] = ksdensity([stat_SL_wt_191003(idx_good_wt).num]./[stat_SL_wt_191003(idx_good_wt).sum_pack_length]);
% hold on;plot(xi,f);
% 
% [f,xi] = ksdensity([stat_SL_wt_191003(idx_good_wt).mean_vel_pack]);
% figure();plot(xi,f);
% 
% [f,xi] = ksdensity([stat_SL_wt_191003(idx_good_wt).mean_vel_pack],'bandwidth',90);
% figure();plot(xi,f);
% [f,xi] = ksdensity([stat_SL_wt_191003(idx_good_wt).mean_vel_pack],'bandwidth',130);
% figure();plot(xi,f);
% [f,xi] = ksdensity([stat_SL_wt_191003.mean_vel_pack],'bandwidth',130);
% hold on; plot(xi,f);
% [f,xi] = ksdensity([stat(idx_good_Q163A).mean_vel_pack],'bandwidth',130);
% hold on;plot(xi,f);
% [f,xi] = ksdensity([stat.mean_vel_pack],'bandwidth',130);
% hold on; plot(xi,f);