figure()
set(gcf,'Name','Paper Fig1')
%% plot 6 sample traces
fig_traces = subplot(2,3,1:3);
hold on
samples_wt = [80 108 23];
samples_Q163A = [23 77 5];
for i = 1:3
    directory = OT;
    Date = str2num(traces_wt{1,samples_wt(i)}.file(1:6));
    stgTimeFilenum = str2num(traces_wt{1,samples_wt(i)}.file(8:10));
    [stgTime, stgPos] = StageMoveTime(directory, Date, stgTimeFilenum);
    time = traces_wt{1,samples_wt(i)}.time - stgTime(2);
    h_samples(2*i-1)=plot(time,traces_wt{1,samples_wt(i)}.DNAbp_mod,'Color',blue,'LineWidth',1.5);
end
for i = 1:3
    Date = str2num(traces_Q163A{1,samples_Q163A(i)}.file(1:6));
    stgTimeFilenum = str2num(traces_Q163A{1,samples_Q163A(i)}.file(8:10));
    [stgTime, stgPos] = StageMoveTime(directory, Date, stgTimeFilenum);
    time = traces_Q163A{1,samples_Q163A(i)}.time - stgTime(2);

        h_samples(2*i)=plot(time,traces_Q163A{1,samples_Q163A(i)}.DNAbp_mod,'Color',red,'LineWidth',1.5);
end

ylim([0 5000])
xlim([-5 20])
h_move = plot([0 0], [0 5000],'LineStyle','--','Color','k','LineWidth',1.25);
h_stop = plot([stgTime(3)-stgTime(2) stgTime(3)-stgTime(2)], [0 5000],'LineStyle','--','Color','k','LineWidth',1.25);
legend([h_samples([1 4]) h_move],'WT','MP','start/stop');
xlabel('Time (s)')
ylabel('DNA Extension (kb)')
grid on
grid minor
set(gca,'YTick',[0 1000 2000 3000 4000 5000],...
        'YTickLabel',{'0','1','2','3','4','5'});
% i=3;
%     Date = str2num(traces_Q163A{1,samples_Q163A(i)}.file(1:6));
%     stgTimeFilenum = str2num(traces_Q163A{1,samples_Q163A(i)}.file(8:10));
%     [stgTime, stgPos] = StageMoveTime(directory, Date, stgTimeFilenum);
%     time = traces_Q163A{1,samples_Q163A(i)}.time - stgTime(2);
%         ax1 = gca;
%         fig_extra = subplot(2,3,3);
%         plot(time,traces_Q163A{1,samples_Q163A(i)}.DNAbp_mod,'Color',red,'LineWidth',1.5);
%         ax2=gca;
%         set(ax2,'position',[ax1.Position(1)+ax1.Position(3) ax2.Position(2) ax2.Position(1)+ax2.Position(3)-ax1.Position(1)-ax1.Position(3) ax2.Position(4)]);
%         set(ax2,'xlim',[20 70],'ylim',[0 5000],'yticklabel','','ytick',0 ,'box','off');
%         uistack(ax1,'top');
%         grid on
%         grid minor
 




%% plot packaging velcoity
% vel_wt = [stat_wt(idx_good_wt).mean_vel_pack];
% vel_mp = [stat_Q163A(idx_good_Q163A).mean_vel_pack];
vel_wt = [stat_wt_100.mean_vel_pack];
vel_mp = [stat_mp_100.mean_vel_pack];

fig_vel = subplot(2,3,4);
hold on
h_vel_wt = histogram(vel_wt,'Normalization','pdf');
h_vel_mp = histogram(vel_mp,'Normalization','pdf');
h_vel_wt.BinWidth = 300; h_vel_mp.BinWidth = h_vel_wt.BinWidth;
[kde_wt, x_wt] = ksdensity(vel_wt,'bandwidth',h_vel_wt.BinWidth);
[kde_mp, x_mp] = ksdensity(vel_mp,'bandwidth',h_vel_wt.BinWidth);
plot(x_wt,kde_wt,'Color',blue);
plot(x_mp,kde_mp,'Color',red);
legend('WT','MP')
xlim([-200 2000])
xlabel('Mean Packaging Velocity (bp/s)')
ylabel('Probability Density')
set(gca,'XTick',[0      600     1200    1800],...
        'XTickLabel',{'0','600','1200','1800'});
% Probability density function estimate: The height of each bar is, 
% (number of observations in bin)/ (total number of observations * width of bin). 
% The area of each bar is the relative number of observations, 
% and the sum of the bar areas is 1.

%% plot pause frequency
% pf_wt = [stat_wt(idx_good_wt).num]./[stat_wt(idx_good_wt).sum_pack_length]*1000;
% pf_mp = [stat_Q163A(idx_good_Q163A).num]./[stat_Q163A(idx_good_Q163A).sum_pack_length]*1000;
pf_wt = [stat_wt_100.pause_freq];
pf_mp = [stat_mp_100.pause_freq];
FakeInf = 10^(-1);
pf_wt(pf_wt == 0) = FakeInf;
pf_mp(pf_mp == 0) = FakeInf;%eps = sthE-18;

fig_pf = subplot(2,3,5);
hold on
h_pf_wt = histogram(log10(pf_wt),'Normalization','pdf');
h_pf_mp = histogram(log10(pf_mp),'Normalization','pdf');
h_pf_wt.BinWidth = 0.5; 
h_pf_mp.BinWidth = h_pf_wt.BinWidth;

% [kde_wt, x_wt] = ksdensity(pf_wt);
% [kde_mp, x_mp] = ksdensity(pf_mp);
% plot(log10(x_wt),kde_wt,'Color',blue);
% plot(log10(x_mp),kde_mp,'Color',red);

xlabel('Number of Pauses per kb')
ylabel('Probability Density')
legend('WT','MP')
set(gca,'XTick',[-1 0 1 2],...
        'XTickLabel',{'10^{-1}','10^0','10^1','10^2'});
xlim([-1 2])
%% plot relationship between packaging velocity and pause frequency
% pf_wt = [stat_wt(idx_good_wt).num]./[stat_wt(idx_good_wt).sum_pack_length]*1000;
% pf_mp = [stat_Q163A(idx_good_Q163A).num]./[stat_Q163A(idx_good_Q163A).sum_pack_length]*1000;
pf_wt = [stat_wt_100.pause_freq];
pf_mp = [stat_mp_100.pause_freq];

fig_velvspf = subplot(2,3,6);
hold on
FakeInf = 2E-2;
pf_wt(pf_wt == 0) = FakeInf;
pf_mp(pf_mp == 0) = FakeInf;%eps = sthE-18;
h_velvspf_wt = plot(vel_wt,pf_wt,'o','Color',blue);
h_velvspf_mp = plot(vel_mp,pf_mp,'o','Color',red);
set(gca,'yscale','log','xscale','log');
legend('WT','MP')
xlabel('Mean Packaging Velocity (bp/s)')
ylabel('Number of Pauses per kb')
xlim([100 3000])
ylim([FakeInf 100])
set(gca,'YTick',[FakeInf 1E-1 1 1E1 1E2],...
        'YTickLabel',{'0','10^{-1}','10^0','10^1','10^2'});
