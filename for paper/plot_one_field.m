
function plot_one_field(struc1,struc2,fld,unit)
%% plot spread and kernel density estimate
exclude0 = 0; % or other modification
ylog = 0;
mod = 0;
arr1 = [struc1.(fld)];
arr2 = [struc2.(fld)];
% exclude 0 if needed
if exclude0 
    num1 = find(arr1==0);   arr01 = arr1(num1); arr1(num1)=[];
    num2 = find(arr2==0);   arr02 = arr2(num2); arr2(num2)=[];
end
if mod
    arr1 = arr1*1000;
    arr2 = arr2*1000;
end

figure()
set(gcf,'Name',[fld 'dist.'])
subplot(1,2,1)
plotSpread({arr1,arr2}, ...
    'xNames', {'WT', 'Q163A'}, 'distributionColors',{[0,0.447,0.741],[0.85 0.325 0.098]},...
    'distributionMarkers', {'o', 'o'});
ax1=gca;
if exclude0
    plotSpread({arr01,arr02}, ...
    'xNames', {'WT', 'Q163A'}, 'distributionColors',{[0,0.447,0.741],[0.85 0.325 0.098]},...
    'distributionMarkers', {'.', '.'});
end
if ylog set(gca, 'YScale', 'log')
end

ylabel([line2space(fld) ' (' unit ')'])
% [H,P]=kstest2(arr1,arr2,'tail','larger')
[~,~,band_wt] = ksdensity(arr1);
[~,~,band_mp] = ksdensity(arr2);
[kde_wt, x_wt] = ksdensity(arr1,'Bandwidth',min(band_wt,band_mp));
[kde_mp, x_mp] = ksdensity(arr2,'Bandwidth',min(band_wt,band_mp));
subplot(1,2,2)
plot(kde_wt,x_wt,kde_mp,x_mp)
if ylog set(gca, 'YScale', 'log') 
end
ax2=gca;
linkaxes([ax1,ax2],'y');
xlabel('Probability Density')
title('Kernel Density Estimate')
legend({['WT,Mean=' num2str(mean(arr1)) ',N=' num2str(length(arr1))],...
    ['MP,Mean=' num2str(mean(arr2)) ',N=' num2str(length(arr2))]})
end
function str = line2space(str)
    str(find(str == '_'))=' ';
end