%% Supplementay Figs
set(0,'DefaultLineMarkerSize',6);
set(0,'defaultAxesFontSize',20)
set(0, 'DefaultAxesLineWidth', 1.5)
figure('Name','Supp Pause')

%% plot relationship between packaging velocity and pause frequency
pf_wt = [stat_wt_100.pause_freq];
pf_mp = [stat_mp_100.pause_freq];
temp = find([stat_mp_100.mean_vel_pause]<-100);

fig_velvspf = subplot(1,3,1);

hold on
FakeInf = 10^(-1);
pf_wt(pf_wt == 0) = FakeInf;
pf_mp(pf_mp == 0) = FakeInf;%eps = sthE-18;
h_velvspf_wt = plot(vel_wt,pf_wt,'o','Color',blue);
h_velvspf_mp = plot(vel_mp,pf_mp,'o','Color',red);
h_velvspf_hh = plot(vel_mp(temp),pf_mp(temp),'*k');
set(gca,'yscale','log','xscale','log');
legend('WT','MD')
xlabel('Mean Packaging Velocity (bp/s)')
ylabel('Number of Pauses per kb')
xlim([200 3000])
ylim([FakeInf 100])
set(gca,'YTick',[FakeInf 10^(-0.5) 10^0 10^(0.5) 10^1 10^(1.5)],...
        'YTickLabel',{'0','10^{-0.5}','10^0','10^{0.5}','10^1','10^{1.5}'},...
        'XTick',[200 600 1000 1800],...
        'XTickLabel',{'200','600','1000','1800'});
    xlim([200 1800])
    ylim([10^(-1.1) 10^(1.6)])

    
%% plot pause vel
fig_vpause = subplot(1,3,2);

nonzero_mp = find([stat_mp_100.mean_vel_pause]~=0);
nonzero_wt = find([stat_wt_100.mean_vel_pause]~=0);
temp = find([stat_mp_100(nonzero_mp).mean_vel_pause]<-100);
catIdx = [ones(length(nonzero_mp),1);3*ones(length(nonzero_wt),1)];
catIdx(temp)=2;
catCol=[0.85 0.325 0.098; 0 0 0; 0 0.447 0.741];
catMark = num2cell('o*o',1);


subplot(1,3,2);
plotSpread({[stat_mp_100(nonzero_mp).mean_vel_pause],[stat_wt_100(nonzero_wt).mean_vel_pause]}, 'xNames', {'MD','WT'},...
    'categoryIdx',catIdx,...
'xyor','flipped','categoryColors',catCol,...
'categoryMarkers',catMark,'spreadWidth',1.25)

xlim([-520 200])
ax1=gca;xlabel('Mean Pausing Velocity (bp/s)')
set(gca, 'xscale','linear')

%% plot pause duration
fig_duration = subplot(1,3,3);
plotSpread({[stat_mp_100(nonzero_mp).mean_pause_duration],[stat_wt_100(nonzero_wt).mean_pause_duration]}, 'xNames', {'MD','WT'},...
'xyor','flipped','categoryIdx',catIdx,...
'categoryColors',catCol,'categoryMarkers',catMark)
ax1=gca;xlabel('Mean Pause Duration (s)')
set(gca, 'xscale','log')
%ax2 = gca;
%linkaxes([ax1,ax2],'y')













%% Supplementay Figs
set(0,'DefaultLineMarkerSize',6);
set(0,'defaultAxesFontSize',20)
set(0, 'DefaultAxesLineWidth', 1.5)
figure('Name','Supp Restart')

%% plot relationship between packaging velocity and pause frequency
pf_wt = [stat_wt_100.pause_freq];
pf_mp = [stat_mp_100.pause_freq];
temp = find([stat_mp_100.restart]>15);

fig_velvspf = subplot(1,3,1);

hold on
FakeInf = 10^(-1);
pf_wt(pf_wt == 0) = FakeInf;
pf_mp(pf_mp == 0) = FakeInf;%eps = sthE-18;
h_velvspf_wt = plot(vel_wt,pf_wt,'o','Color',blue);
h_velvspf_mp = plot(vel_mp,pf_mp,'o','Color',red);
h_velvspf_hh = plot(vel_mp(temp),pf_mp(temp),'*k');
set(gca,'yscale','log','xscale','log');
legend('WT','MD')
xlabel('Mean Packaging Velocity (bp/s)')
ylabel('Number of Pauses per kb')
xlim([200 3000])
ylim([FakeInf 100])
set(gca,'YTick',[FakeInf 10^(-0.5) 10^0 10^(0.5) 10^1 10^(1.5)],...
        'YTickLabel',{'0','10^{-0.5}','10^0','10^{0.5}','10^1','10^{1.5}'},...
        'XTick',[200 600 1000 1800],...
        'XTickLabel',{'200','600','1000','1800'});
    xlim([200 1800])
    ylim([10^(-1.1) 10^(1.6)])

    
%% plot pause vel
fig_vpause = subplot(1,3,2);

temp = find([stat_mp_100(nonzero_mp).restart]>15);
nonzero_mp = find([stat_mp_100.mean_vel_pause]~=0);
nonzero_wt = find([stat_wt_100.mean_vel_pause]~=0);
catIdx = [ones(length(nonzero_mp),1);3*ones(length(nonzero_wt),1)];
catIdx(temp)=2;
catCol=[0.85 0.325 0.098; 0 0 0; 0 0.447 0.741];
catMark = num2cell('o*o',1);


subplot(1,3,2);
plotSpread({[stat_mp_100(nonzero_mp).mean_vel_pause],[stat_wt_100(nonzero_wt).mean_vel_pause]}, 'xNames', {'MD','WT'},...
    'categoryIdx',catIdx,...
'categoryColors',catCol,...
'xyor','flipped','categoryMarkers',catMark,'spreadWidth',1.25)

xlim([-520 200])
xlabel('Mean Pausing Velocity (bp/s)')
set(gca, 'xscale','linear')

%% plot pause duration
fig_duration = subplot(1,3,3);
plotSpread({[stat_mp_100(nonzero_mp).mean_pause_duration],[stat_wt_100(nonzero_wt).mean_pause_duration]}, 'xNames', {'MD','WT'},...
    'xyor','flipped','categoryIdx',catIdx,...
'categoryColors',catCol,'categoryMarkers',catMark)
ax1=gca;xlabel('Mean Pause Duration (s)')
set(gca, 'xscale','log')
%ax2 = gca;
%linkaxes([ax1,ax2],'y')





