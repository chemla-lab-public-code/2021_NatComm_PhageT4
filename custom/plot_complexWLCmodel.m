function [WLC_ext F] = plot_complexWLCmodel(max_force,dsDNA1,ssDNA1,ssDNA2,fake_offset)
% construct help


%% create the WLC model

F = 0:0.5:max_force;
[WLC_param] = WLC_parameters;

        bp = 1*WLC_param.hds*XWLCContour(F,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext = dsDNA1.*bp;
        
%         bp2 = 1*WLC_param.hds*XWLCContour(F/2,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
%         WLC_ext2 = dsDNA2.*bp2;
        
        nt1 = 1*WLC_param.hss*XWLCContour(F,WLC_param.Pss,WLC_param.Sss,WLC_param.kT); % nm/nt
        WLC_extss1 = ssDNA1.*nt1;
        
        nt2 = 1*WLC_param.hss*XWLCContour(0.5.*F,WLC_param.Pss,WLC_param.Sss,WLC_param.kT); % nm/nt
        WLC_extss2 = ssDNA2.*nt2;

        WLC_ext = WLC_ext + WLC_extss1 + WLC_extss2;

WLC_ext = WLC_ext + fake_offset;



%% plot
figure()
plot(WLC_ext, F, '--r', 'LineWidth', 2)

end