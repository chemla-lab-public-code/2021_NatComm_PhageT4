function data = plot_TrapSep_ext_F_vs_Time(trapData)
f = figure('Name', trapData.file(1:end-4));



directory = trapData.path(1:end-7);
Date = str2num(trapData.file(1:6));
stgTimeFilenum = str2num(trapData.file(8:10));
[trapData.path trapData.file(1:end-4) '_stagetime.txt'];
ifstage = exist([trapData.path trapData.file(1:end-4) '_stagetime.txt'],'file');
if ifstage
    [stgTime, stgPos] = StageMoveTime(directory, Date, stgTimeFilenum);
end



f1 = subplot(3,1,1);
plot(trapData.time, trapData.TrapSep)
legend('TapSep')
title(['Feedback\_'  trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' Trap Separation(nm) vs Time(s)']);
ylim([min(trapData.TrapSep)-50 max(trapData.TrapSep)+50])

if ifstage
ylims = get(gca,'ylim');
for i = 2:length(stgTime)-1
    line([stgTime(i) stgTime(i)], ylim);
    text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
end
end



f2 = subplot(3,1,2);
plot(trapData.time, trapData.DNAext_X)
hold on
plot(trapData.time, trapData.DNAext_Y)
legend('DNAext_X','DNAext_Y')
title(['Feedback\_'  trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' DNA Extension(nm) vs Time(s)']);
if trapData.scandir == 1
ylim([min(trapData.DNAext_X)-50 max(trapData.DNAext_X)+50])
elseif trapData.scandir == 0
ylim([min(trapData.DNAext_Y)-50 max(trapData.DNAext_Y)+50]) 
end


if ifstage
ylims = get(gca,'ylim');
for i = 2:length(stgTime)-1
    line([stgTime(i) stgTime(i)], ylim);
    text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
end
end



f3 = subplot(3,1,3);

plot(trapData.time, (trapData.force_X))
hold on
plot(trapData.time, (trapData.force_Y))
legend('force_X','force_Y')
title(['Feedback\_'  trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' Force(pN) vs Time(s)']);
% ylim([min(trapData.DNAext_Y)-0.5 max(trapData.DNAext_Y)+0.5]) 
if trapData.scandir == 1
ylim([min(trapData.force_X)-0.5 max(trapData.force_X)+0.5])
elseif trapData.scandir == 0
ylim([min(trapData.force_Y)-0.5 max(trapData.force_Y)+0.5]) 
end


if ifstage
ylims = get(gca,'ylim');
for i = 2:length(stgTime)-1
    line([stgTime(i) stgTime(i)], ylim);
    text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
end
end


linkaxes([f1,f2,f3],'x')


end
