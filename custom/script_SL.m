%%
%     num_pause
%     mean_pause_duration
%     std_pause_duration
%     sem_pause_duration
%     mean_pause_unpack
%     std_pause_unpack
%     sem_pause_unpack
%     mean_pack_length
%     std_pack_length
%     sem_pack_length
%     mean_Vel_pack
%     std_Vel_pack
%     sem_Vel_pack
%     mean_Vel_pause
%     std_Vel_pause
%     sem_Vel_pause
%     num_slip
%     mean_slip_Velocity
%     std_slip_Velocity
%     sem_slip_Velocity
    
%%  stat_script
% fprintf('wt_mean_Vel_pack = %d\n',mean([stat_wt.mean_Vel_pack]))
% fprintf('T287D_mean_Vel_pack = %d\n',mean([stat_T287D.mean_Vel_pack]))
% fprintf('Q163A_mean_Vel_pack = %d\n',mean([stat_Q163A.mean_Vel_pack]))
% fprintf('wt_num_pause = %d\n',mean([stat_wt.num_pause]))
% fprintf('T287D_num_pause = %d\n',mean([stat_T287D.num_pause]))
% fprintf('Q163A_num_pause = %d\n',mean([stat_Q163A.num_pause]))
% fprintf('wt_mean_pause_duration = %d\n',mean([stat_wt.mean_pause_duration]))
% fprintf('T287D_mean_pause_duration = %d\n',mean([stat_T287D.mean_pause_duration]))
% fprintf('Q163A_mean_pause_duration = %d\n',mean([stat_Q163A.mean_pause_duration]))


fprintf('mean velocity of SL_wt is %d +/- %d\n', mean(SL_wt),std(SL_wt))
fprintf('mean velocity of VK_wt is %d +/- %d\n', mean(VK_wt),std(VK_wt))
fprintf('mean velocity of SL_T287D is %d +/- %d\n', mean(SL_T287D),std(SL_T287D))
fprintf('mean velocity of SL_Q163A is %d +/- %d\n', mean(SL_Q163A),std(SL_Q163A))
% fprintf('mean velocity of VK_T287D is %d +/- %d\n', mean(VK_T287D),std(VK_T287D))

%% SL
f1=figure();
for i = 1:length(stat_wt_output)
plot(stat_wt_output(i).mean_Vel_pack,stat_wt_output(i).num_pause,'bo')
hold on
end
for i = 1:length(stat_T287D_output)
plot(stat_T287D_output(i).mean_Vel_pack,stat_T287D_output(i).num_pause,'ro')
hold on
end
for i = 1:length(stat_Q163A_output)
plot(stat_Q163A_output(i).mean_Vel_pack,stat_Q163A_output(i).num_pause,'go')
hold on
end
title('SL\_wt\_blue\_T287\_red\_Q163A\_green: mean\_Vel\_pack vs. num\_pause')
xlabel('SL:mean\_Vel\_pack (bp/s)')
ylabel('SL:num\_pause (a.u.)')

f2=figure();
for i = 1:length(stat_wt_output)
plot(stat_wt_output(i).mean_Vel_pack,stat_wt_output(i).mean_pack_length,'bo')
hold on
end
for i = 1:length(stat_T287D_output)
plot(stat_T287D_output(i).mean_Vel_pack,stat_T287D_output(i).mean_pack_length,'ro')
hold on
end
for i = 1:length(stat_Q163A_output)
plot(stat_Q163A_output(i).mean_Vel_pack,stat_Q163A_output(i).mean_pack_length,'go')
hold on
end
title('SL\_wt\_blue\_T287\_red\_Q163A\_green: mean\_Vel\_pack vs. mean\_pack\_length')
xlabel('SL:mean\_Vel\_pack (bp/s)')
ylabel('SL:mean\_pack\_legnth (bp)')

f3=figure();
for i = 1:length(stat_wt_output)
plot(stat_wt_output(i).mean_Vel_pack,stat_wt_output(i).mean_pause_duration,'bo')
hold on
end
for i = 1:length(stat_T287D_output)
plot(stat_T287D_output(i).mean_Vel_pack,stat_T287D_output(i).mean_pause_duration,'ro')
hold on
end
for i = 1:length(stat_Q163A_output)
plot(stat_Q163A_output(i).mean_Vel_pack,stat_Q163A_output(i).mean_pause_duration,'go')
hold on
end
title('SL\_wt\_blue\_T287\_red\_Q163A\_green: mean\_Vel\_pack vs. mean\_pause\_duration')
xlabel('SL\_wt:mean\_Vel\_pack (bp/s)')
ylabel('SL\_wt:mean\_pause\_duration (s)')

%% VK
VK_wt_output = [];
i = 0;
for aa = [aa03 aa04 aa05 aa06 aa07 aa08 aa25 aa26 aa27 aa28 aa29 aa30 aa31 aa32 aa33 aa34 aa35 aa36 aa37 aa38 aa39 aa40]
	for k = 1:length(aa)
		i = i+1;
		VK_wt_output(i).mean_Vel_pack = aa{k}.mean_vel_pack;
        VK_wt_output(i).num_pause = aa{k}.num;
        VK_wt_output(i).mean_pause_duration = aa{k}.mean_duration;
        VK_wt_output(i).mean_pack_length = aa{k}.mean_pack_length;
        
	end
end

figure(f1);
for i = 1:length(VK_wt_output)
plot(VK_wt_output(i).mean_Vel_pack,VK_wt_output(i).num_pause/3,'m+')
hold on
end
% title('VK_wt:mean\_Vel\_pack vs. num\_pause')
% xlabel('VK_wt:mean\_Vel\_pack (bp/s)')
% ylabel('VK_wt:num\_pause (a.u.)')

figure(f2);
for i = 1:length(VK_wt_output)
plot(VK_wt_output(i).mean_Vel_pack,VK_wt_output(i).mean_pack_length,'m+')
hold on
end

figure(f3);
for i = 1:length(VK_wt_output)
plot(VK_wt_output(i).mean_Vel_pack,VK_wt_output(i).mean_pause_duration,'m+')
hold on
end
% title('VK\_wt:mean\_Vel\_pack vs. mean\_pause\_duration')
% xlabel('VK\_wt:mean\_Vel\_pack (bp/s)')
% ylabel('VK\_wt:mean\_pause\_duration (s)')

% figure()
% for i = 1:length(stat)
% plot(stat(i).mean_Vel_pause,stat(i).num_pause,'bo')
% hold on
% end
% title('mean\_Vel\_pause vs. num\_pause')
% xlabel('mean\_Vel\_pause (bp/s)')
% ylabel('num\_pause (a.u.)')
% 
% figure()
% for i = 1:length(stat)
% plot(stat(i).mean_Vel_pause,stat(i).mean_pause_duration,'bo')
% hold on
% end
% title('mean\_Vel\_pause vs. mean\_pause\_duration')
% xlabel('mean\_Vel\_pause (bp/s)')
% ylabel('mean\_pause\_duration (s)')

% Method: Silverman's rule of thumb
% to determin the band width of kernel density estimation

% figure(1)
% for i = 1:length(stat)
%     hold on
%     plot(stat_T287D(i).mean_Vel_pack,stat(i).num_pause,'ro')
% end
% title('wt:mean\_Vel\_pack vs. num\_pause')
% xlabel('wt:mean\_Vel\_pack (bp/s)')
% ylabel('wt:num\_pause (a.u.)')
% 




