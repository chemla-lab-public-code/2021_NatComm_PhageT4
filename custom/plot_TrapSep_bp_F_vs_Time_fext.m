function plot_TrapSep_bp_F_vs_Time_fext(directory, Date, calarray, calnumber, offsetnumber, fbnumber, fextnumber,fitrange)

global fbslash
if nargin ~= 8
    error('Not enough input: directory,Date,calarray,cal,off,fb,fext.')
end
%common preprocessing
strDate = num2str(Date);
startpath = [directory strDate fbslash];

%% get calibration data
calFilename = [strDate '_' num2str(calnumber,'%03d') '.dat'];
if ~exist([startpath calFilename],'file')
    calnumber = GetCalNumber(directory, Date, fbnumber);
    warning(['Reset calnum to ' num2str(calnumber) ', fb ' num2str(fbnumber) ', fext ' num2str(fextnumber)])
end
calData = Calibrate(directory, Date, calarray, calnumber, 0, 0);

%% get fb data
traceFilename = [strDate '_' num2str(fbnumber,'%03d') '.dat'];
if ~exist([startpath traceFilename],'file')
    error(['Wrong File Path as a Trace: ' startpath traceFilename])
end
traceData = ReadJointFile(startpath,traceFilename);
traceData = trap_data_nmConversion(traceData);

traceData.calnumber = calnumber;
traceData.calarray = [calData.beadA calData.beadB calData.avwin calData.f_xyhi calData.f_sumhi calData.f_low];
% get offset data if needed
traceData.offsetnumber = offsetnumber;
offsetFilename = [strDate '_' num2str(offsetnumber,'%03d') '.dat'];
if ~exist([startpath offsetFilename],'file')
    offsetnumber = calnumber + 1;
    offsetFilename = [strDate '_' num2str(offsetnumber,'%03d') '.dat'];
    if ~exist([startpath offsetFilename],'file')
        error('No Offset File Exists next to Calibrateion File.')
    end
end
offsetData = ReadJointFile(startpath, offsetFilename);
offsetData = trap_data_nmConversion(offsetData); % convert from V or MHz to nm
traceData = force_offset_subtraction(traceData, calData, offsetData);


%% get fext data
fextFilename = [strDate '_' num2str(fextnumber,'%03d') '.dat'];
if ~exist([startpath traceFilename],'file')
    error(['Wrong File Path as a Trace: ' startpath fextFilename])
end
fextData = ReadJointFile(startpath,fextFilename);
fextData = trap_data_nmConversion(fextData);

fextData.calnumber = calnumber;
fextData.calarray = [calData.beadA calData.beadB calData.avwin calData.f_xyhi calData.f_sumhi calData.f_low];
% get offset data if needed
fextData.offsetnumber = offsetnumber;

offsetFilename = [strDate '_' num2str(offsetnumber,'%03d') '.dat'];
if ~exist([startpath offsetFilename],'file')
    offsetnumber = calnumber + 1;
    offsetFilename = [strDate '_' num2str(offsetnumber,'%03d') '.dat'];
    if ~exist([startpath offsetFilename],'file')
        error('No Offset File Exists next to Calibrateion File.')
    end
end
offsetData = ReadJointFile(startpath, offsetFilename);
offsetData = trap_data_nmConversion(offsetData); % convert from V or MHz to nm
fextData = force_offset_subtraction(fextData, calData, offsetData);





% plot force trace,  DNAbp trace, fext, 
fake_offset = 0;

f = figure('Name', traceData.file(1:end-4));

directory = traceData.path(1:end-7);
Date = str2num(traceData.file(1:6));
stgTimeFilenum = str2num(traceData.file(8:10));
ifstage = exist([traceData.path traceData.file(1:end-4) '_stagetime.txt'],'file');
if ifstage
    [stgTime, stgPos] = StageMoveTime(directory, Date, stgTimeFilenum);
end

f1 = subplot(3,2,1);
    if ifstage
        stgTime;
        ylims = [min(traceData.TrapSep)-50 max(traceData.TrapSep)+50];
        for i = 2:length(stgTime)-1
            line([stgTime(i) stgTime(i)], ylim);
            text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
        %     patch([stgTime(i) stgTime(i-1) stgTime(i-1) stgTime(i)], [ylims(1) ylims(1) ylims(2) ylims(2)],[1,1,1-0.05*i])
        end
    end
    hold on
    plot(traceData.time, traceData.TrapSep)
    legend('TrapSep')
    title(['Feedback\_'  traceData.file(1:end-8) '\' traceData.file(end-7:end-4) ' Trap Separation(nm) vs Time(s)']);
    ylim([min(traceData.TrapSep)-50 max(traceData.TrapSep)+50])
    grid on


f2 = subplot(3,2,3);
    [WLC_param] = WLC_parameters;
    %   fake_offset = 280; % unit nm;
    if traceData.scandir == 1
        bp = 1*WLC_param.hds*XWLCContour(traceData.force_X,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        traceData.DNAext_X = traceData.DNAext_X - fake_offset;
        traceData.DNAbp = (traceData.DNAext_X-fake_offset)./bp;
    elseif traceData.scandir == 0 
        bp = 1*WLC_param.hds*XWLCContour(traceData.force_Y,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        traceData.DNAext_Y = traceData.DNAext_Y - fake_offset;
        traceData.DNAbp = traceData.DNAext_Y./bp;
    end
    plot(traceData.time, traceData.DNAbp)
    legend('DNAbp')
    title(['Feedback\_'  traceData.file(1:end-8) '\' traceData.file(end-7:end-4) ' DNA Extension(bp) vs Time(s)']);
    ylim([min(traceData.DNAbp)-50 max(traceData.DNAbp)+50])
    ylim([-300 5000])
    ylim([0 4000])
    grid on

    if ifstage
        ylims = get(gca,'ylim');
        for i = 2:length(stgTime)-1
            line([stgTime(i) stgTime(i)], ylim);
            text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
        end
    end

f3 = subplot(3,2,5);
    plot(traceData.time, traceData.force_X,'Color',[0.85,0.325,0.098])
    hold on
    plot(traceData.time, traceData.force_Y,'Color',[0,0.447,0.741])
    plot(traceData.time, traceData.force_AX,'-','Color',[0.6 0.839 1]);
    plot(traceData.time, traceData.force_BX,'-','Color',[0.6 0.839 1]);
    
    legend('force_X','force_Y','AX','BX')
    title(['Feedback\_'  traceData.file(1:end-8) '\' traceData.file(end-7:end-4) ' Force(pN) vs Time(s) Cal\_' num2str(traceData.calnumber,'%03d') ' Offset\_' num2str(traceData.offsetnumber,'%03d')]);
    % ylim([min(traceData.DNAext_Y)-0.5 max(traceData.DNAext_Y)+0.5]) 
    if traceData.scandir == 1
        ylim([min(traceData.force_X)-0.5 max(traceData.force_X)+0.5])
        ylim([-6 6])
    elseif traceData.scandir == 0
        ylim([min(traceData.force_Y)-0.5 max(traceData.force_Y)+0.5]) 
        ylim([-6 6])
    end
    grid on

    if ifstage
    ylims = get(gca,'ylim');
        for i = 2:length(stgTime)-1
            line([stgTime(i) stgTime(i)], ylim);
            text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
        end
    end

linkaxes([f1,f2,f3],'x')



% plot fext curve
f4 = subplot(3,2,2:2:6);
        dsDNA1 = 3.4e3;%+906;
        fitoffset = 1;
        fake_offset = 0;
        stdF = [2 15];

%% create the WLC model
max_force = max([fextData.force_AX, fextData.force_AY, fextData.force_BX, fextData.force_BY]);
F = 0:0.5:max_force;
[WLC_param] = WLC_parameters;

    bp = 1*WLC_param.hds*XWLCContour(F,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
    WLC_ext = dsDNA1.*bp;
    bp2 = 1*WLC_param.hds*XWLCContour(F/2,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
    WLC_ext2 = dsDNA1.*bp2;
    bp3 = 1*WLC_param.hds*XWLCContour(F/3,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
    WLC_ext3 = dsDNA1.*bp3;
    bp4 = 1*WLC_param.hds*XWLCContour(F/4,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
    WLC_ext4 = dsDNA1.*bp4;
    ntss = 1*WLC_param.hss*XWLCContour(F/2,WLC_param.Pss,WLC_param.Sss,WLC_param.kT); % nm/nt
    WLC_extss = dsDNA1.*ntss;
     
%% plot the data
l = length(fextData.TrapSep);
TrapSepFilt=zeros(1,l);
gauge = 14;
for ii = 1:l
    windowsize = length(max(1,ii-gauge):min(l,ii+gauge));
    for jj = max(1,ii-gauge):min(l,ii+gauge)
        TrapSepFilt(ii) = TrapSepFilt(ii) + fextData.TrapSep(jj);
    end
    TrapSepFilt(ii)=TrapSepFilt(ii)/windowsize;
end

% remove jumps in trap position
TrapSepFilt_diff = diff(TrapSepFilt);
normalized_diff = diff(TrapSepFilt)/mean(TrapSepFilt_diff(TrapSepFilt_diff>0));
TrapSepFiltClean = TrapSepFilt(normalized_diff > 0.8 | normalized_diff < -0.8);
fextData.force_AX = fextData.force_AX(normalized_diff > 0.8 | normalized_diff < -0.8);
fextData.force_BX = fextData.force_BX(normalized_diff > 0.8 | normalized_diff < -0.8);
fextData.force_AY = fextData.force_AY(normalized_diff > 0.8 | normalized_diff < -0.8);
fextData.force_BY = fextData.force_BY(normalized_diff > 0.8 | normalized_diff < -0.8);
fextData.DNAext_X = fextData.DNAext_X(normalized_diff > 0.8 | normalized_diff < -0.8);
fextData.DNAext_Y = fextData.DNAext_Y(normalized_diff > 0.8 | normalized_diff < -0.8);
% find the top peaks
[numMAX, indMAX] = findpeaks(TrapSepFiltClean);
% find the bottom peaks
TrapSepInverse = [-max(numMAX) -TrapSepFiltClean -max(numMAX)];
[numMIN, indMIN] = findpeaks(TrapSepInverse);
indMIN = indMIN-1;
% count number of raster scans
rasterNUM = (max([length(indMIN) length(indMAX)]))/2;
% get the force array
if fextData.scandir == 1
    FA = fextData.force_AX;
    FB = fextData.force_BX;
    FM = (fextData.force_AX+fextData.force_BX)./2;
    DNAext = fextData.DNAext_X;
elseif fextData.scandir == 0
    FA = fextData.force_AY;
    FB = fextData.force_BY;
    FM = (fextData.force_AY+fextData.force_BY)./2;
    DNAext = fextData.DNAext_Y;
end

% get fake offset
if ~isnan(fitrange)
    DNAext_offset = [];
    DNAext_offset = mean(DNAext(FM > fitrange(1) & FM < fitrange(2)));
    WLC_offset = mean(WLC_ext(F > fitrange(1) & F < fitrange(2)));
    fake_offset = DNAext_offset - WLC_offset;
    % substract the fake offset
%     DNAext = DNAext-(0);
    DNAext = DNAext-fake_offset;
end
fextData.fake_offset = fake_offset;
fextData.fitrange = fitrange;
fextData.stdF = stdF;

%% plot

plot(WLC_ext, F, '--r', 'LineWidth', 2)

for ii = 1:1:rasterNUM
    indxS = indMIN(ii); % far point
    indxE = indMAX(ii); % close point
    indxEE = indMIN(ii+1);
    hold on
    plot(DNAext(indxS:indxE),FM(indxS:indxE),'color', [0.9100    0.4100    0.1700], 'LineWidth', 1.5)
    hold on
    plot(DNAext(indxE:indxEE),FM(indxE:indxEE), 'color', [0 0.4470 0.7410], 'LineWidth', 1.5)
end
if ~isnan(DNAext)
    xlim([min(DNAext)-50 max(DNAext)+50])
end
ylim([min(0,min(FM)) max(FM)*1.2])
xlabel('DNA extension (nm)')
ylabel('Force (pN)')

legend('WLC model','pull','restore','Location','NorthWest')

if fextData.oldtrap == 1
    instrument = 'Old-Trap';
elseif fextData.oldtrap == 0
    instrument = 'Fleezers';
end
% title({[ construct ' construct, ' instrument] ['data file: ' fextData.file(1:end-4)]})
title([ 'construct: PCR, instrument: FL, fext: ' fextData.file(1:end-8) '\' fextData.file(end-7:end-4) ', fakeoffset: ' num2str(fake_offset) 'nm, cal: ' num2str(fextData.calnumber,'%03d')])



