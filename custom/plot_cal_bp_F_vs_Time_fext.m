function [calData, fextData, traceData] = plot_cal_bp_F_vs_Time_fext(directory, Date, calarray, calnumber, offsetnumber, fbnumber, fextnumber,construct,fitrange)

global fbslash
if nargin ~= 9
    error('Not enough input: directory,Date,calarray,cal,off,fb,fext.')
end
%common preprocessing
strDate = num2str(Date);
startpath = [directory strDate fbslash];

%% get calibration data
calFilename = [strDate '_' num2str(calnumber,'%03d') '.dat'];
if ~exist([startpath calFilename],'file')
    calnumber = GetCalNumber(directory, Date, fbnumber);
    warning(['Reset calnum to ' num2str(calnumber) ', fb ' num2str(fbnumber) ', fext ' num2str(fextnumber)])
end
calData = Calibrate(directory, Date, calarray, calnumber, 0, 0);

%% get fb data
traceFilename = [strDate '_' num2str(fbnumber,'%03d') '.dat'];
if ~exist([startpath traceFilename],'file')
    error(['Wrong File Path as a Trace: ' startpath traceFilename])
end
traceData = ReadJointFile(startpath,traceFilename);
traceData = trap_data_nmConversion(traceData);

traceData.calnumber = calnumber;
traceData.calarray = [calData.beadA calData.beadB calData.avwin calData.f_xyhi calData.f_sumhi calData.f_low];
% get offset data if needed
traceData.offsetnumber = offsetnumber;
offsetFilename = [strDate '_' num2str(offsetnumber,'%03d') '.dat'];
if ~exist([startpath offsetFilename],'file')
    offsetnumber = calnumber + 1;
    offsetFilename = [strDate '_' num2str(offsetnumber,'%03d') '.dat'];
    if ~exist([startpath offsetFilename],'file')
        error('No Offset File Exists next to Calibrateion File.')
    end
end
offsetData = ReadJointFile(startpath, offsetFilename);
offsetData = trap_data_nmConversion(offsetData); % convert from V or MHz to nm
traceData = force_offset_subtraction(traceData, calData, offsetData);


%% get fext data
fextFilename = [strDate '_' num2str(fextnumber,'%03d') '.dat'];
if ~exist([startpath traceFilename],'file')
    error(['Wrong File Path as a Trace: ' startpath fextFilename])
end
fextData = ReadJointFile(startpath,fextFilename);
fextData = trap_data_nmConversion(fextData);

fextData.calnumber = calnumber;
fextData.calarray = [calData.beadA calData.beadB calData.avwin calData.f_xyhi calData.f_sumhi calData.f_low];
% get offset data if needed
fextData.offsetnumber = offsetnumber;

offsetFilename = [strDate '_' num2str(offsetnumber,'%03d') '.dat'];
if ~exist([startpath offsetFilename],'file')
    offsetnumber = calnumber + 1;
    offsetFilename = [strDate '_' num2str(offsetnumber,'%03d') '.dat'];
    if ~exist([startpath offsetFilename],'file')
        error('No Offset File Exists next to Calibrateion File.')
    end
end
offsetData = ReadJointFile(startpath, offsetFilename);
offsetData = trap_data_nmConversion(offsetData); % convert from V or MHz to nm
fextData = force_offset_subtraction(fextData, calData, offsetData);

%% plot calData
f = figure('Name', [traceData.file(1:end-4) '_cal_bp_F_fext']);

f_cal_AX = subplot(3,4,1);
loglog(calData.fitting.fA,calData.fitting.AXSpecRaw)
hold on
loglog(calData.fitting.fA,calData.fitting.predictedAX)
legend({['\kappa = ' num2str(calData.kappaAX,3) ','],...
        ['\alpha = ' num2str(calData.alphaAX,4) ',']},'Location','SouthWest');
%         ['Offset = ' num2str(calData.AXoffset,3)]},'Location','SouthWest');
xlim([30 80000])
ylim([.5E-10 .1E-7])
title ('AX')

f_cal_BX = subplot(3,4,2);
loglog(calData.fitting.fB,calData.fitting.BXSpecRaw)
hold on
loglog(calData.fitting.fB,calData.fitting.predictedBX)
legend({['\kappa = ' num2str(calData.kappaBX,3) ','],...
        ['\alpha = ' num2str(calData.alphaBX,4) ',']},'Location','SouthWest');
%         ['Offset = ' num2str(calData.BXoffset,3)]},'Location','SouthWest');
xlim([30 80000])
ylim([.5E-10 .1E-7])
title ('BX')

f_cal_AY = subplot(3,4,3);
loglog(calData.fitting.fA,calData.fitting.AYSpecRaw)
hold on
loglog(calData.fitting.fA,calData.fitting.predictedAY)
legend({['\kappa = ' num2str(calData.kappaAY,3) ','],...
        ['\alpha = ' num2str(calData.alphaAY,4) ',']},'Location','SouthWest');
%         ['Offset = ' num2str(calData.AYoffset,3)]},'Location','SouthWest');
xlim([30 80000])
ylim([.5E-10 .1E-7])
title ('AY')

f_cal_BY = subplot(3,4,4);
loglog(calData.fitting.fB,calData.fitting.BYSpecRaw)
hold on
loglog(calData.fitting.fB,calData.fitting.predictedBY)
legend({['\kappa = ' num2str(calData.kappaBY,3) ','],...
        ['\alpha = ' num2str(calData.alphaBY,4) ',']},'Location','SouthWest');
%         ['Offset = ' num2str(calData.BYoffset,3)]},'Location','SouthWest');
xlim([30 80000])
ylim([.5E-10 .1E-7])
title ('BY')


%% plot force trace,  DNAbp trace, fext, 
directory = traceData.path(1:end-7);
Date = str2num(traceData.file(1:6));
stgTimeFilenum = str2num(traceData.file(8:10));
ifstage = exist([traceData.path traceData.file(1:end-4) '_stagetime.txt'],'file');
if ifstage
    [stgTime, stgPos] = StageMoveTime(directory, Date, stgTimeFilenum);
end



%% plot force time trace
f_force = subplot(3,4,[9 10]);
    plot(traceData.time, traceData.force_X,'Color',[0.85,0.325,0.098])
    hold on
    plot(traceData.time, traceData.force_AY,'-','Color',[0.6 0.839 1])
    plot(traceData.time, traceData.force_BY,'-','Color',[0.6 0.839 1])
    plot(traceData.time, traceData.force_Y,'Color',[0,0.447,0.741])
    legend('force_X','force_Y')
    title(['PackTrace\_'  traceData.file(1:end-8) '\' traceData.file(end-7:end-4) ' Cal\_' num2str(traceData.calnumber,'%03d') ' Offset\_' num2str(traceData.offsetnumber,'%03d')]);
    xlabel('Time(s)')
    ylabel('Force(pN)')
    % ylim([min(traceData.DNAext_Y)-0.5 max(traceData.DNAext_Y)+0.5]) 
    if traceData.scandir == 1
        ylim([min(traceData.force_X)-0.5 max(traceData.force_X)+0.5])
        ylim([-6 6])
    elseif traceData.scandir == 0
        ylim([min(traceData.force_Y)-0.5 max(traceData.force_Y)+0.5]) 
        ylim([-6 6])
    end
    grid on

    if ifstage
    ylims = get(gca,'ylim');
        for i = 2:length(stgTime)-1
            line([stgTime(i) stgTime(i)], ylim);
            text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
        end
    end





%% plot fext curve
f_fext = subplot(3,4,[7 8 11 12]);
        dsDNA1 = construct;
        fitoffset = 1;
        fake_offset = 0;
        stdF = [2 15];

% create the WLC model
max_force = max([fextData.force_AX, fextData.force_AY, fextData.force_BX, fextData.force_BY]);
F = 0:0.5:max_force;
[WLC_param] = WLC_parameters;

    bp = 1*WLC_param.hds*XWLCContour(F,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
    WLC_ext = dsDNA1.*bp;
    bp2 = 1*WLC_param.hds*XWLCContour(F/2,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
    WLC_ext2 = dsDNA1.*bp2;
    bp3 = 1*WLC_param.hds*XWLCContour(F/3,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
    WLC_ext3 = dsDNA1.*bp3;
    bp4 = 1*WLC_param.hds*XWLCContour(F/4,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
    WLC_ext4 = dsDNA1.*bp4;
    ntss = 1*WLC_param.hss*XWLCContour(F/2,WLC_param.Pss,WLC_param.Sss,WLC_param.kT); % nm/nt
    WLC_extss = dsDNA1.*ntss;
     
% plot the data
l = length(fextData.TrapSep);
TrapSepFilt=zeros(1,l);
gauge = 14;
for ii = 1:l
    windowsize = length(max(1,ii-gauge):min(l,ii+gauge));
    for jj = max(1,ii-gauge):min(l,ii+gauge)
        TrapSepFilt(ii) = TrapSepFilt(ii) + fextData.TrapSep(jj);
    end
    TrapSepFilt(ii)=TrapSepFilt(ii)/windowsize;
end

% remove jumps in trap position
TrapSepFilt_diff = diff(TrapSepFilt);
normalized_diff = diff(TrapSepFilt)/mean(TrapSepFilt_diff(TrapSepFilt_diff>0));
TrapSepFiltClean = TrapSepFilt(normalized_diff > 0.8 | normalized_diff < -0.8);
fextData.force_AX = fextData.force_AX(normalized_diff > 0.8 | normalized_diff < -0.8);
fextData.force_BX = fextData.force_BX(normalized_diff > 0.8 | normalized_diff < -0.8);
fextData.force_AY = fextData.force_AY(normalized_diff > 0.8 | normalized_diff < -0.8);
fextData.force_BY = fextData.force_BY(normalized_diff > 0.8 | normalized_diff < -0.8);
fextData.DNAext_X = fextData.DNAext_X(normalized_diff > 0.8 | normalized_diff < -0.8);
fextData.DNAext_Y = fextData.DNAext_Y(normalized_diff > 0.8 | normalized_diff < -0.8);
% find the top peaks
[numMAX, indMAX] = findpeaks(TrapSepFiltClean);
% find the bottom peaks
TrapSepInverse = [-max(numMAX) -TrapSepFiltClean -max(numMAX)];
[numMIN, indMIN] = findpeaks(TrapSepInverse);
indMIN = indMIN-1;
% count number of raster scans
rasterNUM = (max([length(indMIN) length(indMAX)]))/2;
% get the force array
if fextData.scandir == 1
    FA = fextData.force_AX;
    FB = fextData.force_BX;
    FM = (fextData.force_AX+fextData.force_BX)./2;
    DNAext = fextData.DNAext_X;
elseif fextData.scandir == 0
    FA = fextData.force_AY;
    FB = fextData.force_BY;
    FM = (fextData.force_AY+fextData.force_BY)./2;
    DNAext = fextData.DNAext_Y;
end

% get fake offset
if ~isnan(fitrange)
    DNAext_offset = [];
    DNAext_offset = mean(DNAext(FM > fitrange(1) & FM < fitrange(2)));
    WLC_offset = mean(WLC_ext(F > fitrange(1) & F < fitrange(2)));
    fake_offset = DNAext_offset - WLC_offset;
    % substract the fake offset
%     DNAext = DNAext-(0);
    DNAext = DNAext-fake_offset;
end
fextData.fake_offset = fake_offset;
fextData.fitrange = fitrange;
fextData.stdF = stdF;

% plot

plot(WLC_ext, F, '--r', 'LineWidth', 2)

for ii = 1:1:rasterNUM
    indxS = indMIN(ii); % far point
    indxE = indMAX(ii); % close point
    indxEE = indMIN(ii+1);
    hold on
%     % pull A pull B
%     plot(DNAext(indxS:indxE),FA(indxS:indxE),'color', [0.9100    0.4100    0.1700], 'LineWidth', 1,'LineStyle','--')
%     plot(DNAext(indxS:indxE),FB(indxS:indxE),'color', [0.467 0.675 0.188], 'LineWidth', 1,'LineStyle','--')
%     % restore A restore B
%     plot(DNAext(indxE:indxEE),FA(indxE:indxEE),'color', [0 0.4470 0.7410], 'LineWidth', 1,'LineStyle','--')
%     plot(DNAext(indxE:indxEE),FB(indxE:indxEE),'color', [0.929 0.694 0.125], 'LineWidth', 1,'LineStyle','--')
%     % pull average restore average
%     plot(DNAext(indxS:indxE),FM(indxS:indxE),'color', [0.9100    0.4100    0.1700], 'LineWidth', 1.5)
%     plot(DNAext(indxE:indxEE),FM(indxE:indxEE), 'color', [0 0.4470 0.7410], 'LineWidth', 1.5)
    % A B
    plot(DNAext(indxS:indxEE),FA(indxS:indxEE),'color', [0.9100    0.4100    0.1700], 'LineWidth', 1.5)
    plot(DNAext(indxS:indxEE),FB(indxS:indxEE), 'color', [0 0.4470 0.7410], 'LineWidth', 1.5)
%     legend('WLC model','pullA','pull B','restore A','restore B','pull','restore','Location','NorthWest')
    legend('WLC model','A','B','Location','NorthWest')
end
if ~isnan(DNAext)
    xlim([min(DNAext)-50 max(DNAext)+50])
end
ylim([min(0,min(FM)) max(FM)*1.2])
xlabel('DNA extension (nm)')
ylabel('Force (pN)')



if fextData.oldtrap == 1
    instrument = 'Old-Trap';
elseif fextData.oldtrap == 0
    instrument = 'Fleezers';
end
% title({[ construct ' construct, ' instrument] ['data file: ' fextData.file(1:end-4)]})
title([ 'construct: PCR, fext: ' fextData.file(1:end-8) '\' fextData.file(end-7:end-4) ', fakeoffset: ' num2str(fake_offset) 'nm'])

%% plot DNAbp
f_DNAbp = subplot(3,4,[5 6]);
    fake_offset = fextData.fake_offset;
    
    [WLC_param] = WLC_parameters;
    %   fake_offset = 280; % unit nm;
    if traceData.scandir == 1
        bp = 1*WLC_param.hds*XWLCContour(traceData.force_X,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        traceData.DNAext_X_mod = traceData.DNAext_X - fake_offset;
        traceData.DNAbp = traceData.DNAext_X./bp;
        traceData.DNAbp_mod = traceData.DNAext_X_mod./bp;
    elseif traceData.scandir == 0 
        bp = 1*WLC_param.hds*XWLCContour(traceData.force_Y,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        traceData.DNAext_Y_mod = traceData.DNAext_Y - fake_offset;
        traceData.DNAbp = traceData.DNAext_Y./bp;
        traceData.DNAbp_mod = traceData.DNAext_Y_mod./bp;
    end
    plot(traceData.time, traceData.DNAbp)
    ylabel('bp')
    legend('DNA Extension')
%     title(['Feedback\_'  traceData.file(1:end-8) '\' traceData.file(end-7:end-4) ' DNA Extension(bp) vs Time(s)']);
    ylim([min(traceData.DNAbp)-50 max(traceData.DNAbp)+50])
    ylim([-300 5000])
    grid on

    if ifstage
        ylims = get(gca,'ylim');
        for i = 2:length(stgTime)-1
            line([stgTime(i) stgTime(i)], ylim);
            text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
        end
    end
    
    linkaxes([f_DNAbp f_force],'x')
%% evaluate_Newton3( fextData )
grade = (FA-FB).^2;
criterion = smooth(grade(indxS:indxEE),100);
% figure()
% plot(DNAext(indxS:indxEE),grade(indxS:indxEE),'Color',[0.8 0.8 0.8],'LineWidth', 2)
% hold on
% plot(DNAext(indxS:indxEE),smooth(grade(indxS:indxEE),100),'k', 'LineWidth', 1.5)
fextData.newton3 = length(criterion(criterion>0.2));

% 200211
% length(DNAext(indxS:indxEE))
% xL = DNAext(indxS:indxEE) ./WLC_param.hds./dsDNA1;
% length(xL)
% a = 0.25*(1-xL).^(-2);
% length(a)
% F_WLC = WLC_param.kT./WLC_param.Pds.*a-0.25+xL;
% length(F_WLC)
% fextData.ppt_error = FM(indxS:indxEE) - F_WLC;
% fextData.sum_error = sum(fextData.ppt_error);
% fextData.mean_eroor = sum(fextData.ppt_error)/(indxEE-indxS);
% fextData.std_error = std(fextData.ppt_error);
% plot(DNAext(indxS:indxEE),fextData.ppt_error)

curvexy = [WLC_ext; F]';
mapxy = [DNAext(indxS:indxEE);FM(indxS:indxEE)]';
[~,fextData.distance,~] = distance2curve(curvexy,mapxy,'linear');
f_fext;
plot(DNAext(indxS:indxEE),fextData.distance);



end




