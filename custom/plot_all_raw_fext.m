% 
% %% open fext figure manualy
% 
% for i = 2:29
%     a=get(figure(i),'CurrentAxes');
%     lines = get(a,'Children');
%     h1=get(a,'title');
%     titre=get(h1,'string');
% %     legre=titre(33:43);% for PCR construct
%     legre=titre(32:42);% for T4 construct
% %     fakeoffset = str2double(titre(58:end-12)); % for PCR construct
%     fakeoffset = str2double(titre(57:end-12)); % for T4 construct
%     figure(30)
%     hold on
%     plot(lines(1).XData+fakeoffset,lines(1).YData,'color', [0 0.4470 0.7410], 'LineWidth', 1.5,'DisplayName',[legre 'restore'])
% %     plot(lines(1).XData+fakeoffset,lines(1).YData,'color', [0 0 1], 'DisplayName',[legre 'restore'])
%     hold on
%     plot(lines(2).XData+fakeoffset,lines(2).YData,'color', [0.9100    0.4100    0.1700], 'LineWidth', 1.5,'DisplayName',[legre 'pull'])
% %     plot(lines(2).XData+fakeoffset,lines(2).YData,'color', [1 0 0], 'DisplayName',[legre 'pull'])
% end
% legend show

%% open fext figure manualy

for i = 1:110
    a=get(figure(i),'CurrentAxes');
    lines = get(a,'Children');
    h1=get(a,'title');
    titre=get(h1,'string');
    
    pos = strfind(titre, ':');
    posa = pos(2)+1;
    posb = pos(3)+1;
    if length(pos) ~= 4 
        display('more than 4 :')
        display(i)
    end
    pos = strfind(titre,'nm');
    pos3 = pos(1)-1;
    
    legre = titre(posa:posa+11)
    fakeoffset = str2double(titre(posb:pos3)); % for T4 construct
    figure(111)
    hold on
    plot(lines(1).XData+fakeoffset,lines(1).YData,'color', [0 0.4470 0.7410], 'LineWidth', 1.5,'DisplayName',[legre ' restore'])
%     plot(lines(1).XData+fakeoffset,lines(1).YData,'color', [0 0 1], 'DisplayName',[legre 'restore'])
    hold on
    plot(lines(2).XData+fakeoffset,lines(2).YData,'color', [0.9100    0.4100    0.1700], 'LineWidth', 1.5,'DisplayName',[legre ' pull'])
%     plot(lines(2).XData+fakeoffset,lines(2).YData,'color', [1 0 0], 'DisplayName',[legre 'pull'])
end
% legend show