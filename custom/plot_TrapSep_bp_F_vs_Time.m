function data = plot_TrapSep_bp_F_vs_Time(trapData,fake_offset)
if isnan(fake_offset)
    fake_offset = 0;
end
f = figure('Name', trapData.file(1:end-4));



directory = trapData.path(1:end-7);
Date = str2num(trapData.file(1:6));
stgTimeFilenum = str2num(trapData.file(8:10));
ifstage = exist([trapData.path trapData.file(1:end-4) '_stagetime.txt'],'file');
if ifstage
    [stgTime, stgPos] = StageMoveTime(directory, Date, stgTimeFilenum);
end



f1 = subplot(3,1,1);



if ifstage
    stgTime;
ylims = [min(trapData.TrapSep)-50 max(trapData.TrapSep)+50];

for i = 2:length(stgTime)-1
    line([stgTime(i) stgTime(i)], ylim);
    text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
%     patch([stgTime(i) stgTime(i-1) stgTime(i-1) stgTime(i)], [ylims(1) ylims(1) ylims(2) ylims(2)],[1,1,1-0.05*i])
end
end
hold on
plot(trapData.time, trapData.TrapSep)
legend('TrapSep')
title(['Feedback\_'  trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' Trap Separation(nm) vs Time(s)']);
ylim([min(trapData.TrapSep)-50 max(trapData.TrapSep)+50])
grid on


% if ifstage
%     stgTime
% ylims = get(gca,'ylim');
% for i = 2:length(stgTime)-1
%     line([stgTime(i) stgTime(i)], ylim);
%     text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
%     patch([stgTime(i) stgTime(i-1) stgTime(i-1) stgTime(i)], [ylims(1) ylims(1) ylims(2) ylims(2)],[0.01,0.01,0.01])
% end
% end



f2 = subplot(3,1,2);

    [WLC_param] = WLC_parameters;
%   fake_offset = 280; % unit nm;

    if trapData.scandir == 1
        bp = 1*WLC_param.hds*XWLCContour(trapData.force_X,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        trapData.DNAext_X = trapData.DNAext_X - fake_offset;
        trapData.DNAbp = (trapData.DNAext_X-fake_offset)./bp;
    elseif trapData.scandir == 0 
        bp = 1*WLC_param.hds*XWLCContour(trapData.force_Y,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        trapData.DNAext_Y = trapData.DNAext_Y - fake_offset;
        trapData.DNAbp = trapData.DNAext_Y./bp;
    end
        
plot(trapData.time, trapData.DNAbp)

legend('DNAbp')
title(['Feedback\_'  trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' DNA Extension(bp) vs Time(s)']);
ylim([min(trapData.DNAbp)-50 max(trapData.DNAbp)+50])
grid on









if ifstage
ylims = get(gca,'ylim');
for i = 2:length(stgTime)-1
    line([stgTime(i) stgTime(i)], ylim);
    text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
end
end



f3 = subplot(3,1,3);

plot(trapData.time, (trapData.force_X))
hold on
plot(trapData.time, (trapData.force_Y))
plot(trapData.time, (trapData.force_AX),'-')
plot(trapData.time, (trapData.force_BX),'.-')
legend('force_X','force_Y','force_AX','force_BX')
title(['Feedback\_'  trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' Force(pN) vs Time(s) Cal\_' num2str(trapData.calnumber,'%03d') ' Offset\_' num2str(trapData.offsetnumber,'%03d')]);
% ylim([min(trapData.DNAext_Y)-0.5 max(trapData.DNAext_Y)+0.5]) 
if trapData.scandir == 1
ylim([min(trapData.force_X)-0.5 max(trapData.force_X)+0.5])
elseif trapData.scandir == 0
ylim([min(trapData.force_Y)-0.5 max(trapData.force_Y)+0.5]) 
end
grid on


if ifstage
ylims = get(gca,'ylim');
for i = 2:length(stgTime)-1
    line([stgTime(i) stgTime(i)], ylim);
    text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
end
end


linkaxes([f1,f2,f3],'x')

data = trapData;
end
