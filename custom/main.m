%% [data_idx_output, stat]= main(datadirectory,calparams,data_idx)
function [data_idx_output, stat_idx_output]= main(directory,calarray,data_idx)
    [data_num, ~] = size(data_idx);
    clear data_idx_output
    clear stat_idx_output
    data_idx_output = zeros(data_num,6); % 7th column could be mean pack vel;
    data_idx_output(:,1:4) = data_idx(:,1:4);
%     stat_idx_output = repmat(struct('num_pause', NaN, ...
%         'mean_pause_duration', NaN, 'std_pause_duration', NaN, 'sem_pause_duration', NaN, ...
%         'mean_pause_unpack', NaN, 'std_pause_unpack', NaN, 'sem_pause_unpack', NaN, ...
%         'mean_pack_length', NaN, 'std_pack_length', NaN, 'sem_pack_length', NaN, ...
%         'mean_vel_pack', NaN, 'std_vel_pack', NaN, 'sem_vel_pack', NaN, ...
%         'mean_vel_pause', NaN, 'std_vel_pause', NaN, 'sem_vel_pause', NaN, ...
%         'num_slip', NaN, 'mean_slip_velocity', NaN, 'std_slip_velocity', NaN, 'sem_slip_velocity', NaN), data_num, 1 );
    
    for k = 1:data_num
        try
            data = AnalyzeChangepts(directory,data_idx(k,1),calarray,data_idx(k,4), data_idx(k,2), data_idx(k,3),10,1,[data_idx(k,5) data_idx(k,6)]);
            
            data_idx_output(k,5) = data.cropmarks(1);
            data_idx_output(k,6) = data.cropmarks(2);
%             close all
            stat_idx_output(k).num = data.crop.num;
            stat_idx_output(k).start = data.crop.start;
            stat_idx_output(k).end = data.crop.end;
            stat_idx_output(k).duration = data.crop.duration;
            stat_idx_output(k).unpack = data.crop.unpack;
            stat_idx_output(k).pack_length = data.crop.pack_length;
            stat_idx_output(k).vel_pause = data.crop.vel_pause;
            stat_idx_output(k).sum_pack_length = data.crop.sum_pack_length;
            stat_idx_output(k).mean_pause_duration = data.crop.mean_duration; 
            stat_idx_output(k).std_pause_duration = data.crop.std_duration; 
            stat_idx_output(k).sem_pause_duration = data.crop.sem_duration;
            stat_idx_output(k).mean_unpack = data.crop.mean_unpack;
            stat_idx_output(k).std_unpack = data.crop.std_unpack;
            stat_idx_output(k).sem_unpack = data.crop.sem_unpack;
            stat_idx_output(k).mean_pack_length = data.crop.mean_pack_length;
            stat_idx_output(k).std_pack_length = data.crop.std_pack_length;
            stat_idx_output(k).sem_pack_length = data.crop.sem_pack_length;
            stat_idx_output(k).mean_vel_pack = data.crop.mean_vel_pack;
            stat_idx_output(k).std_vel_pack = data.crop.std_vel_pack;
            stat_idx_output(k).sem_vel_pack = data.crop.sem_vel_pack;
            stat_idx_output(k).mean_vel_pause = data.crop.mean_vel_pause;
            stat_idx_output(k).std_vel_pause = data.crop.std_vel_pause;
            stat_idx_output(k).sem_vel_pause = data.crop.sem_vel_pause;
            stat_idx_output(k).slip_num = data.crop.slip_num;
            stat_idx_output(k).slip_start = data.crop.slip_start;
            stat_idx_output(k).slip_end = data.crop.slip_end;
            stat_idx_output(k).slip_duration = data.crop.slip_duration;
            stat_idx_output(k).vel_slip = data.crop.vel_slip;
            stat_idx_output(k).mean_slip_velocity = data.crop.mean_slip_velocity;
            stat_idx_output(k).std_slip_velocity = data.crop.std_slip_velocity;
            stat_idx_output(k).sem_slip_velocity = data.crop.sem_slip_velocity;
            
            % added on April 9 2020
            stat_idx_output(k).pause_freq = stat_idx_output(k).num/stat_idx_output(k).sum_pack_length*1000;
            
            [stgTime, ~] = StageMoveTime(directory, data.file(1:6), data.file(8:10));
                    %line([stgTime(i) stgTime(i)], ylim);
            stat_idx_output(k).move = stgTime(2);
            stat_idx_output(k).arrive = stgTime(3);
            
            
            display(['Done' num2str(k) ': data = AnalyzePack(datadirectory,' num2str(data_idx(k,1)) ...
                ',calparams, ' num2str(data_idx(k,4)) ...
                ', ' num2str(data_idx(k,2)) ...
                ', ' num2str(data_idx(k,3))...
                ',10,5);    cropmark = (' num2str(data_idx_output(k,5)) ' ' num2str(data_idx_output(k,6)) ')']);
        catch ME
            fprintf('%s\n',ME.message)
            for i = 1 : length(ME.stack)
                fprintf('Line: %d, Name: %s\n', ME.stack(i).line, ME.stack(i).name)
            end
            warning(['Wrong Command: data = AnalyzePack(datadirectory,' num2str(data_idx(k,1)) ...
                ',calparams, ' num2str(data_idx(k,4)) ...
                ', ' num2str(data_idx(k,2)) ...
                ', ' num2str(data_idx(k,3))...
                ',10,5);'])
            data_idx_output(k,5) = nan;
            data_idx_output(k,6) = nan;
            
            pause;
        end
    end
end