function [ output ] = Dynamics(myfunc,label, unit)
% myfunc = @(x) [x.mean_vel_pack];
% myfunc = @(x) 1000*[x.num]./[x.sum_pack_length];
% myfunc = @(x) [x.mean_pause_duration];

load('/Users/WanderingCorn/Documents/Research Diary/191125/stat_Comb.mat');

data_1_1 = myfunc(stat_Comb_wt_191003);
data_2_1 = myfunc(stat_Comb_Q163A_50_191003);
data_3_1 = myfunc(stat_Comb_T287D_50_191003);

% [data_1_2,idx_1,outliers_1] = deleteoutliers(data_1_1,0.0);
% [data_2_2,idx_2,outliers_2] = deleteoutliers(data_2_1,0.0);
% [data_3_2,idx_3,outliers_3] = deleteoutliers(data_3_1,0.0);

% data_1_2 = data_1_1(data_1_1~=0);
% data_2_2 = data_2_1(data_2_1~=0);
% data_3_2 = data_3_1(data_3_1~=0);

data_1_2 = data_1_1;
data_2_2 = data_2_1;
data_3_2 = data_3_1;

% figure()
clear YData
YData = [mean(data_1_2) mean(data_2_2) mean(data_3_2)];
colorlist = {[237 202 118]/255, [94 151 189]/255,[217 141 108]/255};
for i = 1:3
    hBar = bar(i,YData(i),'FaceColor',colorlist{i});
    hold on
end
% set(gca, 'XTickLabel', xtickstr)
legend({'WT','Q163A','T287D'})
title([label '(' unit ')'])
set(gcf,'Name',label)

plotSpread({data_1_2,data_2_2,data_3_2}, ...
    'xNames', {'WT', 'Q163A', 'T287D'}, 'distributionColors',{[0.929 0.694 0.125],[0 0.447 0.741],[0.85 0.325 0.0980]},...
    'distributionMarkers', {'o', 'o', 'o'});

output  = 'complete!';

textstr = {num2str(YData(1),'%.3g'),num2str(YData(2),'%.3g'),num2str(YData(3),'%.3g')};
text(1:3,YData,textstr,'HorizontalAlignment','center','VerticalAlignment','bottom');

std(data_1_2)/sqrt(length(data_1_2))
std(data_2_2)/sqrt(length(data_2_2))
std(data_3_2)/sqrt(length(data_3_2))
end

