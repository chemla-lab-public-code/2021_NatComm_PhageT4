
%% This script is to organize Vishal's mutant poisoning experiment
% 01	D:\Research\MATLAB\Data\081909\			S161T	1:2
% 02	D:\Research\MATLAB\Data\082009\			S161T	1:1
% 03	D:\Research\MATLAB\Data\082109\			S161T	1:1
% 04	D:\Research\MATLAB\Data\083009\			T287D	1:1     *****
% 05	D:\Research\MATLAB\Data\090409\			T287D	2:1     ***
% 06	D:\Research\MATLAB\Data\101009\			T287D	1:1     *****
% 07	D:\Research\MATLAB\Data\101009\			Q163A	1:1     *****
% 08	D:\Research\MATLAB\Data\101209\			Q163A	1:1     *****
% 09	D:\Research\MATLAB\Data\101209\			Q163A	2:1     ***
% 10	D:\Research\MATLAB\Data\101209\			Q163A	3:1     ***
% 11	D:\Research\MATLAB\Data\101309\			Q163A	1:1     *****
% 12	D:\Research\MATLAB\Data\101409\			Q163A	1:1     *****
% 13	D:\Research\MATLAB\Data\102009\			R162K	1:1     *****
% 14	D:\Research\MATLAB\Data\102009\			SYV	1:1
% 15	D:\Research\MATLAB\Data\102309\			GSS-AAA	1:1
% 16	D:\Research\MATLAB\Data\102309\			DEED-E404N	1:1
% 17	D:\Research\MATLAB\Data\102309\			E404N	1:1
% 18	D:\Research\MATLAB\Data\102109\			Q163A-cy3	1:1
% 19	D:\Research\MATLAB\Data\102109\			Q163A-cy3	1:1
% 20	D:\Research\MATLAB\Data\102109\			Q163A-cy3	1:1
% 21	D:\Research\MATLAB\Data\102109\			Q163A-cy3	1:1
% 22	D:\Research\MATLAB\Data\102109\			Q163A-cy3	1:1
% 23	D:\Research\MATLAB\Data\102109\			Q163A-cy3	1:1
% 24	D:\Research\MATLAB\Data\102109\			Q163A-cy3	1:1
% 25	D:\Research\MATLAB\Data\102109\			Q163A-cy3	1:1
% Note: only analyzed data mp19-25 have fields about unpack

% %% clear stat_VK
% clear stat_VK_wt
% clear stat_VK_Q163A_half
% clear stat_VK_Q163A_all
% clear stat_VK_T287D_half
% clear stat_VK_T287D_all
% 
% %% convert vishal's data to my format
% i = 1;
% for aa = [aa03 aa04 aa05 aa06 aa07 aa08 aa25 aa26 aa27 aa28 aa29 aa30 aa31 aa32 aa33 aa34 aa35 aa36 aa37 aa38 aa39 aa40]
% 	for k = 1:length(aa)
% 		stat_VK_wt(i) = aa{k};
% 		i = i+1;
% 	end
% end
% 
% i = 1; 
% for mp = [mp04 mp05 mp06]
%     for k = 1 : length(mp)
%         stat_VK_T287D_all(i)= mp{1,k};
% %         stat_VK_T287D_all(i).sum_pack_length = sum(mp{1,k}.pack_length);
%         i = i + 1;
%     end
% end
% 
% i = 1; 
% for mp = [mp07 mp08 mp09 mp10 mp11 mp12 mp18]
%     for k = 1 : length(mp)
%         stat_VK_Q163A_all(i)= mp{1,k};
% %         stat_VK_Q163A_all(i).sum_pack_length = sum(mp{1,k}.pack_length);
%         i = i + 1;
%     end
% end
% stat_VK_Q163A_all(1).unpack = [];
% stat_VK_Q163A_all(1).mean_unpack = nan;
% stat_VK_Q163A_all(1).std_unpack = nan;
% stat_VK_Q163A_all(1).sem_unpack = nan;
% stat_VK_Q163A_all = orderfields(stat_VK_Q163A_all,mp19{1,1});
% for mp = [mp19 mp23 mp21 mp22 mp23 mp24 mp25]
%     for k = 1 : length(mp)
%         [stat_VK_Q163A_all(i)]= mp{1,k};
% %         stat_VK_Q163A_all(i).sum_pack_length = sum(mp{1,k}.pack_length);
%         i = i + 1;
%     end
% end
% 
% 
% i = 1; 
% for mp = [mp04 mp06]
%     for k = 1 : length(mp)
%         stat_VK_T287D_half(i)= mp{1,k};
% %         stat_VK_T287D_half(i).sum_pack_length = sum(mp{1,k}.pack_length);
%         i = i + 1;
%     end
% end
% 
% 
% 
% i = 1; 
% for mp = [mp07 mp08 mp11 mp12 mp18]
%     for k = 1 : length(mp)
%         stat_VK_Q163A_half(i)= mp{1,k};
% %         stat_VK_Q163A_half(i).sum_pack_length = sum(mp{1,k}.pack_length);
%         i = i + 1;
%     end
% end
% stat_VK_Q163A_half(1).unpack = [];
% stat_VK_Q163A_half(1).mean_unpack = nan;
% stat_VK_Q163A_half(1).std_unpack = nan;
% stat_VK_Q163A_half(1).sem_unpack = nan;
% stat_VK_Q163A_half = orderfields(stat_VK_Q163A_half,mp19{1,1});
% for mp = [mp19 mp23 mp21 mp22 mp23 mp24 mp25]
%     for k = 1 : length(mp)
%         stat_VK_Q163A_half(i)= mp{1,k};
% %         stat_VK_Q163A_all(i).sum_pack_length = sum(mp{1,k}.pack_length);
%         i = i + 1;
%     end
% end
% 
% % add field "sum_pack_length" to stat_VK_wt
% cell_pack_length = {stat_VK_wt.pack_length}; array_sum_pack_length = cellfun(@sum,cell_pack_length);
% array_sum_pack_length = num2cell(array_sum_pack_length);
% [stat_VK_wt.sum_pack_length] = array_sum_pack_length{:};
% 
% % add field "sum_pack_length" to stat_VK_T287D_half
% cell_pack_length = {stat_VK_T287D_half.pack_length}; array_sum_pack_length = cellfun(@sum,cell_pack_length);
% array_sum_pack_length = num2cell(array_sum_pack_length);
% [stat_VK_T287D_half.sum_pack_length] = array_sum_pack_length{:};
% 
% % add field "sum_pack_length" to stat_VK_T287D_all
% cell_pack_length = {stat_VK_T287D_all.pack_length}; array_sum_pack_length = cellfun(@sum,cell_pack_length);
% array_sum_pack_length = num2cell(array_sum_pack_length);
% [stat_VK_T287D_all.sum_pack_length] = array_sum_pack_length{:};
% 
% % add field "sum_pack_length" to stat_VK_Q163A_half
% cell_pack_length = {stat_VK_Q163A_half.pack_length}; array_sum_pack_length = cellfun(@sum,cell_pack_length);
% array_sum_pack_length = num2cell(array_sum_pack_length);
% [stat_VK_Q163A_half.sum_pack_length] = array_sum_pack_length{:};
% 
% % add field "sum_pack_length" to stat_VK_Q163A_all
% cell_pack_length = {stat_VK_Q163A_all.pack_length}; array_sum_pack_length = cellfun(@sum,cell_pack_length);
% array_sum_pack_length = num2cell(array_sum_pack_length);
% [stat_VK_Q163A_all.sum_pack_length] = array_sum_pack_length{:};
% 
% % % figure
% % % hold on
% % % p = structfun(@plot,S);
% % % p(1).Marker = 'o';
% % % p(2).Marker = '+';
% % % p(3).Marker = 's';
% % % hold off
% 
% % h1 = plot(x, first_y, 'b');     %first_y has 4 columns so h1 is length 4
% % h2 = plot(x, second_y, 'r');    %second_y has 3 columns so h2 is length 3
% % legend([h1(1), h2(1)], 'Blue Meanies', 'Red Rovers')

%% calculate the mean and std of pack velocity
fprintf('mean velocity of SL_wt is %d +/- %d\n', mean([stat_SL_wt.mean_vel_pack]),std([stat_SL_wt.mean_vel_pack]))
fprintf('mean velocity of VK_wt_1mM_2mM is %d +/- %d\n', mean([stat_VK_1mM_2mM.mean_vel_pack]),std([stat_VK_1mM_2mM.mean_vel_pack]))

fprintf('mean velocity of VK_T287D_50 is %d +/- %d\n', mean([stat_VK_T287D_50.mean_vel_pack]),std([stat_VK_T287D_50.mean_vel_pack]))
fprintf('mean velocity of VK_Q163A_50 is %d +/- %d\n', mean([stat_VK_Q163A_50.mean_vel_pack]),std([stat_VK_Q163A_50.mean_vel_pack]))


%% plot the kde of SL & VK's wt&MP packaging velocity
vel_range = [0 2000];
figure()    % showing SL and VK's wt kde
f_SL_wt = subplot(2,3,1);
bandwidth = plot_kde([stat_SL_wt.mean_vel_pack],f_SL_wt);
title({[num2str(length(stat_SL_wt)) ' SL\_wt'],['KDE bandwidth = ' num2str(bandwidth)]})
xlabel('Velocity (bp/s)')
ylabel('a.u.')
xlim(vel_range)
f_VK_1mM_2mM = subplot(2,3,4);
bandwidth = plot_kde([stat_VK_1mM_2mM.mean_vel_pack],f_VK_1mM_2mM);
title({[num2str(length(stat_VK_1mM_2mM)) ' VK\_wt_1mM_2mM'],['KDE bandwidth = ' num2str(bandwidth)]})
xlabel('Velocity (bp/s)')
ylabel('a.u.')
xlim(vel_range)

 % showing SL and VK's T287D 1:1: kde
f_SL_T287D_50 = subplot(2,3,2);
bandwidth = plot_kde([stat_SL_T287D_50.mean_vel_pack],f_SL_T287D_50);
title({[num2str(length(stat_SL_T287D_50)) ' SL\_T287D_50'],['KDE bandwidth = ' num2str(bandwidth)]})
xlabel('Velocity (bp/s)')
ylabel('a.u.')
xlim(vel_range)
f_VK_T287D_50 = subplot(2,3,5);
bandwidth = plot_kde([stat_VK_T287D_50.mean_vel_pack],f_VK_T287D_50);
title({[num2str(length(stat_VK_T287D_50)) ' VK\_T287D(1:1)'],['KDE bandwidth = ' num2str(bandwidth)]})
xlabel('Velocity (bp/s)')
ylabel('a.u.')
xlim(vel_range)

 % showing SL and VK's Q163A 1:1 kde
f_SL_Q163A_50 = subplot(2,3,3);
bandwidth = plot_kde([stat_SL_Q163A_50.mean_vel_pack],f_SL_Q163A_50);
title({[num2str(length(stat_SL_Q163A_50)) ' SL\_Q163A'],['KDE bandwidth = ' num2str(bandwidth)]})
xlabel('Velocity (bp/s)')
ylabel('a.u.')
xlim(vel_range)
f_VK_Q163A_50 = subplot(2,3,6);
bandwidth = plot_kde([stat_VK_Q163A_50.mean_vel_pack],f_VK_Q163A_50);
title({[num2str(length(stat_VK_Q163A_50)) ' VK\_Q163A(1:1)'],['KDE bandwidth = ' num2str(bandwidth)]})
xlabel('Velocity (bp/s)')
ylabel('a.u.')
xlim(vel_range)

%% plot #pause/bp against vel

str = {[0  0.4470 0.7410], [0.8500 0.3250 0.0980],[0.4660, 0.6740, 0.1880]};
str = {'b','r','g'};

f = figure();

plot([stat_SL_wt.mean_vel_pack],[stat_SL_wt.num]./[stat_SL_wt.sum_pack_length],'o','Color',str{1}) %stat\_SL\_wt
hold on
plot([stat_VK_wt.mean_vel_pack],[stat_VK_wt.num]./[stat_VK_wt.sum_pack_length],'+','Color',str{1}) %stat\_VK\_wt
plot([stat_SL_T287D.mean_vel_pack],[stat_SL_T287D.num]./[stat_SL_T287D.sum_pack_length],'o','Color',str{2}) %stat\_SL\_T287D
plot([stat_VK_T287D_half.mean_vel_pack],[stat_VK_T287D_half.num]./[stat_VK_T287D_half.sum_pack_length],'+','Color',str{2}) %stat\_VK\_T287D_half
plot([stat_SL_Q163A.mean_vel_pack],[stat_SL_Q163A.num]./[stat_SL_Q163A.sum_pack_length],'o','Color',str{3}) %stat\_SL\_Q163A
plot([stat_VK_Q163A_half.mean_vel_pack],[stat_VK_Q163A_half.num]./[stat_VK_Q163A_half.sum_pack_length],'+','Color',str{3}) %stat\_VK\_Q163A_half
title('#pause per bp vs velocity')
xlabel('Velocity (bp/s)')
legend('SL\_wt','VK\_wt','SL\_T287D','VK\_T287D','SL\_Q163A','VK\_Q163A')
xdata = [[stat_SL_wt.mean_vel_pack] [stat_VK_wt.mean_vel_pack] [stat_SL_T287D.mean_vel_pack] [stat_VK_T287D_half.mean_vel_pack] [stat_SL_Q163A.mean_vel_pack] [stat_VK_Q163A_half.mean_vel_pack]];
ydata = [[stat_SL_wt.num]./[stat_SL_wt.sum_pack_length] ...
[stat_VK_wt.num]./[stat_VK_wt.sum_pack_length] ...
[stat_SL_T287D.num]./[stat_SL_T287D.sum_pack_length] ...
[stat_VK_T287D_half.num]./[stat_VK_T287D_half.sum_pack_length] ...
[stat_SL_Q163A.num]./[stat_SL_Q163A.sum_pack_length] ...
[stat_VK_Q163A_half.num]./[stat_VK_Q163A_half.sum_pack_length]];
idx  = find(ydata>0);
coeff = polyfit(log(xdata(idx)),log(ydata(idx)),1)
plot([100:20:2000],exp(coeff(2))*[100:20:2000].^coeff(1))

%% plot pack_length against vel
f = figure(); str = {'bo','b+','ro','r+','go','g+'};
stat = {stat_SL_wt,stat_VK_wt,stat_SL_T287D,stat_VK_T287D_half,stat_SL_Q163A,stat_VK_Q163A_half};
for i = 1 : length(stat)
    plot([stat{i}.mean_vel_pack], [stat{i}.mean_pack_length],str{i})
    hold on
end
title('mean pack length vs velocity')
xlabel('Velocity (bp/s)')
ylabel('mean pack length (bp)')
legend('SL\_wt','VK\_wt','SL\_T287D','VK\_T287D','SL\_Q163A','VK\_Q163A')

%% plot pause_duration against vel
f = figure(); 
% str = {'bo','b+','ro','r+','go','g+'};
% stat = {stat_SL_wt,stat_VK_wt,stat_SL_T287D,stat_VK_T287D_half,stat_SL_Q163A,stat_VK_Q163A_half};
% for i = 1 : length(stat)
%     plot([stat{i}.mean_vel_pack], [stat{i}.mean_pause_duration],str{i})
%     hold on
% end

plot([stat_SL_wt.mean_vel_pack],[stat_SL_wt.mean_pause_duration],'bo') %stat\_SL\_wt
hold on
plot([stat_VK_wt.mean_vel_pack],[stat_VK_wt.mean_duration],'b+') %stat\_VK\_wt
plot([stat_SL_T287D.mean_vel_pack],[stat_SL_T287D.mean_pause_duration],'ro') %stat\_SL\_T287D
plot([stat_VK_T287D_half.mean_vel_pack],[stat_VK_T287D_half.mean_duration],'r+') %stat\_VK\_T287D_half
plot([stat_SL_Q163A.mean_vel_pack],[stat_SL_Q163A.mean_pause_duration],'go') %stat\_SL\_Q163A
plot([stat_VK_Q163A_half.mean_vel_pack],[stat_VK_Q163A_half.mean_duration],'g+') %stat\_VK\_Q163A_half

title('mean pause duration vs velocity')
xlabel('Velocity (bp/s)')
ylabel('mean pause duration (s)')
legend('SL\_wt','VK\_wt','SL\_T287D','VK\_T287D','SL\_Q163A','VK\_Q163A')


%% 190828

% I re-analyze Vishal's data, wild type gp17 + >= 1 mM ATP
clear stat_VK_wt_1mM_2mM_bySL
clear idx_VK_wt_1mM_2mM_bySL
[stat_VK_wt_1mM_2M_bySL, idx_VK_wt_1mM_2mM_bySL]= AnalyzePackVK(packaa25,10,5);
for packaa = [packaa26 packaa27 packaa28 packaa29 packaa30 packaa31 packaa32 packaa38 packaa39 packaa40]
    clear tempstat
    clear tempidx
    [tempstat, tempidx] = AnalyzePackVK(packaa,10,5);
    stat_VK_wt_1mM_2M_bySL = [stat_VK_wt_1mM_2M_bySL tempstat];
    idx_VK_wt_1mM_2mM_bySL = [idx_VK_wt_1mM_2mM_bySL tempidx];
end
for k = 1:length(idx_VK_wt_1mM_2mM_bySL)
    date_VK_wt_1mM_2mM_numeric(k) = str2num(idx_VK_wt_1mM_2mM_bySL(k).file(5:6))*10000+str2num(idx_VK_wt_1mM_2mM_bySL(k).file(1:4));
end

% I re-analyze Vishal's data, wild type gp17 : Q163A = 1:1 + 1 mM ATP
clear stat_VK_Q163A_50_bySL
clear idx_VK_Q163A_50_bySL
[stat_VK_Q163A_50_bySL, idx_VK_Q163A_50_bySL]= AnalyzePackVK(packmp07,10,5);
for packaa = [packmp08 packmp11 packmp12 packmp18] % packmp19 packmp20 packmp21 packmp22 packmp23 packmp24 packmp25
    clear tempstat
    clear tempidx
    [tempstat, tempidx] = AnalyzePackVK(packaa,10,5);
    stat_VK_Q163A_50_bySL = [stat_VK_Q163A_50_bySL tempstat];
    idx_VK_Q163A_50_bySL = [idx_VK_Q163A_50_bySL tempidx];
end
% for k = 1:length(idx_VK_Q163A_50_bySL)
% idx_VK_Q163A_50_numeric(k) = str2num(idx_VK_Q163A_50_bySL(k).file(5:6))*10000+str2num(idx_VK_Q163A_50_bySL(k).file(1:4));
% end

% I re-analyze Vishal's data, wild type gp17 : Q163A = 1:1 including cy3-A163A + 1 mM ATP
clear stat_VK_Q163A_50_cy3_bySL
clear idx_VK_Q163A_50_cy3_bySL
[stat_VK_Q163A_50_cy3_bySL, idx_VK_Q163A_50_cy3_bySL]= AnalyzePackVK(packmp07,10,5);
for packaa = [packmp08 packmp11 packmp12 packmp18 packmp19 packmp20 packmp21 packmp22 packmp23 packmp24 packmp25]
    clear tempstat
    clear tempidx
    [tempstat, tempidx] = AnalyzePackVK(packaa,10,5);
    stat_VK_Q163A_50_cy3_bySL = [stat_VK_Q163A_50_cy3_bySL tempstat];
    idx_VK_Q163A_50_cy3_bySL = [idx_VK_Q163A_50_cy3_bySL tempidx];
end


clear stat_VK_packaa_bySL
clear idx_VK_packmp_bySL
iwin = 10;
istep = 5;
[stat_VK_packaa_bySL{1}, idx_VK_packaa_bySL{1}]= AnalyzePackVK(packaa01,iwin,istep);
[stat_VK_packaa_bySL{2}, idx_VK_packaa_bySL{2}]= AnalyzePackVK(packaa02,iwin,istep);
[stat_VK_packaa_bySL{3}, idx_VK_packaa_bySL{3}]= AnalyzePackVK(packaa03,iwin,istep);
[stat_VK_packaa_bySL{4}, idx_VK_packaa_bySL{4}]= AnalyzePackVK(packaa04,iwin,istep);
[stat_VK_packaa_bySL{5}, idx_VK_packaa_bySL{5}]= AnalyzePackVK(packaa05,iwin,istep);
[stat_VK_packaa_bySL{6}, idx_VK_packaa_bySL{6}]= AnalyzePackVK(packaa06,iwin,istep);
[stat_VK_packaa_bySL{7}, idx_VK_packaa_bySL{7}]= AnalyzePackVK(packaa07,iwin,istep);
[stat_VK_packaa_bySL{8}, idx_VK_packaa_bySL{8}]= AnalyzePackVK(packaa08,iwin,istep);
[stat_VK_packaa_bySL{9}, idx_VK_packaa_bySL{9}]= AnalyzePackVK(packaa09,iwin,istep);
[stat_VK_packaa_bySL{10}, idx_VK_packaa_bySL{10}]= AnalyzePackVK(packaa10,iwin,istep);
[stat_VK_packaa_bySL{11}, idx_VK_packaa_bySL{11}]= AnalyzePackVK(packaa11,iwin,istep);
[stat_VK_packaa_bySL{12}, idx_VK_packaa_bySL{12}]= AnalyzePackVK(packaa12,iwin,istep);
[stat_VK_packaa_bySL{13}, idx_VK_packaa_bySL{13}]= AnalyzePackVK(packaa13,iwin,istep);
[stat_VK_packaa_bySL{14}, idx_VK_packaa_bySL{14}]= AnalyzePackVK(packaa14,iwin,istep);
[stat_VK_packaa_bySL{15}, idx_VK_packaa_bySL{15}]= AnalyzePackVK(packaa15,iwin,istep);
[stat_VK_packaa_bySL{16}, idx_VK_packaa_bySL{16}]= AnalyzePackVK(packaa16,iwin,istep);
[stat_VK_packaa_bySL{17}, idx_VK_packaa_bySL{17}]= AnalyzePackVK(packaa17,iwin,istep);
[stat_VK_packaa_bySL{18}, idx_VK_packaa_bySL{18}]= AnalyzePackVK(packaa18,iwin,istep);
[stat_VK_packaa_bySL{19}, idx_VK_packaa_bySL{19}]= AnalyzePackVK(packaa19,iwin,istep);
[stat_VK_packaa_bySL{20}, idx_VK_packaa_bySL{20}]= AnalyzePackVK(packaa20,iwin,istep);
[stat_VK_packaa_bySL{21}, idx_VK_packaa_bySL{21}]= AnalyzePackVK(packaa21,iwin,istep);
[stat_VK_packaa_bySL{22}, idx_VK_packaa_bySL{22}]= AnalyzePackVK(packaa22,iwin,istep);
[stat_VK_packaa_bySL{23}, idx_VK_packaa_bySL{23}]= AnalyzePackVK(packaa23,iwin,istep);
[stat_VK_packaa_bySL{24}, idx_VK_packaa_bySL{24}]= AnalyzePackVK(packaa24,iwin,istep);
[stat_VK_packaa_bySL{25}, idx_VK_packaa_bySL{25}]= AnalyzePackVK(packaa25,iwin,istep);
[stat_VK_packaa_bySL{26}, idx_VK_packaa_bySL{26}]= AnalyzePackVK(packaa26,iwin,istep);
[stat_VK_packaa_bySL{27}, idx_VK_packaa_bySL{27}]= AnalyzePackVK(packaa27,iwin,istep);
[stat_VK_packaa_bySL{28}, idx_VK_packaa_bySL{28}]= AnalyzePackVK(packaa28,iwin,istep);
[stat_VK_packaa_bySL{29}, idx_VK_packaa_bySL{29}]= AnalyzePackVK(packaa29,iwin,istep);
[stat_VK_packaa_bySL{30}, idx_VK_packaa_bySL{30}]= AnalyzePackVK(packaa30,iwin,istep);
[stat_VK_packaa_bySL{31}, idx_VK_packaa_bySL{31}]= AnalyzePackVK(packaa31,iwin,istep);
[stat_VK_packaa_bySL{32}, idx_VK_packaa_bySL{32}]= AnalyzePackVK(packaa32,iwin,istep);
[stat_VK_packaa_bySL{33}, idx_VK_packaa_bySL{33}]= AnalyzePackVK(packaa33,iwin,istep);
[stat_VK_packaa_bySL{34}, idx_VK_packaa_bySL{34}]= AnalyzePackVK(packaa34,iwin,istep);
[stat_VK_packaa_bySL{35}, idx_VK_packaa_bySL{35}]= AnalyzePackVK(packaa35,iwin,istep);
[stat_VK_packaa_bySL{36}, idx_VK_packaa_bySL{36}]= AnalyzePackVK(packaa36,iwin,istep);
[stat_VK_packaa_bySL{37}, idx_VK_packaa_bySL{37}]= AnalyzePackVK(packaa37,iwin,istep);
[stat_VK_packaa_bySL{38}, idx_VK_packaa_bySL{38}]= AnalyzePackVK(packaa38,iwin,istep);
[stat_VK_packaa_bySL{39}, idx_VK_packaa_bySL{39}]= AnalyzePackVK(packaa39,iwin,istep);
[stat_VK_packaa_bySL{40}, idx_VK_packaa_bySL{40}]= AnalyzePackVK(packaa40,iwin,istep);

[stat_VK_packmp_bySL{2}, idx_VK_packmp_bySL{2}]= AnalyzePackVK(packmp02,iwin,istep);
[stat_VK_packmp_bySL{3}, idx_VK_packmp_bySL{3}]= AnalyzePackVK(packmp03,iwin,istep);
[stat_VK_packmp_bySL{4}, idx_VK_packmp_bySL{4}]= AnalyzePackVK(packmp04,iwin,istep);
[stat_VK_packmp_bySL{5}, idx_VK_packmp_bySL{5}]= AnalyzePackVK(packmp05,iwin,istep);
[stat_VK_packmp_bySL{6}, idx_VK_packmp_bySL{6}]= AnalyzePackVK(packmp06,iwin,istep);
[stat_VK_packmp_bySL{7}, idx_VK_packmp_bySL{7}]= AnalyzePackVK(packmp07,iwin,istep);
[stat_VK_packmp_bySL{8}, idx_VK_packmp_bySL{8}]= AnalyzePackVK(packmp08,iwin,istep);
[stat_VK_packmp_bySL{9}, idx_VK_packmp_bySL{9}]= AnalyzePackVK(packmp09,iwin,istep);
[stat_VK_packmp_bySL{10}, idx_VK_packmp_bySL{10}]= AnalyzePackVK(packmp10,iwin,istep);
[stat_VK_packmp_bySL{11}, idx_VK_packmp_bySL{11}]= AnalyzePackVK(packmp11,iwin,istep);
[stat_VK_packmp_bySL{12}, idx_VK_packmp_bySL{12}]= AnalyzePackVK(packmp12,iwin,istep);
[stat_VK_packmp_bySL{13}, idx_VK_packmp_bySL{13}]= AnalyzePackVK(packmp13,iwin,istep);
[stat_VK_packmp_bySL{14}, idx_VK_packmp_bySL{14}]= AnalyzePackVK(packmp14,iwin,istep);
[stat_VK_packmp_bySL{15}, idx_VK_packmp_bySL{15}]= AnalyzePackVK(packmp15,iwin,istep);
[stat_VK_packmp_bySL{16}, idx_VK_packmp_bySL{16}]= AnalyzePackVK(packmp16,iwin,istep);
[stat_VK_packmp_bySL{17}, idx_VK_packmp_bySL{17}]= AnalyzePackVK(packmp17,iwin,istep);
[stat_VK_packmp_bySL{18}, idx_VK_packmp_bySL{18}]= AnalyzePackVK(packmp18,iwin,istep);
[stat_VK_packmp_bySL{19}, idx_VK_packmp_bySL{19}]= AnalyzePackVK(packmp19,iwin,istep);
[stat_VK_packmp_bySL{20}, idx_VK_packmp_bySL{20}]= AnalyzePackVK(packmp20,iwin,istep);
[stat_VK_packmp_bySL{21}, idx_VK_packmp_bySL{21}]= AnalyzePackVK(packmp21,iwin,istep);
[stat_VK_packmp_bySL{22}, idx_VK_packmp_bySL{22}]= AnalyzePackVK(packmp22,iwin,istep);
[stat_VK_packmp_bySL{23}, idx_VK_packmp_bySL{23}]= AnalyzePackVK(packmp23,iwin,istep);
[stat_VK_packmp_bySL{24}, idx_VK_packmp_bySL{24}]= AnalyzePackVK(packmp24,iwin,istep);
[stat_VK_packmp_bySL{25}, idx_VK_packmp_bySL{25}]= AnalyzePackVK(packmp25,iwin,istep);

% 1mM packaa21-37
% 2mM packaa38-40
% 50% Q163A 1mM packmp07 08 11 12 18
% 67% Q163A 1mM packmp09
% 75% Q163A 1mM packmp10
% 50% cy3-Q163A 1mM packmp19-25
idx_VK_packaa_bySL = {
'2009/07/27',...
'2009/07/29',...
'2009/08/02',...
'2009/08/04',...
'2009/08/06',...
'2009/08/11',...
'2009/09/08',...
'2009/09/16',...
'2009/09/18',...
'2009/10/02',...
'2009/10/10',...
'2009/10/13',...
'2009/10/28',...
'2009/11/11',...
'2009/11/29',...
'2011/07/08',...
'2011/07/09',...
'2009/11/22',...
'2009/11/24',...
'2009/11/28'};
for k = 21:40
date_VK_packaa_bySL{k} = idx_VK_packaa_bySL{k-20};
end

idx_VK_packmp_bySL = {
    '2009/08/19',...
    '2009/08/20',...
    '2009/08/21',...
    '2009/08/30',...
    '2009/09/04',...
    '2009/10/10',...
    '2009/10/12',...
    '2009/10/12',...
    '2009/10/12',...
    '2009/10/13',...
    '2009/10/14',...
    '2009/10/20',...
    '2009/10/20',...
    '2009/10/23',...
    '2009/10/23',...
    '2009/10/23',...
    '2009/10/21',...
    '2011/07/13',...
    '2011/07/15',...
    '2011/09/30',...
    '2011/10/05',...
    '2011/10/20',...
    '2011/10/21',...
    '2011/10/21',...
    '2011/20/26'};
idx_VK_packmp_bySL = date_VK_packmp_bySL;

clear temp
clear k
for k = 21:40
temp(k) = mean([stat_VK_packaa_bySL{k}.mean_vel_pack]);
end
figure()
bar(temp(21:40))
set(gca,'XTickLabel',idx_VK_packaa_bySL)
title('Vishal''s wt vel\_pack grouped by date')


clear stat_VK_1mM_2mM
stat_VK_1mM_2mM = [];
for k = 21:40
    stat_VK_1mM_2mM = [stat_VK_1mM_2mM stat_VK_packaa_bySL{k}];
end

clear stat_VK_Q163_50
stat_VK_Q163_50 = [];
for k = [7 8 11 12 18]
    stat_VK_Q163_50 = [stat_VK_Q163_50 stat_VK_packmp_bySL{k}];
end

clear stat_VK_Q163_67
stat_VK_Q163_67 = [];
stat_VK_Q163_67 = [stat_VK_Q163_67 stat_VK_packmp_bySL{9}];

clear stat_VK_Q163_75
stat_VK_Q163_75 = [];
stat_VK_Q163_75 = [stat_VK_Q163_75 stat_VK_packmp_bySL{10}];

clear stat_VK_Q163_cy3
stat_VK_Q163_cy3 = [];
for k = 19:25
    stat_VK_Q163_cy3 = [stat_VK_Q163_cy3 stat_VK_packmp_bySL{k}];
end

clear stat_VK_Q163_50_cy3
stat_VK_Q163_50_cy3 = [];
for k = [7, 8, 11, 12, 18:25]
    stat_VK_Q163_50_cy3 = [stat_VK_Q163_50_cy3 stat_VK_packmp_bySL{k}];
end

clear stat_VK_T287D_50
stat_VK_T287D_50 = [];
for k = [4 6]
    stat_VK_T287D_50 = [stat_VK_T287D_50 stat_VK_packmp_bySL{k}];
end

clear stat_VK_T287D_67
stat_VK_T287D_67 = [];
stat_VK_T287D_67 = [stat_VK_T287D_67 stat_VK_packmp_bySL{5}];


YData = zeros(20);
for k = 21:40
	YData(k-20) = mean([stat_VK_packaa_bySL{1,k}.mean_vel_pack]);
end
ctrs = 1:20;
bar (ctrs, YData)
set(gca,'XTickLabel', idx_VK_packaa_bySL);
set(gca,'xTick',ctrs);
set(gca,'XTickLabelRotation',60);
title('Vishal''s wt data grouped by date')
set(gcf,'Name','Vishal''s wt data grouped by date')

