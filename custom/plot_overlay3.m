function hg = plot_overlay3(directory, calparams,idx_array,iwin, istep,hg)
%overlay with the same origin

for idx = 1:length(idx_array)
    clear data
    data = load_trapData(directory,idx_array(idx,1),calparams,idx_array(idx,4),idx_array(idx,2),idx_array(idx,3));
    data = plot_TrapSep_bp_F_vs_Time(data,0);
    close(gcf);
    tmax = idx_array(idx,6); tmin = idx_array(idx,5);    
    crop = find(data.time >= tmin & data.time <=tmax);
    
    data.crop.force_X = data.force_X(crop); 
    data.crop.force_Y = data.force_Y(crop); 
    data.crop.DNAext_X = data.DNAext_X(crop); 
    data.crop.DNAext_Y = data.DNAext_Y(crop); 
    data.crop.time = data.time(crop);
    data.crop.time = data.crop.time - data.crop.time(1);
    data.crop.DNAbp = data.DNAbp(crop); 
    
    % calculate Velocity by moving average
    [t_vel,vel] = Velocity(data.crop.time,data.crop.DNAbp,iwin,istep);
%     velbeforeLoess = vel;
    vel = smooth(t_vel,vel,round(0.4/data.sampperiod),'loess');
    vel = vel';
    
    % Method: Silverman's rule of thumb
    % to determin the band width of kernel density estimation
    [bandwidth_crop, std_dev_crop, delta_iqr_crop] = Silverman(vel);
    % KDE
    vel_crop_kde = kde(vel, bandwidth_crop);
    
    hold on;
    for k = 1 : length(vel)
        y2 = 0.5*max(vel_crop_kde(2, :));
        hold on
    end
    
    % mark the peaks in KDE distribution
    [pks,vel_crop_pks] = findpeaks(vel_crop_kde(2, :),vel_crop_kde(1, :),'SortStr','descend');
    k = 1;
    while k <= length(pks) && pks(k) > 0.5*max(pks(1)) 
        
        k = k+1;
    end
    
    
    % Select Velocities that are pauses & slips
%     k = 1;
%     tail = 0;
%     while tail < 0.003
%         tail = tail + (Vel_Crop_Kde(1, 2) - Vel_Crop_Kde(1, 1))*Vel_Crop_Kde(2, k);
%         k = k+1;
%     end
%     threshold = max(Vel_Crop_Kde(1, k-1),50);

    % find the pauses and slips
    % for normal distribution, iqr = 1.34896 sigma;
    % Assume in KDE the highest peak is the mean and 3 sigma ~=2.22 iqr
    threshold = max(max(vel_crop_pks(1:k-1))-2.22*delta_iqr_crop, 50);
    idx = find(vel < threshold & vel >= -2000); % select pauses [pause_Vel-2*std_pause_Vel]
    sidx = find(vel <= -2000); % select slips
    
    % Select times that are pauses in original time trace
    dt = data.crop.time(1+iwin)-data.crop.time(1);
    pause_idx = []; temp = [];
    for j = 1:length(idx)
        temp = find(data.crop.time >= t_vel(idx(j))-dt/2 & data.crop.time <= t_vel(idx(j))+dt/2);
        pause_idx = [pause_idx temp];
    end;
    pause_idx = unique(pause_idx);
    
    
    % Select times that are slips in original time trace
    slip_idx = [];
    for j = 1:length(sidx)
        temp = find(data.crop.time >= t_vel(sidx(j))-dt/2 & data.crop.time <= t_vel(sidx(j))+dt/2);
        slip_idx = [slip_idx temp];
    end;
    slip_idx = unique(slip_idx);
    pause_idx = setdiff(pause_idx,slip_idx); % split up any pauses containing a slip
    
    
    % Select start and end points for each pause
    ddidx = find(diff(diff(pause_idx)) >= 10);    
    pstart_idx = [1 ddidx+2];
    pend_idx = [ddidx+1 length(pause_idx)];
    
    % Select start and end points for each pause
    ddidx = find(diff(diff(slip_idx)) >= 10);    
    sstart_idx = [1 ddidx+2];
    send_idx = [ddidx+1 length(slip_idx)];
   
    %Yann's ghetto edit------------
    if isempty(pause_idx) == 1;
        pause_start = [];
        pause_end = [];
    else
        pause_start = pause_idx(pstart_idx);
        pause_end = pause_idx(pend_idx);  
    end
    
    if isempty(slip_idx) == 1;
        slip_start = [];
        slip_end = [];
    else
        slip_start = pause_idx(sstart_idx);
        slip_end = pause_idx(send_idx);  
    end
    
    
    
    % Select start and end points for each slip
    if ~isempty(slip_idx)
        ddsidx = find(diff(diff(slip_idx)) >= 10);
        sstart_idx = [1 ddsidx+2];
        send_idx = [ddsidx+1 length(slip_idx)];
        slip_start = slip_idx(sstart_idx);
        slip_end = slip_idx(send_idx);
    else
        slip_start = [];
        slip_end = [];
    end;
    
    
    % Catenate time trace without pauses
    pause_slip_start = [pause_start slip_start];
    pause_slip_end = [pause_end slip_end];
    [pause_slip_start,sort_idx] = sort(pause_slip_start);
    pause_slip_end = pause_slip_end(sort_idx);
    pack_start = [1 pause_slip_end+1];
    pack_end = [pause_slip_start length(data.crop.time)];
    cat_DNA_length = []; temp = [];
    cat_DNA_length = data.crop.DNAbp(pack_start(1):pack_end(1)); %-1
    for j = 2:length(pack_start)
        temp = data.crop.DNAbp(pack_start(j):pack_end(j)) - data.crop.DNAbp(pack_start(j)) + cat_DNA_length(end); % DNA_Length{i}(pack_end(j-1));
        cat_DNA_length = [cat_DNA_length temp];
    end;
    cat_time = (0:length(cat_DNA_length)-1)*(data.crop.time(2)-data.crop.time(1));

    % Compile statistics on pauses and packaging
    
    [~,vel_pack] = Velocity(cat_time,cat_DNA_length,iwin,istep);
%     [t_Vel_pack,Vel_pack] = Velocity(cat_Time,cat_DNA_Length,iwin,istep);

    % KDE Bandwidth Method: Silverman's rule of thumb to determin the band width of kernel density estimation
    [bandwidth_pack, std_dev_pack, delta_iqr_pack] = Silverman(vel_pack);
    vel_pack_kde = kde(vel_pack, bandwidth_pack);

%
    mean_vel_pack = mean(vel_pack);
    std_vel_pack = std(vel_pack);
    sem_vel_pack = std_vel_pack/sqrt(length(cat_DNA_length)/iwin);
    
    num_pause = length(pause_start);
    pause_duration = zeros (1,length(num_pause));
    pack_length = zeros (1,length(pack_start));
    vel_pause = zeros (1,length(num_pause));
    
    num_slip = length(slip_start);
    slip_duration = zeros (1,length(num_slip));
    slip_length = [];
    vel_slip = zeros (1,length(num_slip));
    pause_unpack = zeros (1,length(num_pause));
    
    for j = 1:length(pack_start)
        pack_length(j) = data.crop.DNAbp(pack_start(j)) - data.crop.DNAbp(pack_end(j));
    end;
    
    for j = 1:num_pause
        pause_duration(j) = data.crop.time(pause_end(j)) - data.crop.time(pause_start(j));
        pause_unpack(j) = data.crop.DNAbp(pause_end(j)) - data.crop.DNAbp(pause_start(j)); % added line YRC 12/14/10
        a = polyfit(data.crop.time(pause_start(j):pause_end(j)),data.crop.DNAbp(pause_start(j):pause_end(j)),1);
        vel_pause(j) = -a(1);
    end;
    
    for j = 1:num_slip
        slip_duration(j) = data.crop.time(slip_end(j)) - data.crop.time(slip_start(j));
        b = polyfit(data.crop.time(slip_start(j):slip_end(j)),data.crop.DNAbp(slip_start(j):slip_end(j)),1);
        vel_slip(j) = -b(1);
    end;
    
    mean_pause_duration = mean(pause_duration);
    std_pause_duration = std(pause_duration);
    sem_pause_duration = std(pause_duration)/sqrt(num_pause);

    mean_pause_unpack = mean(pause_unpack); % added these 3 lines YRC 12/14/10
    std_pause_unpack = std(pause_unpack); %
    sem_pause_unpack = std(pause_unpack)/sqrt(num_pause); %

    mean_pack_length = mean(pack_length);
    std_pack_length = std(pack_length);
    sem_pack_length = std(pack_length)/sqrt(length(pack_start));
    
    mean_vel_pause = mean(vel_pause);
    std_vel_pause = std(vel_pause);
    sem_vel_pause = std(vel_pause)/sqrt(num_pause);
    
    mean_slip_velocity = mean(vel_slip);
    std_slip_velocity = std(vel_slip);
    sem_slip_velocity = std(vel_slip)/sqrt(num_slip);

    % Plot everything out
    
    cat_DNA_length = cat_DNA_length - cat_DNA_length(1) + 3400;
    plot(cat_time,cat_DNA_length);%,'Parent',hg);
    
end

end

