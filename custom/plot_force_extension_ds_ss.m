function dsDNA1 = plot_force_extension_ds_ss(trapData)
% construct help

%% plot the data

l = length(trapData.TrapSep);
TrapSepFilt=zeros(1,l);
gauge = 14;
for ii = 1:l
    windowsize = length(max(1,ii-gauge):min(l,ii+gauge));
    for jj = max(1,ii-gauge):min(l,ii+gauge)
        TrapSepFilt(ii) = TrapSepFilt(ii) + trapData.TrapSep(jj);
    end
    TrapSepFilt(ii)=TrapSepFilt(ii)/windowsize;
end

% remove jumps in trap position
TrapSepFilt_diff = diff(TrapSepFilt);
normalized_diff = diff(TrapSepFilt)/mean(TrapSepFilt_diff(TrapSepFilt_diff>0));
TrapSepFiltClean = TrapSepFilt(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.force_AX = trapData.force_AX(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.force_BX = trapData.force_BX(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.force_AY = trapData.force_AY(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.force_BY = trapData.force_BY(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.DNAext_X = trapData.DNAext_X(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.DNAext_Y = trapData.DNAext_Y(normalized_diff > 0.8 | normalized_diff < -0.8);
% find the top peaks
[numMAX, indMAX] = findpeaks(TrapSepFiltClean);
% find the bottom peaks
TrapSepInverse = [-max(numMAX) -TrapSepFiltClean -max(numMAX)];
[numMIN, indMIN] = findpeaks(TrapSepInverse);
indMIN = indMIN-1;
% count number of raster scans
rasterNUM = (max([length(indMIN) length(indMAX)]))/2;
% get the force array
if trapData.scandir == 1
    FA = trapData.force_AX;
    FB = trapData.force_BX;
    FM = (trapData.force_AX+trapData.force_BX)./2;
    DNAext = trapData.DNAext_X;
elseif trapData.scandir == 0
    FA = trapData.force_AY;
    FB = trapData.force_BY;
    FM = (trapData.force_AY+trapData.force_BY)./2;
    DNAext = trapData.DNAext_Y;
end

% get 6.0941 pN offset
    fitrange = [6.0941-0.1,6.0941+0.1];
    DNAext_offset = [];
    DNAext_offset = mean(DNAext(FM > fitrange(1) & FM < fitrange(2)))
    [WLC_param] = WLC_parameters;
    dsDNA1 = DNAext_offset/(WLC_param.hds*XWLCContour(6.0941,WLC_param.Pds,WLC_param.Sds,WLC_param.kT)); % nm/bp
    
    max_force = max([trapData.force_AX, trapData.force_AY, trapData.force_BX, trapData.force_BY], 25)+0.5;
    
    F = 0:0.5:max_force;
    figure()
    for ssDNA1 = 0:100:dsDNA1
        hold on
        bp1 = 1*WLC_param.hds*XWLCContour(F,WLC_param.Pds,WLC_param.Sds,WLC_param.kT);
        nt1 = 1*WLC_param.hss*XWLCContour(F,WLC_param.Pss,WLC_param.Sss,WLC_param.kT); % nm/nt
        WLC_ext = (dsDNA1-ssDNA1).*bp1 + ssDNA1.*nt1; 
%       WLC_ext = WLC_ext + fake_offset;
    
        plot(WLC_ext, F, '--r', 'LineWidth', 2)
    end
for ii = 1:1:rasterNUM
    indxS = indMIN(ii); % far point
    indxE = indMAX(ii); % close point
    indxEE = indMIN(ii+1);
    hold on
    plot(DNAext(indxS:indxE),FM(indxS:indxE),'color', [0.9100    0.4100    0.1700], 'LineWidth', 1.5)
    hold on
    plot(DNAext(indxE:indxEE),FM(indxE:indxEE), 'color', [0 0.4470 0.7410], 'LineWidth', 1.5)
end
if ~isnan(DNAext)
    xlim([min(DNAext)-50 max(DNAext)+50])
end
ylim([min(0,min(FM)) max(FM)*1.2])
xlabel('DNA extension (nm)')
ylabel('Force (pN)')

legend([num2str(dsDNA1) 'bp dsDNA, ssDNA length interval 100'])

if trapData.oldtrap == 1
    instrument = 'Old-Trap';
elseif trapData.oldtrap == 0
    instrument = 'Fleezers';
end
% title({[ construct ' construct, ' instrument] ['data file: ' trapData.file(1:end-4)]})
title([ instrument ', fext: ' trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ', fakedsDNA1: ' num2str(dsDNA1) 'bp, ss interval: 100'])
set(gcf, 'Name',[ 'Fext_' trapData.file(1:end-4)])

end