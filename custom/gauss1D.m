function [realDensity] = gauss1D(m, v, N, w)

    pos = (-w:(2*w/N):w-w/N);
    meanV = repmat(m,N,1)';

    aux = pos - meanV;
    insE = (aux.*aux)./(v^2)*(-0.5);
    norm = 1/(v*sqrt(2*pi));

    res = norm.*exp(insE);

    realDensity = [pos;res];

end