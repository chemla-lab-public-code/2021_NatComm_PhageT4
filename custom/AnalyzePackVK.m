function [stat, FileNames] = AnalyzePackVK(packcell, iwin, istep)
    
    FileNames = [];
for cellk = 1:length(packcell)
    %% load data
%     f1 = figure();
%         rawf_bp = subplot(2,1,1)
%     plot(packcell{1,cellk}.time,packcell{1,cellk}.length)
%     title(packcell{1,cellk}.name)
%         rawf_pN = subplot(2,1,2)
%         plot(packaacell{1,k}.time,packcell{1,k}.force)
%         title('force in pN')
%     grid on
%     set(gcf,'Name',packcell{1,cellk}.name)
    
    % linkxx

    data.crop.time = packcell{1,cellk}.time;
    data.crop.DNAbp = packcell{1,cellk}.length;
    data.crop.force = packcell{1,cellk}.force;
%     FileNames(cellk).file = packcell{1,cellk}.name;
    %% calculate Velocity by moving average
    [t_vel,vel] = Velocity(data.crop.time,data.crop.DNAbp,iwin,istep);
    %190909 by Suoang
    vel = smooth(t_vel,vel,40,'loess');    % sample period  = t_vel(2) - t_vel(1) which is 0.01.
    vel = vel';
    
    % left y axis is extension, right y axis is Velocity.
%     [ax,h1,h2] = plotyy(data.crop.time,data.crop.DNAbp,t_vel,vel);
%     xlabel('Time (s)');
%     ax1 = get(f1,'Childre');    % For plotting in specific axes later
%     set(h1,'LineWidth', 1.5);
%     set(h2,'LineWidth', 1.5);
%     
%     ylabel(ax1(2),'DNA length (bp)'); % The left y axis is the 2nd axis;
%     ylabel(ax1(1),'Packaging Velocity (bp/s)'); % The right y axis is the 1st axis;
%     title(['Cropped '])
%     set(f1,'Name',['Cropped '])
    
    % Method: Silverman's rule of thumb
    % to determin the band width of kernel density estimation
    [bandwidth_crop, std_dev_crop, delta_iqr_crop] = Silverman(vel);
    % KDE
    vel_crop_kde = kde(vel, bandwidth_crop);
%     f2 = figure();
%     plot(vel_crop_kde(1, :), vel_crop_kde(2, :), 'r', 'LineWidth', 1.5);
%     grid on
%     ylabel('KDE Estimated Distribution')
%     xlabel('Velocity (bp/s)')
%     hold on;
%     for k = 1 : length(vel)
%         y2 = 0.5*max(vel_crop_kde(2, :));
%         line([vel(k) vel(k)],[0,y2])
%         hold on
%     end
    
    % mark the peaks in KDE distribution
    [pks,vel_crop_pks] = findpeaks(vel_crop_kde(2, :),vel_crop_kde(1, :),'SortStr','descend');
    k = 1;
    while k <= length(pks) && pks(k) > 0.5*max(pks(1)) 
%         text(vel_crop_pks(k)+.02,pks(k),num2str(vel_crop_pks(k),4))
        k = k+1;
    end
%     title(['KDE Band Width:' num2str(bandwidth_crop) ' bp/s'])
%     set(f2,'Name',['KDE '])
    
    % Select Velocities that are pauses & slips
%     k = 1;
%     tail = 0;
%     while tail < 0.003
%         tail = tail + (Vel_Crop_Kde(1, 2) - Vel_Crop_Kde(1, 1))*Vel_Crop_Kde(2, k);
%         k = k+1;
%     end
%     threshold = max(Vel_Crop_Kde(1, k-1),50);

    % find the pauses and slips
    % for normal distribution, iqr = 1.34896 sigma;
    % Assume in KDE the highest peak is the mean and 3 sigma ~=2.22 iqr
    threshold = max(max(vel_crop_pks(1:k-1))-2.22*delta_iqr_crop, 50);
    idx = find(vel < threshold & vel >= -2000); % select pauses [pause_Vel-2*std_pause_Vel]
    sidx = find(vel <= -2000); % select slips
%     line([threshold threshold],[0 y2],'Color','black','LineStyle','--','LineWidth',2)
    
%     figure(f1); %     set(0, 'currentfigure', f);  %# for figures
%     set(f1, 'currentaxes', ax1(1));     hold on
%     plot(t_vel(idx),vel(idx),'ro');     hold on
%     plot(t_vel(sidx),vel(sidx),'co');
    
    % Select times that are pauses in original time trace
    dt = data.crop.time(1+iwin)-data.crop.time(1);
    pause_idx = []; temp = [];
    for j = 1:length(idx)
        temp = find(data.crop.time >= t_vel(idx(j))-dt/2 & data.crop.time <= t_vel(idx(j))+dt/2);
        pause_idx = [pause_idx temp];
    end;
    pause_idx = unique(pause_idx);
%     set(f1, 'currentaxes', ax1(2));     hold on
%     plot(data.crop.time(pause_idx),data.crop.DNAbp(pause_idx),'r.');
    
    % Select times that are slips in original time trace
    slip_idx = [];
    for j = 1:length(sidx)
        temp = find(data.crop.time >= t_vel(sidx(j))-dt/2 & data.crop.time <= t_vel(sidx(j))+dt/2);
        slip_idx = [slip_idx temp];
    end;
    slip_idx = unique(slip_idx);
    pause_idx = setdiff(pause_idx,slip_idx); % split up any pauses containing a slip
%     plot(data.crop.time(slip_idx),data.crop.DNAbp(slip_idx),'c.');
    
    % Select start and end points for each pause
    ddidx = find(diff(diff(pause_idx)) >= 10);    
    pstart_idx = [1 ddidx+2];
    pend_idx = [ddidx+1 length(pause_idx)];
    
    % Select start and end points for each pause
    ddidx = find(diff(diff(slip_idx)) >= 10);    
    sstart_idx = [1 ddidx+2];
    send_idx = [ddidx+1 length(slip_idx)];
   
    %Yann's ghetto edit------------
    if isempty(pause_idx) == 1;
        pause_start = [];
        pause_end = [];
    else
        pause_start = pause_idx(pstart_idx);
        pause_end = pause_idx(pend_idx);  
    end
    
    if isempty(slip_idx) == 1;
        slip_start = [];
        slip_end = [];
    else
        slip_start = pause_idx(sstart_idx);
        slip_end = pause_idx(send_idx);  
    end
    
    % Mark index of each pause
    for j = 1:length(pause_start)
%         text(data.crop.time(pause_start(j)),data.crop.DNAbp(pause_start(j)),num2str(j));
    end  
    
    % Select start and end points for each slip
    if ~isempty(slip_idx)
        ddsidx = find(diff(diff(slip_idx)) >= 10);
        sstart_idx = [1 ddsidx+2];
        send_idx = [ddsidx+1 length(slip_idx)];
        slip_start = slip_idx(sstart_idx);
        slip_end = slip_idx(send_idx);
    else
        slip_start = [];
        slip_end = [];
    end;
    
    
    % Catenate time trace without pauses
    pause_slip_start = [pause_start slip_start];
    pause_slip_end = [pause_end slip_end];
    [pause_slip_start,sort_idx] = sort(pause_slip_start);
    pause_slip_end = pause_slip_end(sort_idx);
    pack_start = [1 pause_slip_end+1];
    pack_end = [pause_slip_start length(data.crop.time)];
    cat_DNA_length = []; temp = [];
    cat_DNA_length = data.crop.DNAbp(pack_start(1):pack_end(1)); %-1
    for j = 2:length(pack_start)
        temp = data.crop.DNAbp(pack_start(j):pack_end(j)) - data.crop.DNAbp(pack_start(j)) + cat_DNA_length(end); % DNA_Length{i}(pack_end(j-1));
        cat_DNA_length = [cat_DNA_length temp];
    end;
    cat_time = (0:length(cat_DNA_length)-1)*(data.crop.time(2)-data.crop.time(1));
%     plot(data.crop.time(pack_start),data.crop.DNAbp(pack_start),'m+');
%     plot(data.crop.time(pack_end),data.crop.DNAbp(pack_end),'mo');
%     plot(cat_time,cat_DNA_length,'c');

    % Compile statistics on pauses and packaging
    
    [~,vel_pack] = Velocity(cat_time,cat_DNA_length,iwin,istep);
%     [t_Vel_pack,Vel_pack] = Velocity(cat_Time,cat_DNA_Length,iwin,istep);

    % KDE Bandwidth Method: Silverman's rule of thumb to determin the band width of kernel density estimation
    [bandwidth_pack, std_dev_pack, delta_iqr_pack] = Silverman(vel_pack);
    vel_pack_kde = kde(vel_pack, bandwidth_pack);

%
    mean_vel_pack = mean(vel_pack);
    std_vel_pack = std(vel_pack);
    sem_vel_pack = std_vel_pack/sqrt(length(cat_DNA_length)/iwin);
    
    num_pause = length(pause_start);
    pause_duration = zeros (1,length(num_pause));
    pack_length = zeros (1,length(pack_start));
    vel_pause = zeros (1,length(num_pause));
    
    num_slip = length(slip_start);
    slip_duration = zeros (1,length(num_slip));
    slip_length = [];
    vel_slip = zeros (1,length(num_slip));
    pause_unpack = zeros (1,length(num_pause));
    
    for j = 1:length(pack_start)
        pack_length(j) = data.crop.DNAbp(pack_start(j)) - data.crop.DNAbp(pack_end(j));
    end;
    
    for j = 1:num_pause
        pause_duration(j) = data.crop.time(pause_end(j)) - data.crop.time(pause_start(j));
        pause_unpack(j) = data.crop.DNAbp(pause_end(j)) - data.crop.DNAbp(pause_start(j)); % added line YRC 12/14/10
        a = polyfit(data.crop.time(pause_start(j):pause_end(j)),data.crop.DNAbp(pause_start(j):pause_end(j)),1);
        vel_pause(j) = -a(1);
    end;
    
    for j = 1:num_slip
        slip_duration(j) = data.crop.time(slip_end(j)) - data.crop.time(slip_start(j));
        b = polyfit(data.crop.time(slip_start(j):slip_end(j)),data.crop.DNAbp(slip_start(j):slip_end(j)),1);
        vel_slip(j) = -b(1);
    end;
    
    mean_pause_duration = mean(pause_duration);
    std_pause_duration = std(pause_duration);
    sem_pause_duration = std(pause_duration)/sqrt(num_pause);

    mean_pause_unpack = mean(pause_unpack); % added these 3 lines YRC 12/14/10
    std_pause_unpack = std(pause_unpack); %
    sem_pause_unpack = std(pause_unpack)/sqrt(num_pause); %

    mean_pack_length = mean(pack_length);
    std_pack_length = std(pack_length);
    sem_pack_length = std(pack_length)/sqrt(length(pack_start));
    
    mean_vel_pause = mean(vel_pause);
    std_vel_pause = std(vel_pause);
    sem_vel_pause = std(vel_pause)/sqrt(num_pause);
    
    mean_slip_velocity = mean(vel_slip);
    std_slip_velocity = std(vel_slip);
    sem_slip_velocity = std(vel_slip)/sqrt(num_slip);

%     % Plot everything out
%     f3 = figure('Name',['File: ' ]);
%     kk(1) = subplot(2,2,1);
%     plot(data.crop.time,data.crop.DNAbp,'b'); hold on;
%     plot(cat_time,cat_DNA_length,'c');
%     grid on
%     for j = 1:num_pause
%         plot(data.crop.time(pause_start(j):pause_end(j)),data.crop.DNAbp(pause_start(j):pause_end(j)),'r');
%     end;
%     xlabel('Time (s)');
%     ylabel('DNA length (bp)');
%     s0 = ['$N_{pause}=$' ' ' num2str(num_pause,4) ', '];
%     s1 = ['$\overline{\Delta t}_{pause}=$' num2str(mean_pause_duration,4)];
%     s2 = ['$\pm$',num2str(sem_pause_duration,4),'$s$'];
% %     xlabel(s2, 'Interpreter', 'LaTeX');
% %     title(['#Pause: ',num2str(num_pause),' Mean pause duration: ',num2str(mean_pause_duration),'\pm',num2str(sem_pause_duration),'s']);
%     title([s0 s1 s2], 'Interpreter', 'LaTeX');
% %     title('$\hat{\psi}$','Interpreter','latex')
%     % latex    \[ a^2_{i_1} = b^2_{i,j} \]
%     
%     kk(2) = subplot(2,2,2);
%     plot(data.crop.time,data.crop.force,'b')
%     grid on
%     xlabel('Time (s)');
%     ylabel('Force (pN)');
%     s0 = '$\overline{\Delta l}_{pack}=$';
%     s1 = [num2str(mean_pack_length,4),'$\pm$',num2str(sem_pack_length,4),'$bp$'];
%     title([s0 s1], 'Interpreter', 'LaTeX');
%     
%     kk(3) = subplot(2,2,3);
%     plot(t_vel,vel); hold on;
%     plot(t_vel(idx),vel(idx),'r.');
%     plot(t_vel,threshold*ones(1,length(t_vel)),'g--');
%     grid on
%     xlabel('Time (s)');
%     ylabel('Velocity (bp/s)');
% 
%     subplot(2,2,4);
%     plot(vel_pack_kde(1, :), vel_pack_kde(2, :), 'r', 'LineWidth', 1);
%     grid on
%     ylabel('KDE Estimated Distribution')
%     xlabel('Velocity (bp/s)')
%     hold on;
%     for k = 1 : length(vel_pack)
%         y2 = 0.5*max(vel_pack_kde(2, :));
%         line([vel_pack(k) vel_pack(k)],[0,y2])
%         hold on
%     end
    
    % find the highest Velocity
    pks = [];
    [pks,vel_pack_pks] = findpeaks(vel_pack_kde(2, :),vel_pack_kde(1, :),'SortStr','descend');
    k = 1;
%     while k <= length(pks) && pks(k) > 0.5*max(pks(1)) 
%         text(vel_pack_pks(k)+.02,pks(k),num2str(vel_pack_pks(k),4))
%         k = k+1;
%     end
%     s0 = ['$BandWidth=$' num2str(bandwidth_pack,4) '$bp/s$, '];
%     s1 = ['$\overline{v}=$' ,num2str(mean_vel_pack,4),'$\pm$',num2str(std_vel_pack,4),'$bp/s$'];
%     title({[s0 s1],''}, 'Interpreter', 'LaTeX')
%    
%     
%     linkaxes(kk, 'x');
    
%     CurrentSpace = whos;
%     for k = 1 : length(CurrentSpace)
%         display(CurrentSpace(k).name)
%     end

%     path = 'D:\Research\Matlab\';
%     file = data{i}.file;
%     saveas(gcf,[sprintf('%s',path),'Figures_Temp\AnalyzePackaging\AnalyzePackaging_',file,'.fig'], 'fig');
%     

    %% Output all parameters
    stat(cellk).num = num_pause;
    stat(cellk).start = pause_start;
    stat(cellk).end = pause_end;
    stat(cellk).duration = pause_duration;
    stat(cellk).unpack = pause_unpack; % added line YRC 12/14/10
    stat(cellk).pack_length = pack_length;
    stat(cellk).vel_pause = vel_pause;
    stat(cellk).sum_pack_length = sum(pack_length);    % add line SL 2019/07/08
    stat(cellk).mean_pause_duration = mean_pause_duration;
    stat(cellk).std_pause_duration = std_pause_duration;
    stat(cellk).sem_pause_duration = sem_pause_duration;
    stat(cellk).mean_unpack = mean_pause_unpack; % added 3 lines YRC 12/14/10
    stat(cellk).std_unpack = std_pause_unpack; % note by SL: this pause_unpack refers to length
    stat(cellk).sem_unpack = sem_pause_unpack; %
    stat(cellk).mean_pack_length = mean_pack_length;
    stat(cellk).std_pack_length = std_pack_length;
    stat(cellk).sem_pack_length = sem_pack_length;
    stat(cellk).mean_vel_pack = mean_vel_pack;
    stat(cellk).std_vel_pack = std_vel_pack;
    stat(cellk).sem_vel_pack = sem_vel_pack;
    stat(cellk).mean_vel_pause = mean_vel_pause;
    stat(cellk).std_vel_pause = std_vel_pause;
    stat(cellk).sem_vel_pause = sem_vel_pause;   
    stat(cellk).slip_num = num_slip;
    stat(cellk).slip_start = slip_start;
    stat(cellk).slip_end = slip_end;
    stat(cellk).slip_duration = slip_duration;
    stat(cellk).vel_slip = vel_slip;
    stat(cellk).mean_slip_velocity = mean_slip_velocity;
    stat(cellk).std_slip_velocity = std_slip_velocity;
    stat(cellk).sem_slip_velocity = sem_slip_velocity;
%     
% %     pause_param{i}.pause_time_start = pause_idx;
% %     pause_param{i}.slip_time_start = slip_idx;
%     
%     % Output data trace 
%     packaging{i}.time = data.crop.time;
%     packaging{i}.length = data.crop.DNAbp;
%     packaging{i}.force = Force{i};
%     packaging{i}.name = file;

%     close(f1)
%     close(f2)
end
end


