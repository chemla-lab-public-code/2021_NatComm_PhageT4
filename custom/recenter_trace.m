function recenter_trace(fig,t1,t2,target_figure)

g1 = fig.Children;

g2 = g1(4).Children;

for i = 1:length(g2)
    if ~isempty(strncmpi(class(g2(i)),'Line',1))
        if strcmp(g2(i).DisplayName,'DNAbp')
            ax = g2(i);
        end  
    end
end

if exist('ax')
    XData = ax.XData(ax.XData > t1 & ax.XData < t2);
    YData = ax.YData(ax.XData > t1 & ax.XData < t2);
    XData = XData-XData(1);
    target_figure;
    hold on
    plot(XData,YData,'DisplayName',[fig.Name(4:9) '\_' fig.Name(end-7:end-5)])
    hold on
end
end