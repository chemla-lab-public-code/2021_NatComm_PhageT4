% Method: Silverman's rule of thumb
% to determin the band width of kernel density estimation
function bandwidth_crop = plot_kde(sample,f,varargin)
    if nargin == 2 
        [bandwidth_crop, std_dev_crop, delta_iqr_crop] = Silverman(sample);
    else
        bandwidth_crop = varargin{1};
    end
%      bandwidth_crop = 57;
    % KDE
    Kde_xy = kde(sample, bandwidth_crop);
    f;% = figure();
    plot(Kde_xy(1, :), Kde_xy(2, :), 'r', 'LineWidth', 1.5);
    grid on
%         ylabel(text_ylabel)
%         xlabel(text_xlabel)
    hold on;
    for k = 1 : length(sample)
        y2 = 0.5*max(Kde_xy(2, :));
        line([sample(k) sample(k)],[0,y2])
        hold on
    end

    % mark the peaks in KDE distribution
    [pks,pos] = findpeaks(Kde_xy(2, :),Kde_xy(1, :),'SortStr','descend');
    k = 1;
    while k <= length(pks) && pks(k) > 0.5*max(pks(1)) 
        text(pos(k)+.02,pks(k),num2str(pos(k),4))
        k = k+1;
    end
    title(['KDE Band Width:' num2str(bandwidth_crop)])
%         set(f,'Name',['KDE ' data.file])
end