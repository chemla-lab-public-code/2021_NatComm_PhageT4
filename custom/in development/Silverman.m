function [h_short, std_dev, delta_iqr]= Silverman(data)
    % compute bandwidth for KDE with optimized Silverman's method "SHORT:silverman H Rule Of Thumb"
    % Input
    %  data    :    matrix of data points
    % Output
    %  h_short : bandwidth
    %  std_dev : standard deviation of data points
    %  delta_iqr : interquartile range of data points

%     std_dev = std(data);
%     delta_iqr = iqr(data)/1.34;
%     A = min(std_dev, delta_iqr/1.34);
%     h_short = 0.9*A*length(data)^-0.2;
    std_dev = std(data);
    delta_iqr = iqr(data)/1.34;
    A = min(std_dev, delta_iqr);
    h_short = 0.9*A*length(data)^-0.2;
end