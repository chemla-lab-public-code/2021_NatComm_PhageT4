function indicators = CrooksFT( stat,trace )
% Find the qualified interval with constant velocity
figure()
indicators = {}; idx_indicators = 1;
for i = 1:length(stat)
    switch stat(i).num
        case 0
            time = {trace{1,i}.packbp(1,:)};
            DNAbp = {trace{1,i}.packbp(2,:)};
        case 1
            time = {...
                trace{1,i}.packbp(1,1:stat(i).start(1)),...
                trace{1,i}.packbp(1,stat(i).end(1):end)...
                };
            DNAbp = {...
                trace{1,i}.packbp(2,1:stat(i).start(1)),...
                trace{1,i}.packbp(2,stat(i).end(1):end)...
                };
        otherwise
            time = {};
            time{1} = trace{1,i}.packbp(1,1:stat(i).start(1));
            for k = 2:stat(i).num
                time{k} = trace{1,i}.packbp(1,stat(i).end(k-1):stat(i).start(k));
            end
            time{k+1} = trace{1,i}.packbp(1,stat(i).end(k):end);

            DNAbp = {};
            DNAbp{1} = trace{1,i}.packbp(2,1:stat(i).start(1));
            for k = 2:stat(i).num
                DNAbp{k} = trace{1,i}.packbp(2,stat(i).end(k-1):stat(i).start(k));
            end
            DNAbp{k+1} = trace{1,i}.packbp(2,stat(i).end(k):end);
    end
    % calculate deltaX =X(t+0.1s)-X(t)
    indicator = [];
    for dt = 1:10
        dX=[];
        for j = 1:length(DNAbp)
            timeseg = time{1,j};
            DNAbpseg = DNAbp{1,j};
            t1=1;
            while t1+dt<length(timeseg)
                dX = [dX DNAbpseg(t1+dt)-DNAbpseg(t1)];
                t1 = t1+1;
            end
        end
        figure(i+1)
        subplot(2,5,dt);hold on
        h = histogram(dX,'Normalization','pdf');
        title([num2str(i) '\_dt\_' num2str(dt)])
        set(gcf,'Name',[num2str(i) '_' num2str(j)])
        BinCts = ([0 h.BinEdges] + [h.BinEdges 0])/2;
        BinCts = BinCts(2:end-1);
        clear f
        try
            % f=fit(BinCts',h.Values','gauss1','Lower',[-Inf -10000 -Inf],'Upper',[Inf 0 Inf]);
            f = fitdist(dX','Normal');
            hold on
            cfd =  paramci(f);
            l = min([2*cfd(2,1)/cfd(1,2)^2 2*cfd(1,1)/cfd(2,1)^2 2*cfd(2,1)/cfd(2,2)^2 2*cfd(1,1)/cfd(2,2)^2]);
            u = max([2*cfd(2,1)/cfd(1,2)^2 2*cfd(1,1)/cfd(2,1)^2 2*cfd(2,1)/cfd(2,2)^2 2*cfd(1,1)/cfd(2,2)^2]);
            indicator = [indicator [dt;2*f.mu/f.sigma^2;l;u]];
            plot(BinCts,1/sqrt(2*pi)/f.sigma*exp(-(BinCts-f.mu).^2/2/f.sigma^2),'DisplayName',num2str(2*f.mu/f.sigma^2))
        catch ME
            fprintf('%s\n',ME.message)
            for kk = 1 : length(ME.stack)
                % ME.stack(kk)
                %                             fprintf('Line: %d, Name: %s\n', ME.stack(i).line, ME.stack(i).name)
            end
            
        end
        % pause;
        % close(gcf);
    end
    
    if ~isempty(indicator)
        figure(1)
        hold on
        title('Indicator vs deltaT')
        set(gcf,'Name',num2str(i))
        my_col = hsv(7);
%         plot(indicator(1,:),indicator(2,:),'Color',my_col(ceil(stat(i).mean_vel_pack/250),:),...
%             'LineWidth',1.5,'LineStyle','--','DisplayName',['trace\_' num2str(i)])
%         herr=errorbar(indicator(1,:),indicator(2,:),indicator(3,:),indicator(4,:),...
%             'Color',my_col(ceil(stat(i).mean_vel_pack/250),:),'LineWidth',2,'LineStyle','--','DisplayName',['errorbar\_' num2str(i)]);
        plot(indicator(1,:),smooth(indicator(2,:)),'Color',my_col(ceil(stat(i).mean_vel_pack/250),:),...
            'LineWidth',1.5,'LineStyle','-','DisplayName',['trace\_' num2str(i)])
        herr=errorbar(indicator(1,:),indicator(2,:),indicator(3,:),indicator(4,:),...
            'Color',my_col(ceil(stat(i).mean_vel_pack/250),:),'LineWidth',2,'LineStyle','-','DisplayName',['errorbar\_' num2str(i)]);
        set(herr,'Visible','off')
        
    indicators{idx_indicators,1} = indicator(1:2,:)';
    idx_indicators = idx_indicators + 1;
    end
    clear indicator
end
% Probability distribution (P(deltaX)) of deltaX

% An example of the force indicator w (eqn (1)) calculated for each P(DX) at a different Dt (10 ms rDt r 100 ms) (black). After

% ?The thin curves (n = 10) represent w calculated based on the boot-strapping method for the error estimation (see Methods section). The estimated error of w was 10%, which was consistent with the systematic error of the experimental system. The thick orange curve represents w after the smoothing filtering (see Methods section).
end

