
%get fixed K of knn and fixed h of kde
p = parameters();

disp('Question: Kernel/K-Nearest Neighborhood Density Estimators');

% Produce the random samples
samples = random('norm', 0, 1, 1, 100);

% Compute the original normal distribution
realDensity = gauss1D(0, 1, 100, 5);

% Estimate the probability density using the KDE
estDensity = kde(samples, p.h);

% plot results
figure;

plot(estDensity(1, :), estDensity(2, :), 'r', 'LineWidth', 1.5);
hold on;
plot(realDensity(1, :), realDensity(2, :), 'b', 'LineWidth', 1.5);
legend('KDE Estimated Distribution', 'Real Distribution');
hold on;
plot(samples, 0.2*ones(1,length(samples)))

% Estimate the probability density using KNN
estDensity = knn(samples, p.k);

% Plot the distributions
figure;
plot(estDensity(1, :), estDensity(2, :), 'r', 'LineWidth', 1.5);
hold on;
plot(realDensity(1, :), realDensity(2, :), 'b', 'LineWidth', 1.5);
legend('KNN Estimated Distribution', 'Real Distribution');
hold off;








