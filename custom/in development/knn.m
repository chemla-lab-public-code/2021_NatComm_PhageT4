function estDensity = knn(samples, k)
    % compute density estimation from samples with KNN
    % Input
    %  samples    : DxN matrix of data points
    %  k          : number of neighbors
    % Output
    %  estDensity : estimated density in the range of [-5, 5]

    % Compute the number of the samples created
    N = length(samples);

    % Create a linearly spaced vector
    pos = linspace(-5, 5, 100);

    % Create two big matrices to avoid for loops
    x = repmat(pos, N, 1);
    samples = repmat(samples', 1, length(pos));

    % Sort the distances so that we can choose the k-th point
    dists = sort(abs(x-samples), 1);

    % Estimate the probability density using the k-NN density estimation
    % dists(k, :) = V/2;    
    res = (k/(2*N)) ./ dists(k, :);

    % Form the output variable
    estDensity = [pos; res];

end