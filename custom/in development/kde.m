function estDensity = kde(samples, h)
    % compute density estimation from samples with KDE
    % Input
    %  samples    : DxN matrix of data points
    %  h          : (half) window size/radius of kernel
    % Output
    %  estDensity : estimated density in the range of [-5,5]

    % Compute the number of samples created
    N = length(samples);

    % Create a linearly spaced vector
    pos = linspace(floor(min(samples)), ceil(max(samples)), 100);

    % Create two big matrices to avoid for loops
    x = repmat(pos, N, 1);
    samples = repmat(samples', 1, length(pos));
    % Estimate the density from the samples using a kernel density estimator
    % ?????????????????? ??????
    res = sum(exp(-(x-samples).^2./(2*h^2)), 1) ./ (sqrt(2*pi)*h*N);

    % Form the output variable
    estDensity = [pos; res];

end