function [packaging, pause_param] = AnalyzePackaging(data, iwin,istep)

    figure;
    plot(data.time,data.DNAbp)
    % Crop data
    title('Select data to analyze and press spacebar');
    set(gcf,'Name',data.file)
    pause;

    cropmarks = axis;
    tmax = cropmarks(2); tmin = cropmarks(1);    
    crop = find(data.time >= tmin & data.time <=tmax);
    
    data.crop.force_X = data.force_X(crop); 
    data.crop.force_Y = data.force_Y(crop); 
    data.crop.DNAext_X = data.DNAext_X(crop); 
    data.crop.DNAext_Y = data.DNAext_Y(crop); 
    data.crop.DNAbp = data.DNAbp(crop); 
    
    data.crop.time = data.time(crop);
    data.crop.timestamp = data.crop.time(1);
    data.crop.time = data.crop.time - data.crop.timestamp;
    
    figure()
    plot(data.crop.time,data.crop.DNAbp)
    title('Cropped Packaging Trace')
    set(gcf,'Name',['cropped ' data.file])
    
   
    
    % Calculate velocity, velocity histogram and fit to double Gaussian
    figure();
    [t_vel,vel] = Velocity(data.crop.time,data.crop.DNAbp,iwin,istep);
    bin_width = 50;
    bin = (floor(min(vel)/bin_width)*bin_width):bin_width:(ceil(max(vel)/bin_width)*bin_width);
    count = hist(vel,bin);
    stairs(bin,count,'b'); hold on;
    [xguess,Aguess] = ginput(2);
    beta0 = [Aguess(1) xguess(1) 50 Aguess(2) xguess(2) 50];
    beta = nlinfit(bin,count,@DoubleGaussian,beta0);
    if beta(4) <= 1 % if it can't fit Gaussian
        beta(4) = 0;
        beta(5) = 0; 
        beta(6) = 50;
    end;
    if beta(1) <= 1 % if it can't fit Gaussian
        beta(1) = 0;
        beta(2) = 0; 
        beta(3) = 50;
    end;    
    
    count_fit = DoubleGaussian(beta,bin);
    plot(bin,count_fit,'r');
    
    % Select velocities that are pauses & slips
    [pause_vel,pause_vel_idx] = max([beta(2) beta(5)]);
    if pause_vel_idx == 1
        std_pause_vel = beta(3);
    else
        std_pause_vel = beta(6);
    end;    
    if pause_vel - 3*std_pause_vel > 50 
        threshold = pause_vel - 3*std_pause_vel;
    else
        threshold = 50;
    end;    
    idx = find(vel <= threshold & vel >= -2000); % select pauses [pause_vel-2*std_pause_vel]
    sidx = find(vel <= -2000); % select slips
    plot(threshold,0,'go'); %[pause_vel-2*std_pause_vel]
    
    pause;
    clf;
    
    plot(t_vel,vel); hold on;
    plot(t_vel(idx),vel(idx),'r.');
    plot(t_vel(sidx),vel(sidx),'c.');
    
%     % Select times that are pauses in original time trace
%     pause; clf;
%     dt = Time{i}(1+iwin)-Time{i}(1);
%     pause_idx = []; temp = [];
%     for j = 1:length(idx)
%         temp = find(Time{i} >= t_vel(idx(j))-dt/2 & Time{i} <= t_vel(idx(j))+dt/2);
%         pause_idx = [pause_idx temp];
%     end;
%     pause_idx = unique(pause_idx);
%     plot(Time{i},DNA_Length{i}); hold on;
%     plot(Time{i}(pause_idx),DNA_Length{i}(pause_idx),'r.');
%     
%     % Select times that are slips in original time trace
%     slip_idx = [];
%     for j = 1:length(sidx)
%         temp = find(Time{i} >= t_vel(sidx(j))-dt/2 & Time{i} <= t_vel(sidx(j))+dt/2);
%         slip_idx = [slip_idx temp];
%     end;
%     slip_idx = unique(slip_idx);
%     pause_idx = setdiff(pause_idx,slip_idx); % split up any pauses containing a slip
%     plot(Time{i}(slip_idx),DNA_Length{i}(slip_idx),'c.');
%     
%     % Select start and end points for each pause
%     ddidx = find(diff(diff(pause_idx)) >= 10);    
%     pstart_idx = [1 ddidx+2];
%     pend_idx = [ddidx+1 length(pause_idx)];
%     
%      % Select start and end points for each pause
%     ddidx = find(diff(diff(slip_idx)) >= 10);    
%     sstart_idx = [1 ddidx+2];
%     send_idx = [ddidx+1 length(slip_idx)];
%    
%     %Yann's ghetto edit------------
%     if isempty(pause_idx) == 1;
%         pause_start = [];
%         pause_end = [];
%     else
%         pause_start = pause_idx(pstart_idx);
%         pause_end = pause_idx(pend_idx);  
%     end
%     
%     if isempty(slip_idx) == 1;
%         slip_start = [];
%         slip_end = [];
%     else
%         slip_start = pause_idx(sstart_idx);
%         slip_end = pause_idx(send_idx);  
%     end
%     
%     plot(Time{i}(pause_start),DNA_Length{i}(pause_start),'g+');
%     plot(Time{i}(pause_end),DNA_Length{i}(pause_end),'go');
%     for j = 1:length(pause_start)
%         text(Time{i}(pause_start(j)),DNA_Length{i}(pause_start(j)),num2str(j));
%     end;    
%     
%     % Select start and end points for each slip
%     if ~isempty(slip_idx)
%         ddsidx = find(diff(diff(slip_idx)) >= 10);
%         sstart_idx = [1 ddsidx+2];
%         send_idx = [ddsidx+1 length(slip_idx)];
%         slip_start = slip_idx(sstart_idx);
%         slip_end = slip_idx(send_idx);
%     else
%         slip_start = [];
%         slip_end = [];
%     end;
%     
%     % plot(Time{i}(pause_idx(pause_start)),DNA_Length{i}(pause_idx(pause_start)),'g+');
%     % plot(Time{i}(pause_idx(pause_end)),DNA_Length{i}(pause_idx(pause_end)),'go');
%     
%     pause;
%     
%     % Catenate time trace without pauses
%     pause_slip_start = [pause_start slip_start];
%     pause_slip_end = [pause_end slip_end];
%     [pause_slip_start,sort_idx] = sort(pause_slip_start);
%     pause_slip_end = pause_slip_end(sort_idx);
%     pack_start = [1 pause_slip_end+1];
%     pack_end = [pause_slip_start length(Time{i})];
%     cat_DNA_Length = []; temp = [];
%     cat_DNA_Length = DNA_Length{i}(pack_start(1):pack_end(1)); %-1
%     for j = 2:length(pack_start)
%         temp = DNA_Length{i}(pack_start(j):pack_end(j)) - DNA_Length{i}(pack_start(j)) + cat_DNA_Length(end); % DNA_Length{i}(pack_end(j-1));
%         cat_DNA_Length = [cat_DNA_Length temp];
%     end;
%     cat_Time = (0:length(cat_DNA_Length)-1)*(Time{i}(2)-Time{i}(1));
%     plot(Time{i}(pack_start),DNA_Length{i}(pack_start),'m+');
%     plot(Time{i}(pack_end),DNA_Length{i}(pack_end),'mo');
%     plot(cat_Time,cat_DNA_Length,'c');
%     
%     pause; close;
%     
%     % Compile statistics on pauses and packaging
%     [t_vel_pack,vel_pack] = velocity(cat_Time,cat_DNA_Length,iwin);
%     count_pack = hist(vel_pack,bin);
%     mean_vel_pack = mean(vel_pack);
%     std_vel_pack = std(vel_pack);
%     sem_vel_pack = std_vel_pack/sqrt(length(cat_DNA_Length)/iwin);
%     
%     num_pause = length(pause_start);
%     pause_duration = [];
%     pack_length = [];
%     vel_pause = [];
%     
%     num_slip = length(slip_start);
%     slip_duration = [];
%     slip_length = [];
%     vel_slip = [];
%     pause_unpack = [];
%     
%     for j = 1:length(pack_start)
%         pack_length(j) = DNA_Length{i}(pack_start(j)) - DNA_Length{i}(pack_end(j));
%     end;
%     
%     for j = 1:num_pause
%         pause_duration(j) = Time{i}(pause_end(j)) - Time{i}(pause_start(j));
%         pause_unpack(j) = DNA_Length{i}(pause_end(j)) - DNA_Length{i}(pause_start(j)); % added line YRC 12/14/10
%         a = polyfit(Time{i}(pause_start(j):pause_end(j)),DNA_Length{i}(pause_start(j):pause_end(j)),1);
%         vel_pause(j) = -a(1);
%     end;
%     
%     for j = 1:num_slip
%         slip_duration(j) = Time{i}(slip_end(j)) - Time{i}(slip_start(j));
%         b = polyfit(Time{i}(slip_start(j):slip_end(j)),DNA_Length{i}(slip_start(j):slip_end(j)),1);
%         vel_slip(j) = -b(1);
%     end;
%     
%     mean_pause_duration = mean(pause_duration);
%     std_pause_duration = std(pause_duration);
%     sem_pause_duration = std(pause_duration)/sqrt(num_pause);
% 
%     mean_pause_unpack = mean(pause_unpack); % added these 3 lines YRC 12/14/10
%     std_pause_unpack = std(pause_unpack); %
%     sem_pause_unpack = std(pause_unpack)/sqrt(num_pause); %
% 
%     mean_pack_length = mean(pack_length);
%     std_pack_length = std(pack_length);
%     sem_pack_length = std(pack_length)/sqrt(length(pack_start));
%     
%     mean_vel_pause = mean(vel_pause);
%     std_vel_pause = std(vel_pause);
%     sem_vel_pause = std(vel_pause)/sqrt(num_pause);
%     
%     mean_slip_velocity = mean(vel_slip);
%     std_slip_velocity = std(vel_slip);
%     sem_slip_velocity = std(vel_slip)/sqrt(num_slip);
% 
%     % Plot everything out
%     qq = figure('Name',['File: ' data{i}.file]);
%     kk(1) = subplot(2,2,1);
%     plot(Time{i},DNA_Length{i},'b'); hold on;
%     plot(cat_Time,cat_DNA_Length,'c');
%     for j = 1:num_pause
%         plot(Time{i}(pause_start(j):pause_end(j)),DNA_Length{i}(pause_start(j):pause_end(j)),'r');
%     end;
%     xlabel('Time (s)');
%     ylabel('DNA length (bp)');
%     title(['Pause #: ',num2str(num_pause),' Mean pause duration: ',num2str(mean_pause_duration),'\pm',num2str(sem_pause_duration),'s']);
%     
%     kk(2) = subplot(2,2,2);
%     plot(Time{i},Force{i},'b');
%     xlabel('Time (s)');
%     ylabel('Force (pN)');
%     title(['Mean packaging length: ',num2str(mean_pack_length),'\pm',num2str(sem_pack_length),'bp']);
%     
%     kk(3) = subplot(2,2,3);
%     plot(t_vel,vel); hold on;
%     plot(t_vel(idx),vel(idx),'r.');
%     plot(t_vel,threshold*ones(1,length(t_vel)),'g--');
%     xlabel('Time (s)');
%     ylabel('Velocity (bp/s)');
% 
%     subplot(2,2,4);
%     stairs(count,bin,'b'); hold on;
%     stairs(count_pack,bin,'c');
%     plot(count_fit,bin,'r');
%     plot(count,threshold*ones(1,length(count)),'g--');
%     xlabel('Count (a.u.)');
%     ylabel('Velocity (bp/s)');
%     title(['Mean packaging velocity: ',num2str(mean_vel_pack),'\pm',num2str(std_vel_pack),'bp']);
%     
%     linkaxes(kk, 'x');
%         
%     path = 'D:\Research\Matlab\';
%     file = data{i}.file;
%     saveas(gcf,[sprintf('%s',path),'Figures_Temp\AnalyzePackaging\AnalyzePackaging_',file,'.fig'], 'fig');
%     
%     % Output all parameters
%     pause_param{i}.num = num_pause;
%     pause_param{i}.start = pause_start;
%     pause_param{i}.end = pause_end;
%     pause_param{i}.duration = pause_duration;
%     pause_param{i}.unpack = pause_unpack; % added line YRC 12/14/10
%     pause_param{i}.pack_length = pack_length;
%     pause_param{i}.vel_pause = vel_pause;
%     pause_param{i}.mean_duration = mean_pause_duration;
%     pause_param{i}.std_duration = std_pause_duration;
%     pause_param{i}.sem_duration = sem_pause_duration;
%     pause_param{i}.mean_unpack = mean_pause_unpack; % added 3 lines YRC 12/14/10
%     pause_param{i}.std_unpack = std_pause_unpack; %
%     pause_param{i}.sem_unpack = sem_pause_unpack; %
%     pause_param{i}.mean_pack_length = mean_pack_length;
%     pause_param{i}.std_pack_length = std_pack_length;
%     pause_param{i}.sem_pack_length = sem_pack_length;
%     pause_param{i}.mean_vel_pack = mean_vel_pack;
%     pause_param{i}.std_vel_pack = std_vel_pack;
%     pause_param{i}.sem_vel_pack = sem_vel_pack;
%     pause_param{i}.mean_vel_pause = mean_vel_pause;
%     pause_param{i}.std_vel_pause = std_vel_pause;
%     pause_param{i}.sem_vel_pause = sem_vel_pause;   
%     pause_param{i}.slip_num = num_slip;
%     pause_param{i}.slip_start = slip_start;
%     pause_param{i}.slip_end = slip_end;
%     pause_param{i}.slip_duration = slip_duration;
%     pause_param{i}.vel_slip = vel_slip;
%     pause_param{i}.mean_slip_velocity = mean_slip_velocity;
%     pause_param{i}.std_slip_velocity = std_slip_velocity;
%     pause_param{i}.sem_slip_velocity = sem_slip_velocity;
%     
% %     pause_param{i}.pause_time_start = pause_idx;
% %     pause_param{i}.slip_time_start = slip_idx;
%     
%     % Output data trace 
%     packaging{i}.time = Time{i};
%     packaging{i}.length = DNA_Length{i};
%     packaging{i}.force = Force{i};
%     packaging{i}.name = file;
    
end
