function [t_vel,vel]=Velocity(t,l,iwin,istep) % Calculates velocity 
%iwin = 5; 
% istep = 5; 
i=1; 
counter=1; 
N=length(t)-iwin-1; 
while i < N 
    j=i+iwin;
    a=polyfit(t(i:j),l(i:j),1);     % first order polynomial fit (linear fit) 
%     figure()
%     plot(t(i:j),l(i:j))
%     title(num2str(a(1)))
    vel(counter)=-a(1);              %#ok<AGROW> % record velocity data in vector 
    t_vel(counter)=mean(t(i:j)); %#ok<AGROW>
    i=i+istep; 
    counter=counter+1; 
end