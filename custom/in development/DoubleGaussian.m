function y = DoubleGaussian(param,x);

A1 = param(1);
x1 = param(2);
sig1 = param(3);

A2 = param(4);
x2 = param(5);
sig2 = param(6);

y = A1*exp(-(x-x1).^2/(2*sig1^2)) + A2*exp(-(x-x2).^2/(2*sig2^2));