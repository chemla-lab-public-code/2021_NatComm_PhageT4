% % only for 190614
% l=92-68+1;
% fixedX = zeros(5,1);
% fixedY = zeros(5,1);
% err = 999*ones(5,1);
% QPD_X = 2.6027e-06;
% QPD_Y = -1.1829e-05;
% for i = 1:l
% filenum = 67+i;
% rootfolder = [OT '190614/'];
% filename = ['190614_' num2str(filenum,'%03d') '.dat'];
% traceData = ReadJointFile(rootfolder,filename);
% fixedX(i)= traceData.t1x;
% fixedY(i)= traceData.t1y;
% err(i) = (traceData.t1x-QPD_X)^2 + (traceData.t1y-QPD_Y)^2;
% % plot(traceData.time,traceData.QPD.B_Y,traceData.time,traceData.QPD.B_X)
% % title(traceData.file)
% 
% end
% % surf(fixedX,fixedY,err)

% only for 190614
l=92-68+1;
fixedX = zeros(5,1);
fixedY = zeros(5,1);
err = 999*ones(5,1);
QPD_X = -0.0272 %2.6027e-06;
QPD_Y = 0.0060 %-1.1829e-05;
fixedX = [1.273 1.274  1.275 1.276 1.277];
fixedY = [0.7410 0.7430 0.7450 0.7470 0.7490];
for i = 1:l
filenum = 67+i;
rootfolder = [OT '190614/'];
filename = ['190614_' num2str(filenum,'%03d') '.dat'];
traceData = ReadJointFile(rootfolder,filename);
switch traceData.t1x
    case 1.273
        a = 1;
    case 1.274
        a = 2;
    case 1.275
        a = 3;
    case 1.276
        a = 4;
    case 1.277
        a = 5;
end
switch traceData.t1y
    case 0.7410
        b = 1;
    case 0.7430
        b = 2;    
    case 0.7450
        b = 3;
    case 0.7470
        b = 4; 
    case 0.7490
        b = 5;
end
display(a)
display(b)
err(a,b) = (mean(traceData.B_X)-QPD_X)^2 + (mean(traceData.B_Y)-QPD_Y)^2;
display(err(a,b))
% plot(traceData.time,traceData.QPD.B_Y,traceData.time,traceData.QPD.B_X)
% title(traceData.file)

end
surf(fixedX,fixedY,err)