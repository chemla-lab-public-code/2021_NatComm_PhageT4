function indicators = CrooksFT2( stat,trace )
% Find the qualified interval with constant velocity
figure(1)
indicators = {}; idx_indicators = 1;
for i = 1:length(stat)
    switch stat(i).num
        case 0
            time = {trace{1,i}.packbp(1,:)};
            DNAbp = {trace{1,i}.packbp(2,:)};
        case 1
            time = {...
                trace{1,i}.packbp(1,1:stat(i).start(1)),...
                trace{1,i}.packbp(1,stat(i).end(1):end)...
                };
            DNAbp = {...
                trace{1,i}.packbp(2,1:stat(i).start(1)),...
                trace{1,i}.packbp(2,stat(i).end(1):end)...
                };
        otherwise
            time = {};
            time{1} = trace{1,i}.packbp(1,1:stat(i).start(1));
            for k = 2:stat(i).num
                time{k} = trace{1,i}.packbp(1,stat(i).end(k-1):stat(i).start(k));
            end
            time{k+1} = trace{1,i}.packbp(1,stat(i).end(k):end);

            DNAbp = {};
            DNAbp{1} = trace{1,i}.packbp(2,1:stat(i).start(1));
            for k = 2:stat(i).num
                DNAbp{k} = trace{1,i}.packbp(2,stat(i).end(k-1):stat(i).start(k));
            end
            DNAbp{k+1} = trace{1,i}.packbp(2,stat(i).end(k):end);
    end
    figure(i+1) 
    
    % calculate deltaX =X(t+0.1s)-X(t)
    for j = 1:length(DNAbp)
        timeseg = time{1,j};
        DNAbpseg = DNAbp{1,j};
        if length(timeseg)>100
            indicator = [];
            figure(i+1) 
            subplot(3,5,1:5);hold on
            plot(timeseg,DNAbpseg,'DisplayName',[num2str(i) '-' num2str(j)])
            legend on
            %fprintf('i=%d%d\n',[i j])
            for dt = 1:10
                t1=1;dX=[];
                while t1+dt<length(timeseg)
                    dX = [dX DNAbpseg(t1+dt)-DNAbpseg(t1)];
                    t1 = t1+1;
                end
                % Probability distribution (P(deltaX)) of deltaX
                figure(i+1) 
                subplot(3,5,dt+5);hold on
                h = histogram(dX,'Normalization','pdf');
                title([num2str(i) '\_dt\_' num2str(dt)])
                set(gcf,'Name',[num2str(i) '_' num2str(j)])
                BinCts = ([0 h.BinEdges] + [h.BinEdges 0])/2;
                BinCts = BinCts(2:end-1);
                try
                    clear f
                    f = fitdist(dX','Normal');
                    hold on
                    cfd =  paramci(f);
                    l = min([2*cfd(2,1)/cfd(1,2)^2 2*cfd(1,1)/cfd(2,1)^2 2*cfd(2,1)/cfd(2,2)^2 2*cfd(1,1)/cfd(2,2)^2]);
                    u = max([2*cfd(2,1)/cfd(1,2)^2 2*cfd(1,1)/cfd(2,1)^2 2*cfd(2,1)/cfd(2,2)^2 2*cfd(1,1)/cfd(2,2)^2]);
                    indicator = [indicator [dt;2*f.mu/f.sigma^2;l;u]];
                    plot(BinCts,1/sqrt(2*pi)/f.sigma*exp(-(BinCts-f.mu).^2/2/f.sigma^2),'DisplayName',num2str(2*f.mu/f.sigma^2))
                    % pause;
                catch ME
                    fprintf('%s\n',ME.message)
                    for kk = 1 : length(ME.stack)
                        ME.stack(kk)
                        % fprintf('Line: %d, Name: %s\n', ME.stack(i).line, ME.stack(i).name)
                    end
                end
                
                % close(gcf);
            end
            if ~isempty(indicator)
                figure(1)
                hold on
                title('Indicator vs deltaT')
                set(gcf,'Name',num2str(i))
                my_col = hsv(7);
                % plot(indicator(1,:),indicator(2,:),'Color',my_col(ceil(stat(i).mean_vel_pack/250),:),...
                %   'LineWidth',1.5,'LineStyle','--','DisplayName',['trace\_' num2str(i)])
                % herr=errorbar(indicator(1,:),indicator(2,:),indicator(3,:),indicator(4,:),...
                %   'Color',my_col(ceil(stat(i).mean_vel_pack/250),:),'LineWidth',2,'LineStyle','--','DisplayName',['errorbar\_' num2str(i)]);
                
                plot(indicator(1,:),smooth(indicator(2,:)),'Color',my_col(ceil(stat(i).mean_vel_pack/250),:),...
                    'LineWidth',1.5,'LineStyle','-','DisplayName',['trace\_' num2str(i)])
                herr=errorbar(indicator(1,:),indicator(2,:),indicator(3,:),indicator(4,:),...
                    'Color',my_col(ceil(stat(i).mean_vel_pack/250),:),'LineWidth',2,'LineStyle','-','DisplayName',['errorbar\_' num2str(i)]);
                set(herr,'Visible','off')
                indicators{idx_indicators,1} = indicator(1:2,:)';
                indicators{idx_indicators,2} = i;
                idx_indicators = idx_indicators + 1;
            end
            clear indicator
        end
    end
end
end