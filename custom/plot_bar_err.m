function hBar = plot_bar_err(data,err,colorlist,xtickstr,legendstr,titlestr,counts)
    ctrs = 1:size(data,1);
    figure()
    hBar = bar(ctrs,data,'FaceColor','flat');
    clear ctr
    clear ydt
    for k1 = 1:size(data,2)
        ctr(k1,:) = bsxfun(@plus, hBar(1,k1).XData, [hBar(k1).XOffset]');
        ydt(k1,:) = hBar(k1).YData;
    end
    
    if isempty(colorlist)
        colorlist = {[0.4 0.26 0.13],[0.8 0.5 0.2],[0.94 0.87 0.8],[0.94 0.87 0.8]};
    end
    if length(colorlist)< size(data,2)
        warning('Corlor Not Enough')
        for k = length(colorlist)+1 : size(data,2)
            colorlist{k} = {[0.94 0.87 0.8]};
        end
    end
    
    for k2 = 1:size(data,2)
        hBar(k2).FaceColor = colorlist{k2};
    end
    hold on
    temp = errorbar(ctr, ydt, err', '.k');
    set(gca, 'XTickLabel', xtickstr)
    ax = gca;
    ax.XTickLabelRotation = 0;
    legend(legendstr)
    set(gcf,'Name',titlestr)
    title(titlestr)
    
    ctr = reshape(ctr,1,[]);
    ydt = reshape(ydt,1,[]);
    for textnum = 1:length(counts)
        textstr(1,textnum) = {['N = ' num2str(counts(textnum))]};
    end
    text(ctr,ydt,textstr,'HorizontalAlignment','center','VerticalAlignment','bottom');
end

