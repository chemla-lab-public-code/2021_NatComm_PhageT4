function trapData = plot_force_extension(trapData, construct, fitrange)

%% load construct parameters
switch construct
    case 'PCR+Linker'
        hairpin = 0;
        dsDNA1 = 3.4e3;
        dsDNA2 = 3400+906; % ispace and the protein arm is not taken into account.
        ssDNA1 = 0;
        fitoffset = 1;
        fake_offset = 0;
        stdF = [2 15];
end

%% create the WLC model
max_force = max([trapData.force_AX, trapData.force_AY, trapData.force_BX, trapData.force_BY]);
F = 0:0.5:max_force;
[WLC_param] = WLC_parameters;
switch construct
    case {'PCR+Linker'}
        bp = 1*WLC_param.hds*XWLCContour(F,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        bp2 = 1*WLC_param.hds*XWLCContour(F/2,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        WLC_ext = dsDNA1.*bp;   % 3400 bp WLC
        WLC_ext2 = dsDNA2.*bp;  % 4300 bp WLC
        WLC_ext3 = dsDNA2.*bp2; % 4300 bp double tether;
end


%% plot the data
% Filter TrapSep
% TrapSepFilt  = movmean(trapData.TrapSep,30);
% The 2 rows above is replaced with the following home-made code because
% function movmean not exist in my matlab

l = length(trapData.TrapSep);
TrapSepFilt=zeros(1,l);
gauge = 14;
for ii = 1:l
    windowsize = length(max(1,ii-gauge):min(l,ii+gauge));
    for jj = max(1,ii-gauge):min(l,ii+gauge)
        TrapSepFilt(ii) = TrapSepFilt(ii) + trapData.TrapSep(jj);
    end
    TrapSepFilt(ii)=TrapSepFilt(ii)/windowsize;
end

% remove jumps in trap position
TrapSepFilt_diff = diff(TrapSepFilt);
normalized_diff = diff(TrapSepFilt)/mean(TrapSepFilt_diff(TrapSepFilt_diff>0));
TrapSepFiltClean = TrapSepFilt(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.force_AX = trapData.force_AX(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.force_BX = trapData.force_BX(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.force_AY = trapData.force_AY(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.force_BY = trapData.force_BY(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.DNAext_X = trapData.DNAext_X(normalized_diff > 0.8 | normalized_diff < -0.8);
trapData.DNAext_Y = trapData.DNAext_Y(normalized_diff > 0.8 | normalized_diff < -0.8);
% find the top peaks
[numMAX, indMAX] = findpeaks(TrapSepFiltClean);
% find the bottom peaks
TrapSepInverse = [-max(numMAX) -TrapSepFiltClean -max(numMAX)];
[numMIN, indMIN] = findpeaks(TrapSepInverse);
indMIN = indMIN-1;
% count number of raster scans
rasterNUM = (max([length(indMIN) length(indMAX)]))/2;
% get the force array
if trapData.scandir == 1
    FA = trapData.force_AX;
    FB = trapData.force_BX;
    FM = (trapData.force_AX+trapData.force_BX)./2;
    DNAext = trapData.DNAext_X;
elseif trapData.scandir == 0
    FA = trapData.force_AY;
    FB = trapData.force_BY;
    FM = (trapData.force_AY+trapData.force_BY)./2;
    DNAext = trapData.DNAext_Y;
end

% get fake offset
if ~isnan(fitrange)
    DNAext_offset = [];
    DNAext_offset = mean(DNAext(FM > fitrange(1) & FM < fitrange(2)));
    WLC_offset = mean(WLC_ext(F > fitrange(1) & F < fitrange(2)));
    fake_offset = DNAext_offset - WLC_offset;
    
    DNAext_offset2 = [];
    DNAext_offset2 = mean(DNAext(FM > fitrange(1) & FM < fitrange(2)));
    WLC_offset2 = mean(WLC_ext2(F > fitrange(1) & F < fitrange(2)));
    fake_offset2 = DNAext_offset - WLC_offset2;
    
    DNAext_offset3 = [];
    DNAext_offset3 = mean(DNAext(FM > fitrange(1) & FM < fitrange(2)));
    WLC_offset3 = mean(WLC_ext3(F > fitrange(1) & F < fitrange(2)));
    fake_offset3 = DNAext_offset - WLC_offset3;
    % substract the fake offset
%     DNAext = DNAext-(0);
    DNAext_raw = DNAext;
    DNAext = DNAext-fake_offset;
    DNAext2 = DNAext_raw-fake_offset2;
    DNAext3 = DNAext_raw-fake_offset3;
end


trapData.fake_offset = fake_offset;
trapData.fitrange = fitrange;
trapData.stdF = stdF;

%% plot
figure
plot(WLC_ext, F, '--r', 'LineWidth', 2)
hold on
plot(WLC_ext2, F, '--r', 'LineWidth', 2)
hold on
plot(WLC_ext3, F, '--r', 'LineWidth', 1)
hold on

for ii = 1:1:rasterNUM
    indxS = indMIN(ii); % far point
    indxE = indMAX(ii); % close point
    indxEE = indMIN(ii+1);
    hold on
    plot(DNAext(indxS:indxE),FM(indxS:indxE),'color', [0.9100    0.4100    0.1700], 'LineWidth', 1.5)
    hold on
    plot(DNAext(indxE:indxEE),FM(indxE:indxEE), 'color', [0 0.4470 0.7410], 'LineWidth', 1.5)
end

for ii = 1:1:rasterNUM
    indxS = indMIN(ii); % far point
    indxE = indMAX(ii); % close point
    indxEE = indMIN(ii+1);
    hold on
    plot(DNAext2(indxS:indxE),FM(indxS:indxE),'color', [0.9100    0.4100    0.1700], 'LineWidth', 1.5)
    hold on
    plot(DNAext2(indxE:indxEE),FM(indxE:indxEE), 'color', [0 0.4470 0.7410], 'LineWidth', 1.5)
end

for ii = 1:1:rasterNUM
    indxS = indMIN(ii); % far point
    indxE = indMAX(ii); % close point
    indxEE = indMIN(ii+1);
    hold on
    plot(DNAext_raw(indxS:indxE),FM(indxS:indxE),'color', [1 0 0], 'LineWidth', 1.5)
    hold on
    plot(DNAext_raw(indxE:indxEE),FM(indxE:indxEE), 'color', [0 0 1], 'LineWidth', 1.5)
end

for ii = 1:1:rasterNUM
    indxS = indMIN(ii); % far point
    indxE = indMAX(ii); % close point
    indxEE = indMIN(ii+1);
    hold on
    plot(DNAext3(indxS:indxE),FM(indxS:indxE),'color', [0.9100    0.4100    0.1700], 'LineWidth', 1.5)
    hold on
    plot(DNAext3(indxE:indxEE),FM(indxE:indxEE), 'color', [0 0.4470 0.7410], 'LineWidth', 1.5)
end



if ~isnan(DNAext)
    xlim([600 1500])
%     xlim([min(min(DNAext),min(DNAext_raw))-50 max(max(DNAext),max(DNAext_raw))+50])
end
ylim([min(0,min(FM)) max(FM)*1.2])
xlabel('DNA extension (nm)')
ylabel('Force (pN)')

switch construct
    case {'PCR+Linker'}
        legend('WLC 3.4kb model','WLC 4.3kb model','WLC 4.3kb double-tether','fit 3.4kb pull','fit 3.4kb restore','fit 4.3kb pull','fit 4.3kb restore','raw pull','raw restore','Location','NorthWest')
end

if trapData.oldtrap == 1
    instrument = 'Old-Trap';
elseif trapData.oldtrap == 0
    instrument = 'Fleezers';
end
% title({[ construct ' construct, ' instrument] ['data file: ' trapData.file(1:end-4)]})
title([ 'construct: ' construct ', ' instrument ', fext: ' trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ', fakeoffset: ' num2str(fake_offset) 'nm, fakeoffset2: ' num2str(fake_offset2) 'nm, cal: ' num2str(trapData.calnumber,'%03d')])
set(gcf, 'Name',[ 'Fext_' trapData.file(1:end-4)])

end