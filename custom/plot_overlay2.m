function plot_overlay2(directory, calparams,idx_array)
%overlay with the same origin

for idx = 1:length(idx_array)
    clear data
    data = load_trapData(directory,idx_array(idx,1),calparams,idx_array(idx,4),idx_array(idx,2),idx_array(idx,3));
    data = plot_TrapSep_bp_F_vs_Time(data,0);
    close(gcf);
    tmax = idx_array(idx,6); tmin = idx_array(idx,5);    
    crop = find(data.time >= tmin & data.time <=tmax);
    data.crop.time = data.time(crop);
    data.crop.time = data.crop.time - data.crop.time(1);
    data.crop.DNAbp = data.DNAbp(crop);
    data.crop.DNAbp = data.crop.DNAbp-data.crop.DNAbp(1)+3400;
    hold on
    plot(data.crop.time,data.crop.DNAbp,'Color',[0.85,0.325,0.098])
%     plot(data.crop.time,data.crop.DNAbp,'Parent',hg)%,'Color',[0      0.4470 0.7410],'Parent',hg) % blue
end
% legend(cellstr(num2str(idx_array(:,1:2))))
end

