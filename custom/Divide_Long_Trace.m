function  packcell_short  = Divide_Long_Trace( packcell )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
packcell_short = cell(1,length(packcell));
for i = 1 : length(packcell)
    initial = packcell{1,i}.length(1);
    ending = initial-3400;
    j=1;
    while j <= length(packcell{1,i}.length) && packcell{1,i}.length(j)>ending
        j = j+1;
    end    
    packcell_short{1,i}.length = packcell{1,i}.length(1:j-1);
    packcell_short{1,i}.time = packcell{1,i}.time(1:j-1);
    packcell_short{1,i}.force = packcell{1,i}.force(1:j-1);
end

end

