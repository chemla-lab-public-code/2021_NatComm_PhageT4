function y = SingleGaussian( param,x )

A1 = param(1);
x1 = param(2);
sig1 = param(3);

y = A1*exp(-(x-x1).^2/(2*sig1^2));
