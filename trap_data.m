%% Comments
% This code processes trap data.
% 02-04-2018
% DO NOT MODIFY!!
%
% Input variables:
%     Date: date at which the data was acquired, e.g. '170806'
%     calfnumber: number corresponding to the calibration file, e.g. 141
%     datafnumber: number corresponding to the trace file, e.g. 145
%     calData: data structure output from Calibrate.m
% Output structure array called traceData contains:
%     traceData.TrapSep: trap separation in nm (used conversion factors - see below)
%     traceData.A_X: QPD data in volts for bead A in x
%     traceData.B_X: QPD data in volts for bead B in x
%     traceData.A_Y: QPD data in volts for bead A in y
%     traceData.B_Y: QPD data in volts for bead B in y
%     traceData.A_Xnm: QPD data in nanometers for bead A in x
%     traceData.B_Xnm: QPD data in nanometers for bead B in x
%     traceData.A_Ynm: QPD data in nanometers for bead A in y
%     traceData.B_Ynm: QPD data in nanometers for bead B in y
%     traceData.DNAext_X: DNA extension in nanometers in x
%     traceData.DNAext_Y: DNA extension in nanometers in y
%     traceData.force_AX: force in picoNewtons for bead A in x
%     traceData.force_BX: force in picoNewtons for bead B in x
%     traceData.force_AY: force in picoNewtons for bead A in y
%     traceData.force_BY: force in picoNewtons for bead B in y
%     traceData.time: time in seconds
% The variables stored in structure array called traceData are used to
% measure trap separation. Note: To get trap separation on the old-trap,
% we use Pythagoras's Theorem. The AOM on the fleezers is setup in X only.
% The variables in traceData are defined here:
%     traceData.trappos1 - moving trap position in X
%     traceData.trappos2 - moving trap position in Y
%     traceData.t1x: start position trap 1 in x
%     traceData.t1y: start position trap 1 in y
%     traceData.t2x: start position trap 2 in x
%     traceData.t2y: start position trap 2 in y
%     traceData.t1y - t2y: distance in y
%     traceData.t1x - t2x: distance in x
% Conversion factors from voltage or frequency to nm, as of 02-04-2018:
%     flconv = 123: conversion factor in nm/MHz from grid measurements for fleezers
%     otconvX = 891.5: % conversion factor in nm/V from grid measurements for old-trap in X
%     otconvY = 505.7; % (nm/V) conversion factor in nm/V from grid measurements for old-trap in Y

%% Code
function [trapData] = trap_data(trapData, calData)


%% CONVERT QPD DATA FROM VOLTS TO NANOMETERS
trapData.A_Xnm = (trapData.A_X)*calData.alphaAX; % convert QPD data from volts to nm
trapData.B_Xnm = (-trapData.B_X)*calData.alphaBX; % convert QPD data from volts to nm
trapData.A_Ynm = (-trapData.A_Y)*calData.alphaAY; % convert QPD data from volts to nm
trapData.B_Ynm = (trapData.B_Y)*calData.alphaBY;  % convert QPD data from volts to nm

% DNA EXTENSION IN NANOMETERS
if trapData.scandir == 1
    trapData.DNAext_X = trapData.TrapSep_X - trapData.A_Xnm - trapData.B_Xnm - calData.beadA/2 - calData.beadB/2;
    trapData.DNAext_Y = trapData.TrapSep_Y - trapData.A_Ynm - trapData.B_Ynm;
elseif trapData.scandir == 0
    trapData.DNAext_X = trapData.TrapSep_X - trapData.A_Xnm - trapData.B_Xnm;
    trapData.DNAext_Y = trapData.TrapSep_Y - trapData.A_Ynm - trapData.B_Ynm - calData.beadA/2 - calData.beadB/2;
end
trapData.DNAext = sqrt(trapData.DNAext_X.^2 + trapData.DNAext_Y.^2);

% FORCE (pN)
trapData.force_AX = trapData.A_Xnm * calData.kappaAX; % multiply QPD data by trap stiffness
trapData.force_BX = trapData.B_Xnm * calData.kappaBX; % multiply QPD data by trap stiffness
trapData.force_AY = trapData.A_Ynm * calData.kappaAY; % multiply QPD data by trap stiffness
trapData.force_BY = trapData.B_Ynm * calData.kappaBY; % multiply QPD data by trap stiffness

% new code on 180808, while force 2 pN, it will not make much difference,
% but just in order to keep code in good shape, calculate them.
trapData.force_X = (trapData.force_AX + trapData.force_BX)/2;
trapData.force_Y = (trapData.force_AY + trapData.force_BY)/2;
trapData.force = sqrt(trapData.force_X.^2 + trapData.force_Y.^2);

end



