function [ XData,YData ] = SortXData( XData,YData )
% [XData,YData] = SortXData(XData,YData)
% It sort XData by increasing order, and sort the corresponding YData
l=length(XData);
if l~=length(YData)
    error('XData and YData does not share the same length')
end
for i = 1:l-1
    for j = (i+1):l
        if XData(j)<XData(i)
            tempX = XData(j);   tempY = YData(j);
            XData(j)=XData(i);  YData(j)=YData(i);
            XData(i)=tempX;     YData(i)=tempY;
        end
    end
end

