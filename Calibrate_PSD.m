% [calData] = Calibrate(datadirectory,Date,calarray,filenumber,showplots,writecal)
% [calData] = Calibrate(datadirectory,Date,calarray,filenumber)
% [calData] = Calibrate(datadirectory,Date,calarray,filenumber,showplots)

% This is the shared driver code for doing calibrations using either the
% old trap or the fleezers. DO NOT MODIFY THIS CODE!
%
% This function returns a structure variable 'calData' which contains:
%     - bead: bead diameters for bead A and B
%     - alpha: the V to nm conversion factor for each bead in X and Y
%     - kappa: trap stiffness for each bead in X and Y
%     - offset: QPD offset for each bead in X and in Y
% The input variables are:
%     - Date: the data at which the data was acquired, e.g. '171217'
%     - rootfile: file identifier, e.g. '171217_002.dat'
%     - calarray: the parameters to get the calibration file, e.g. calparams = [900    880     125000  20000    8000    1200     100];
%     - showplots: an option to plot the calibration file, 0 - do not plot, 1 - plot
%     - writecal: an option to save the calibration file, 0 - no not save, 1 - save
% 

% 190329 by Suoang: add more info to cal plot

function [calData] = Calibrate_PSD(directory,Date,calarray,calnumber,varargin)

global fbslash
ifmodify = 0;
fixfc = 0;

if nargin == 6 || nargin == 5 || nargin == 4
    p = inputParser;
    p.addRequired('directory', @(x)validateattributes(x, {'char'},{}));
    p.addRequired('Date', @(x)validateattributes(x, {'numeric'},{'positive','integer'}));
    p.addRequired('calarray', @(x)validateattributes(x, {'numeric'},{'size',[1,6]}));
    p.addRequired('calnumber', @(x)validateattributes(x, {'numeric'},{'integer'}));
    defaultshowplot = 1;
    p.addOptional('showplot', defaultshowplot, @(x)validateattributes(x, {'numeric'},{'integer'}));
    defaultwriteplot = 0;
    p.addOptional('writeplot', defaultwriteplot, @(x)validateattributes(x, {'numeric'},{'integer'}));
    % p.addRequired('offsetnumber',@(x)validateattributes(x,{'numeric'},{'>',-1}));
    p.parse(directory, Date, calarray, calnumber, varargin{:})
    directory = p.Results.directory;
    Date = p.Results.Date;
    calarray = p.Results.calarray;
    calnumber = p.Results.calnumber;
    showplot = p.Results.showplot;
    writeplot = p.Results.writeplot;
    
else
    info = getinput('Load Calibrate Parameters:');
    directory = info.path;
    Date = str2num(info.day);
    calnumber = str2num(info.cal);
    calarray = [str2num(info.params1),str2num(info.params2),str2num(info.params3),...
        str2num(info.params4),str2num(info.params5),str2num(info.params6),
        ];
    showplot = 1;
    writeplot = 0;
    validateattributes(directory,{'char'},{});
    validateattributes(Date,{'numeric'},{'positive','integer'});
    validateattributes(calarray,{'numeric'},{'size',[1,6]});
    validateattributes(calnumber,{'numeric'},{'integer'});
    validateattributes(showplot,{'numeric'},{'integer'});
    validateattributes(writeplot,{'numeric'},{'integer'});
    ifmodify = 1;
end

%common preprocessing
strDate = num2str(Date);
rootfile = [strDate '_' num2str(calnumber,'%03d') '.dat'];
startpath = [directory strDate fbslash];

% Change array of input parameters to structure variable (better for saving
% user calData later)
calpar.beadA = calarray(1);
calpar.beadB = calarray(1);
calpar.avwin = calarray(3);
calpar.f_xyhi = calarray(4);
calpar.f_sumhi = calarray(5);
calpar.f_low = calarray(6);

calData = ReadMattFile_Wrapper(startpath,rootfile);

%which signal sets to analyze
calsets(1) = 1;%Trap A
calsets(2) = 1;%Trap B

param(3) = calpar.avwin; % ave windows
param(5) = calpar.f_sumhi; % f sum high


if calsets(2) == 1 %get cal for trap 2 (B)
    param(1) = calpar.beadB; % set the same diameter rather than beadB

    param(4) = calpar.f_xyhi;% fxy high
    param(6) = calpar.f_low;% f low
    fitmodel = 'AliasedHydro';
    data_sub.sampperiod = calData.sampperiod;
    data_sub.X = calData.B_X;
    data_sub.Y = calData.B_Y;
    data_sub.Sum = calData.B_Sum;
    
    [cal,fit] = CalibrateExact_Stripped(param,fitmodel,startpath,rootfile,data_sub);
    
    % All "cal" fields are named as if they were bead A from the "CalibrateExact_Stripped" function, but they need to be renamed here, since this is actually the calibration for bead B.
    calData.beadB = param(1);
    calData.alphaBX = cal.alphaAX;
    calData.alphaBY = cal.alphaAY;
    calData.kappaBX = cal.kappaAX;
    calData.kappaBY = cal.kappaAY;
    calData.BXoffset = cal.AXoffset;
    calData.BYoffset = cal.AYoffset;
    
    allfit.fB = fit.f;
    allfit.BXSpecRaw = fit.AXSpecRaw;
    allfit.BYSpecRaw = fit.AYSpecRaw;
    allfit.predictedBX = fit.predictedAX;
    allfit.predictedBY = fit.predictedAY;
end

if calsets(1) == 1 %get cal for trap 1 (A)
    param(1) = calpar.beadA;
    param(4) = calpar.f_xyhi;% fxy high
    param(6) = calpar.f_low;% f low
    if fixfc
        fitmodel = 'fixedfc'; param(7) = fit.AXfc; param(8) = fit.AYfc;
    else
        fitmodel = 'AliasedHydro';
    end
    data_sub.sampperiod = calData.sampperiod;
    data_sub.X = calData.PSD_X;
    data_sub.Y = calData.PSD_Y;
    data_sub.Sum = calData.PSD_Sum;
    
    [cal,fit] = CalibrateExact_Stripped(param,fitmodel,startpath,rootfile,data_sub);
    
    calData.beadA = param(1);
    calData.alphaAX = cal.alphaAX;
    calData.alphaAY = cal.alphaAY;
    calData.kappaAX = cal.kappaAX;
    calData.kappaAY = cal.kappaAY;
    calData.AXoffset = cal.AXoffset;
    calData.AYoffset = cal.AYoffset;
    
    allfit.fA = fit.f;
    allfit.AXSpecRaw = fit.AXSpecRaw;
    allfit.AYSpecRaw = fit.AYSpecRaw;
    allfit.predictedAX = fit.predictedAX;
    allfit.predictedAY = fit.predictedAY;
end

% assignin('base',calfilename,calData);
% assignin('base',fitfilename,allfit);

%at some point before 190329 by Suoang: add infor for cal plot
calData.avwin = calarray(3);
calData.f_xyhi = calarray(4);
calData.f_sumhi = calarray(5);
calData.f_low = calarray(6);
calData.fitting = allfit;
calData = rmfield(calData,{'t3x','t3y','nchannels','chanpattern','datatype','extrasaved','flsaved','trpossaved'});

if showplot
    %190329 by Suoang: add infor for cal plot
    if calData.oldtrap == 1
            instrument = 'Old Trap ';
    elseif calData.oldtrap == 0
            instrument = 'Fleezers ';
    end
    
    columns = sum(calsets);
    
    
	figure
    set(gcf,'Name',['Cal_' rootfile(1:end-4)]);
    prev = 0;
    if calsets(1) == 1
        subplot(2,columns,1)
        loglog(allfit.fA, allfit.AXSpecRaw); hold on;
        loglog(allfit.fA, allfit.predictedAX, 'k');
        title(['PSDX: \kappa = ' num2str(calData.kappaAX,3) ', \alpha = ' ...
            num2str(calData.alphaAX,4) ', Offset = ' num2str(calData.AXoffset,3)]);
        % ylim([1e-9 5e-8])
        ylabel('Power (V^2 s)')
                
        % 190329 by Suoang: add more info to cal plot
        legendstring = [instrument ' ' calData.file(1:6) '\_' calData.file(8:10) ' Bead ' num2str(calData.beadA) 'nm'];
        legend(legendstring,'Location','SouthWest')
        
        subplot(2,columns,1+columns)
        loglog(allfit.fA, allfit.AYSpecRaw); hold on;
        loglog(allfit.fA, allfit.predictedAY, 'k');
        title(['PSDY: \kappa = ' num2str(calData.kappaAY,3) ', \alpha = ' ...
            num2str(calData.alphaAY,4) ', Offset = ' num2str(calData.AYoffset,3)]);
        % ylim([1e-9 5e-8])
        prev = 1;
        xlabel('Frequency (Hz)')
        ylabel('Power (V^2 s)')
        
        %190329 by Suoang: add infor for cal plot
        legendstring = ['avwin ' num2str(calData.avwin) ' fxyhi ' num2str(calData.f_xyhi) ' fsumhi ' num2str(calData.f_sumhi) ' flow ' num2str(calData.f_low)];
        legend(legendstring,'Location','SouthWest')
        

    end
    
    if calsets(2) == 1
        subplot(2,columns,prev+1)
        loglog(allfit.fB, allfit.BXSpecRaw); hold on;
        loglog(allfit.fB, allfit.predictedBX, 'k');
        title(['BY: \kappa = ' num2str(calData.kappaBX,3) ', \alpha = ' ...
            num2str(calData.alphaBX,4) ', Offset = ' num2str(calData.BXoffset,3)]);
        
        %190329 by Suoang: add infor for cal plot
        legendstring =  [' BeadA ' num2str(calData.beadA) 'nm, low/left(upstream) trap'];
        legend(legendstring,'Location','SouthWest')
        
        subplot(2,columns,prev+1+columns)
        loglog(allfit.fB, allfit.BYSpecRaw); hold on;
        loglog(allfit.fB, allfit.predictedBY, 'k');
        title(['BY: \kappa = ' num2str(calData.kappaBY,3) ', \alpha = ' ...
            num2str(calData.alphaBY,4) ', Offset = ' num2str(calData.BYoffset,3)]);
        prev = prev + 1;
        xlabel('Frequency (Hz)')
        
        %190329 by Suoang: add infor for cal plot
        legendstring = ['avwin ' num2str(calData.avwin) ' fxyhi ' num2str(calData.f_xyhi) ' fsumhi ' num2str(calData.f_sumhi) ' flow ' num2str(calData.f_low)];
        legend(legendstring,'Location','SouthWest')
       
    end
    
    ud.param = calpar;
    ud.fitted = calData;
    set(gcf, 'UserData', ud) % Save input parameters and fitted values into fig file
    
end

if writeplot % save figure in startpath/Date folder
    saveas(gcf,[startpath rootfile(1:(end-4)) '_cal.fig']);
end

if ifmodify == 1
    display(['Calibrate(datadirectory,' strDate ',calparams,' num2str(calnumber) ',' num2str(showplot) ',' num2str(writeplot) ');'])

end