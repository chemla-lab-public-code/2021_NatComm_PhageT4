function map_data = fplot_T4_map(directory, Date, calarray, datanumber, calnumber,offnumber)
%plot MCL map of APD 1
%note that for now, the code does not work if the fast scan direction is in
%y rather than x (091016, mjc)

%plot apd1 and apd2 with different units (210703 Suoang Lu)

% global datadirectories;
% global laptop;
FixCX=6.714; % um
Phage=0.12;% microns
laptop=0;
if laptop == 1
    figposition = [142          93        1350         885];
    figposition2 = [142          93        1350         885];
else
    figposition = [560    93   801   855];
    figposition2 = [564   145   801   358];
end

Avefactor = 1;
Savefigdir = [directory num2str(Date) '/'];
mclxcal = 1.077;%um/V   % once was 1.07
mclycal = 0.72;
startpath = [directory num2str(Date) '/'];
rootfile = [num2str(Date) '_' num2str(datanumber,'%03d') '.dat'];
datafilename = ['data_' rootfile(1:(end-4))];
% [startpath rootfile]
data = ReadMattFile_Wrapper(startpath,rootfile);


data = load_trapData(directory,Date,calarray,datanumber,calnumber,offnumber);
TrapSep=mean(data.TrapSep_X)/1000;
BeadLeft=calarray(2)/2000;
BeadRight=calarray(1)/2000;
Beads = BeadLeft+BeadRight;


if data.datatype == 3 %map
    %fix for older data sets where napdsampstep wasn't recorded and = 0
    if data.napdsampstep == 0
        data.napdsampstep = 1;
    end
    
    %if more than 1 sample per step, integrate
    if data.napdsampstep > 1
        data.apd1 = apd_integrate(data.apd1,data.napdsampstep);
        data.apd2 = apd_integrate(data.apd2,data.napdsampstep);
    end        

    if data.version <= 8
        scanlines = data.nsteps;
        length(data.apd1)
        if length(data.apd1) == data.nsteps^2*2
            ndim = 1;
        else
            ndim = 0;
        end
    else
        scanlines = data.nscanfb;
        ndim = data.scan2dor1d;
    end
         
    %scale data to be in Hz
    data.apd1 = data.apd1/(data.napdsampstep*(data.apdtime(2)-data.apdtime(1)));
    data.apd2 = data.apd2/(data.napdsampstep*(data.apdtime(2)-data.apdtime(1)));
    
   if ~any(data.apd1) && ~any(data.apd2) % If no APD data, don't plot a figure
       disp(['No APD data for ' rootfile])
   else
    vfig = figure;

    set(gcf,'FileName',[Savefigdir rootfile(1:10)]);
    set(gcf,'Name',[rootfile(1:10) ' APD map']);
    
    Vstep = data.scanrange/(data.nsteps-1);
    
    Vx = fliplr((0:data.nsteps-1)*Vstep + data.initialx);

    if ndim == 1    
        %image
            meanmap1 = reshape_mean_apdimages(data.apd1,scanlines,data.nsteps);
            meanmap2 = reshape_mean_apdimages(data.apd2,scanlines,data.nsteps);
          
            
            xscale = mclxcal;
            yscale = mclycal;
            
            
            Vy = fliplr((0:data.nscanfb-1)*Vstep + data.initialy);
            
            subplot(2,1,1)
            I = imagesc(meanmap1);
            ax=gca;
            
            map_data.image1 = meanmap1;
            map_data.image2 = meanmap2;
            map_data.xscale = Vx;
            map_data.yscale = Vy;
            set(I,'XData',Vx)
            set(I,'YData',Vy)
            xlim([min(Vx) max(Vx)])
            ylim([min(Vy) max(Vy)])
            %axis auto
            set(gca,'XDir','reverse')
            set(gca,'YDir','normal')
            xlabel('MCL X (V)')
            ylabel('MCL Y (V)')
            ax.DataAspectRatio = [1/xscale 1/yscale 1];
            colorbar('west')
            
%             ax2=axes();
%             imagesc(ax2,Vx*xscale,Vy*yscale,meanmap1,'AlphaData',0)
%             
%             xlim([min(Vx) max(Vx)]*xscale)
%             ylim([min(Vy) max(Vy)]*yscale)
%             %axis image
%             %colorbar
%             ax2.DataAspectRatio = [ 1 1 1];
%             ax2.XAxisLocation = 'top';
%             ax2.YAxisLocation = 'right';
%             set(gca,'XDir','reverse')
%             set(gca,'YDir','normal')
%             ax2.Color = 'none';
%             xlabel('MCL X (\mum)')
%             ax2.YTick=[];
%             
%             ax2.Position = ax.Position;
%             linkprop([ax, ax2],{'Units','Position','ActivePositionProperty'});
% 
%             
%             title(['APD 1 Image (' rootfile(1:6) '\_' rootfile(8:10) ')'])
%             set(gcf, 'CurrentAxes', ax);
%             
%             ax.Position
%             ax2.Position
%             ax.DataAspectRatio
%             ax2.DataAspectRatio
            
            ax2=axes();
            viscircles([FixCX mean(Vy)*yscale], 0.1*BeadLeft);
            viscircles([FixCX - mean(data.B_Xnm/1000), mean(Vy)*yscale],BeadLeft);
            viscircles([FixCX + mean(data.A_Xnm/1000) - TrapSep, mean(Vy)*yscale], BeadRight);
            viscircles([FixCX - TrapSep, mean(Vy)*yscale], 0.1*BeadRight);
            
        
            xlim([min(Vx) max(Vx)]*xscale)
            ylim([min(Vy) max(Vy)]*yscale)
            %axis image
            %colorbar
            ax2.DataAspectRatio = [ 1 1 1];
            ax2.XAxisLocation = 'top';
            ax2.YAxisLocation = 'right';
            set(gca,'XDir','reverse')
            set(gca,'YDir','normal')
            ax2.Color = 'none';
            xlabel('MCL X (\mum)')
            ax2.YTick=[];
            
            ax2.Position = ax.Position;
            linkprop([ax, ax2],{'Units','Position','ActivePositionProperty'});

            
            title(['APD 1 Image (' rootfile(1:6) '\_' rootfile(8:10) ')'])
            set(gcf, 'CurrentAxes', ax);
            
            
            
            subplot(2,1,2)
            I = imagesc(meanmap2);
            set(I,'XData',Vx)
            set(I,'YData',Vy)
            axis image
            set(gca,'XDir','reverse')
            set(gca,'YDir','normal')
            xlabel('MCL X (V)')
            ylabel('MCL Y (V)')
            title(['APD 2 Image (' rootfile(1:6) '\_' rootfile(8:10) ')'])
            colorbar
                     
    
    else    %linescan
        
        xscale = mclxcal;
        yscale = mclycal;
        
        line1 = data.apd1(1:data.nsteps);
        line2 = fliplr(data.apd1(data.nsteps+1:end));
        mline = (line1+line2)/2;
        fVx = fliplr(Vx);
        
        
           
        ax2=axes();
        %viscircles([FixCX - mean(data.B_Xnm/1000) 0],BeadLeft);
        %plot(fVx*xscale,mline,'Color',[1, 0, 0, 0.1])
        xlim([min(fVx) max(fVx)]*xscale)
        
        
        ax2.XAxisLocation = 'top';
        ax2.YAxisLocation = 'right';
        set(ax2,'XDir','reverse')
        ax2.YTick=[];
        ax2.Color = 'none';
        xlabel('MCL X (\mum)')
        title(['APD 1 Line (' rootfile(1:6) '\_' rootfile(8:10) ')'])
        
        ax=axes();
        plot(fVx,mline,'Color',[0,0.447,0.741])
        set(gca,'XDir','reverse')
        xlim([min(fVx) max(fVx)])
        xlabel('MCL X (V)')
        ylabel('APD Intensity (Hz)')
        ax.Color = 'none'; 
        ax.Box = 'off';
        ax2.Box = 'off';
        
        
        ax2.Position = ax.Position;
        linkprop([ax, ax2],{'Units','Position','ActivePositionProperty'});
        
        
        limits=ax.XLim(2)-ax.XLim(1);
        rectangle(ax2,'Position',[FixCX - mean(data.B_Xnm/1000) - BeadLeft, 0, Beads, limits],'Curvature',0.2,'LineWidth',2,'EdgeColor','r')
        rectangle(ax2,'Position',[FixCX - TrapSep + mean(data.A_Xnm/1000) - BeadLeft, 0, Beads, limits],'Curvature',0.2,'LineWidth',2,'EdgeColor','r')
        rectangle(ax2,'Position',[FixCX - mean(data.B_Xnm/1000) - BeadLeft - Phage 0.25*limits Phage 0.5*limits],'Curvature',0.2,'LineWidth',2,'EdgeColor','r')

        set(gcf, 'CurrentAxes', ax);
   end
   
   
   
datacursormode on
dcm_obj = datacursormode(vfig);
set(dcm_obj,'DisplayStyle','datatip','SnapToDataVertex','off','DisplayStyle','Window')

end


end