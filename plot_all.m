function plot_all(directory,Date,calarray,construct,fitrange,varargin)
global fbslash

p = inputParser;
p.addRequired('directory', @(x)validateattributes(x, {'char'},{}));
p.addRequired('Date', @(x)validateattributes(x, {'numeric'},{'positive','integer'}));
p.addRequired('calarray', @(x)validateattributes(x, {'numeric'},{'size',[1,6]}));
p.addRequired('construct', @(x)validateattributes(x, {'char'},{}));
p.addRequired('fitrange', @(x)validateattributes(x, {'numeric'},{'integer'}));
p.addOptional('filenum1',0,@(x)validateattributes(x,{'numeric'},{'integer'}));
p.addOptional('filenum2',999,@(x)validateattributes(x,{'numeric'},{'integer'}));
p.parse(directory,Date,calarray,construct,fitrange,varargin{:})
directory = p.Results.directory;
Date = p.Results.Date;
calarray = p.Results.calarray;
filenum1 = p.Results.filenum1;
filenum2 = p.Results.filenum2;


startpath = [directory num2str(Date) fbslash];
calnumber = nan;
for filenumber = filenum1:filenum2
    try
        RootFilename = [num2str(Date) '_' num2str(filenumber,'%03d') '.dat'];
        if exist([startpath RootFilename],'file')
            if filenumber ~= calnumber + 1
                Data = ReadMattFile_Wrapper(startpath,RootFilename);
                if ~isfield(Data, 'trappos1')
                    % this is calibration file
                    calnumber = filenumber;
                    display(['calnumber = ' num2str(calnumber)]);
                    Calibrate(directory,Date,calarray,calnumber,1,0);
                else
                    % this is offset or fext or fb
                    trapData = load_trapData(directory,Date,calarray,filenumber,calnumber);
                    display(trapData.file)
                    plot_force_extension(trapData,construct,fitrange);
                end
            end
        end
    catch
        fprintf('File %d failed\n',filenumber)
    end
    
end