function data = plot_SNAP(trapData)
timeresolution = 0.5;
smoothtrap = round(timeresolution/trapData.sampperiod);
smoothapd = round(timeresolution/trapData.flintperiod);
f = figure('Name', trapData.file(1:end-4));



directory = trapData.path(1:end-7);
Date = str2num(trapData.file(1:6));
stgTimeFilenum = str2num(trapData.file(8:10));
[trapData.path trapData.file(1:end-4) '_stagetime.txt'];
ifstage = exist([trapData.path trapData.file(1:end-4) '_stagetime.txt'],'file');
if ifstage
    [stgTime, stgPos] = StageMoveTime(directory, Date, stgTimeFilenum);
end



f1 = subplot(3,1,1);
plot(trapData.time, smooth(trapData.TrapSep,smoothtrap))
ylabel('TrapSep (nm)')
title(['Feedback\_'  trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' Trap Separation(nm) vs Time(s)']);
ylim([min(trapData.TrapSep)-50-max(trapData.TrapSep)+min(trapData.TrapSep) max(trapData.TrapSep)+50])

if ifstage
ylims = get(gca,'ylim');
for i = 2:length(stgTime)-1
    line([stgTime(i) stgTime(i)], ylim);
    text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
end
end

yyaxis right
plot(trapData.time, smooth(trapData.force_X,smoothtrap))
hold on
plot(trapData.time, smooth(trapData.force_Y,smoothtrap))
legend('TrapSep','force_X','force_Y')
ylabel('Force(pN)')
title(['Feedback\_'  trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' cal' num2str(trapData.calnumber) ' off' num2str(trapData.offsetnumber)]);
ylim([0 20]) 


f2 = subplot(3,1,2);

    [WLC_param] = WLC_parameters;
%   fake_offset = 280; % unit nm;
    if trapData.scandir == 1
        bp = 1*WLC_param.hds*XWLCContour(trapData.force_X,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
        nt = 1*WLC_param.hss*XWLCContour(trapData.force_X,WLC_param.Pss,WLC_param.Sss,WLC_param.kT); % nm/nt
        trapData.OligoExt = trapData.DNAext;% - (1555+1710)*bp - 19*nt; %3263?
    elseif trapData.scandir == 0 
%         bp = 1*WLC_param.hds*XWLCContour(trapData.force_Y,WLC_param.Pds,WLC_param.Sds,WLC_param.kT); % nm/bp
%         trapData.DNAext_Y = trapData.DNAext_Y - fake_offset;
%         trapData.DNAbp = trapData.DNAext_Y./bp;
    end
        
plot(trapData.time, smooth(trapData.OligoExt,smoothtrap))
% title(['Feedback\_'  trapData.file(1:end-8) '\' trapData.file(end-7:end-4) ' Oligo Extension(nm) vs Time(s)']);
ylabel('DNAExt (nm)')
if ifstage
ylims = get(gca,'ylim');
for i = 2:length(stgTime)-1
    line([stgTime(i) stgTime(i)], ylim);
    text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
end
end




f3 = subplot(3,1,3);

if isfield(trapData,'apd1')
    plot(trapData.apdtime,1*smooth(trapData.apd1,smoothapd)/trapData.flintperiod,'Color',[0.4660    0.6740    0.1880]);
    hold on
    plot(trapData.apdtime,1*smooth(trapData.apd2,smoothapd)/trapData.flintperiod,'Color',[0.850    0.3250    0.098]);
end
ylabel('Photon Rate (Hz)')

if ifstage
ylims = get(gca,'ylim');
for i = 2:length(stgTime)-1
    line([stgTime(i) stgTime(i)], ylim);
    text(stgTime(i), 0.5*(ylims(1)+ylims(2)), stgPos(i+1))
end
end


linkaxes([f1,f2,f3],'x')


end
