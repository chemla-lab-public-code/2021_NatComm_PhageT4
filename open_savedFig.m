function f = open_savedFig( directory,Date,filenum )
global fbslash
filename = [directory num2str(Date) fbslash num2str(Date) '_' num2str(filenum,'%03d') '_fext.fig' fbslash];
if exist(filename,'file')
    f = openfig(filename)
elseif exist(filename,'file')
    filename = [directory num2str(Date) fbslash num2str(Date) '_' num2str(filenum,'%03d') '_cal.fig' fbslash];
    f = openfig(filename)
else
    error('No Such Figure')
end
end

