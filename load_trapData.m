% trapData  = load_trapData(directory, Date, calarray, filenumber, calnumber, offsetnumber)

% -1  -> isnumeric isinteger ->  unknown but need to be known
% nan  -> isnumeric notinteger -> ignore offset
function traceData  = load_trapData(directory, Date, calarray, filenumber, calnumber, varargin)

global fbslash
ifmodify = 0;

if nargin == 6 || nargin == 5
    p = inputParser;
    p.addRequired('directory', @(x)validateattributes(x, {'char'},{}));
    p.addRequired('Date', @(x)validateattributes(x, {'numeric'},{'positive','integer'}));
    p.addRequired('calarray', @(x)validateattributes(x, {'numeric'},{'size',[1,6]}));
    p.addRequired('filenumber', @(x)validateattributes(x, {'numeric'},{'integer'}));
    p.addRequired('calnumber', @(x)validateattributes(x, {'numeric'},{'integer'}));
    defaultoffsetnumber = -1;
    p.addOptional('offsetnumber', defaultoffsetnumber, @(x)validateattributes(x, {'numeric'},{}));
    p.parse(directory, Date, calarray, filenumber, calnumber, varargin{:})
    directory = p.Results.directory;
    Date = p.Results.Date;
    calarray = p.Results.calarray;
    filenumber = p.Results.filenumber;
    calnumber = p.Results.calnumber;
    offsetnumber = p.Results.offsetnumber;
    
else
    info = getinput('Load Parameters:');
    directory = info.path;
    Date = str2num(info.day);
    filenumber = str2num(info.file);
    calnumber = str2num(info.cal);
    offsetnumber = str2num(info.offset);
    calarray = [str2num(info.params1),str2num(info.params2),str2num(info.params3),...
        str2num(info.params4), str2num(info.params5),str2num(info.params6)];
    validateattributes(directory,{'char'},{});
    validateattributes(Date,{'numeric'},{'positive','integer'});
    validateattributes(calarray,{'numeric'},{'size',[1,6]});
    validateattributes(filenumber,{'numeric'},{'integer'});
    validateattributes(calnumber,{'numeric'},{'integer'});
    validateattributes(offsetnumber,{'numeric'},{''});
    ifmodify = 1;
end

%common preprocessing
strDate = num2str(Date);
startpath = [directory strDate fbslash];

%% get calibration data
calFilename = [strDate '_' num2str(calnumber,'%03d') '.dat'];
if ~exist([startpath calFilename],'file')
    calnumber = GetCalNumber(directory, Date, filenumber);
    ifmodify = 1;
end
calData = Calibrate(directory, Date, calarray, calnumber, 0, 0);


%% get raw data
traceFilename = [strDate '_' num2str(filenumber,'%03d') '.dat'];
if ~exist([startpath traceFilename],'file')
    error(['Wrong File Path as a Trace: ' startpath traceFilename])
end
traceData = ReadJointFile(startpath,traceFilename);
traceData = trap_data_nmConversion(traceData);

traceData.calnumber = calnumber;
traceData.calarray = [calData.beadA calData.beadB calData.avwin calData.f_xyhi calData.f_sumhi calData.f_low];
%% get offset data if needed
traceData.offsetnumber = offsetnumber;
if ~isnan(offsetnumber)
    offsetFilename = [strDate '_' num2str(offsetnumber,'%03d') '.dat'];
    if ~exist([startpath offsetFilename],'file')
        ifmodify = 1;
        offsetnumber = calnumber + 1;
        offsetFilename = [strDate '_' num2str(offsetnumber,'%03d') '.dat'];
        if ~exist([startpath offsetFilename],'file')
            error('No Offset File Exists next to Calibrateion File.')
        end
    end
    offsetData = ReadJointFile(startpath, offsetFilename);
    offsetData = trap_data_nmConversion(offsetData); % convert from V or MHz to nm
    traceData = force_offset_subtraction(traceData, calData, offsetData);
else
    traceData = force_offset_subtraction(traceData, calData);
end

%% organize structure information
if ifmodify == 1
    display(['load_trapData(datadirectory, ' strDate ', calparams, ' num2str(filenumber) ', ' num2str(calnumber) ', ' num2str(offsetnumber) ');']);
end

% traceData = rmfield(traceData,{'t3x','t3y','nchannels','chanpattern','datatype','extrasaved','flsaved','trpossaved'});


end