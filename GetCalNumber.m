function get_calnum = GetCalNumber(directory, Date, filenumber)
global fbslash
datatype = -1;
calnumber = filenumber;
while datatype ~= 1 && calnumber >= -1
    calnumber = calnumber - 1;
    fid = fopen([directory num2str(Date) fbslash num2str(Date) '_' num2str(calnumber,'%03d') '.dat'],'r','ieee-be');
    fread(fid,6,'float64');
    datatype = fread(fid,1,'float64');
    fclose(fid);
end

get_calnum = calnumber;
end