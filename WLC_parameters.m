% Here are my WLC parameters
% values from Zhi's dissertation:
% persistence length for dsDNA, Pds = 50 nm, for ssDNA, Pss = 1 nm
% stretch modulus for dsDNA, Sds = 1000 pN, for ssDNA, Pss = 1000 pN
% contour length per basepair for dsDNA, hds = 0.34 nm/bp, for ssDNA, hss =
% 0.59 nm/bp

% Rep?2B project

function [WLC_param] = WLC_parameters
WLC_param.hds = 0.34; % [nm/bp] interphosphate distance 
WLC_param.hss = 0.59; % nm/nt
WLC_param.Pds = 53; % nm
WLC_param.Pss = 1; % nm
WLC_param.Sss = 1000; % pN
WLC_param.Sds = 1100; % pN
WLC_param.kT = 4.14;% Zhi's values (dsDNA)

%SSB35 
WLC_param.hds = 0.34; % [nm/bp] interphosphate distance 
WLC_param.hss = 0.59; % nm/nt
WLC_param.Pds = 53; % nm
WLC_param.Pss = 1.2; % nm
WLC_param.Sss = 1000; % pN
WLC_param.Sds = 1100; % pN
WLC_param.kT = 4.14;% Zhi's values (dsDNA)

% T4 parameters
WLC_param.hds = 0.34; % [nm/bp] interphosphate distance 
WLC_param.hss = 0.59; % nm/nt
WLC_param.Pds = 50; % nm
WLC_param.Pss = 1; % nm
WLC_param.Sss = 1000; % pN
WLC_param.Sds = 1000; % pN
WLC_param.kT = 4.14;% Zhi's values (dsDNA)


end
