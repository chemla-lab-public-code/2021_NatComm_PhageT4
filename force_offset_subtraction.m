function trapData = force_offset_subtraction(trapData,calData,varargin)
% trapData = force_offset_subtraction(trapData,calData,offsetData structure)
narginchk(2,3);

p = inputParser;
p.addRequired('trap_Data',@isstruct);

p.addRequired('cal_Data', @isstruct);

defaultoffsetData.path = 'none';
defaultoffsetData.file = '';
p.addOptional('offset_Data',defaultoffsetData,@isstruct);

p.parse(trapData,calData,varargin{:});
trapData = p.Results.trap_Data;
calData = p.Results.cal_Data;
offsetData = p.Results.offset_Data;

if ~exist([calData.path calData.file],'file') 
    display([calData.path calData.file])
    error('Calibration Data does not exist')
end

% QPD OFFSET SUBTRACTION
% trapData.A_X = trapData.A_X-calData.AXoffset; % both are in volts
% trapData.B_X = trapData.B_X-calData.BXoffset; % both are in volts
% trapData.A_Y = trapData.A_Y-calData.AYoffset; % both are in volts
% trapData.B_Y = trapData.B_Y-calData.BYoffset; % both are in volts

if ~exist('trapData.scandir','var')
    if trapData.t1x == trapData.t2x
        trapData.scandir = 0;
    elseif trapData.t1y == trapData.t2y
        trapData.scandir = 1;
    elseif abs(trapData.t1y - trapData.t2y) < 1
        trapData.scandir = 1;
        warning(['t1y =' num2str(trapData.t1y) 't2y' num2str(trapData.t2y)]);
    elseif abs(trapData.t1x - trapData.t2x) < 1
        trapData.scandir = 0;
        warning(['t1y =' num2str(trapData.t1y) 't2y' num2str(trapData.t2y)]);
    end    
end

if length(varargin) == 1
    % Sort offset
    
%     offsetData.A_X = offsetData.A_X-calData.AXoffset; % both are in volts
%     offsetData.B_X = offsetData.B_X-calData.BXoffset; % both are in volts
%     offsetData.A_Y = offsetData.A_Y-calData.AYoffset; % both are in volts
%     offsetData.B_Y = offsetData.B_Y-calData.BYoffset; % both are in volts

    if ~exist('offsetData.scandir','var')
        if trapData.t1x == trapData.t2x
            offsetData.scandir = 0;
        elseif trapData.t1y == trapData.t2y
            offsetData.scandir = 1;
        end
    end
    
    if trapData.scandir == offsetData.scandir
        if trapData.scandir == 1
            [s_extOffset, sid] = sort(offsetData.TrapSep);
            s_AOffset = offsetData.A_X(sid);
            s_BOffset = offsetData.B_X(sid);
        elseif trapData.scandir == 0
            [s_extOffset, sid] = sort(offsetData.TrapSep);
            s_AOffset = offsetData.A_Y(sid);
            s_BOffset = offsetData.B_Y(sid);
        end
        
        % Smooth offset
        Nfilt = 100;
%         Nfilt = round(1 / (trapData.sampperiod * 100)); % Divide by 10 to avoid oversmoothing
        ss_extOffset = filter(ones(1,Nfilt), Nfilt, s_extOffset);
        ss_AOffset = filter(ones(1,Nfilt), Nfilt, s_AOffset);
        ss_BOffset = filter(ones(1,Nfilt), Nfilt, s_BOffset);
        
        % Filter offset
        sss_extOffset = ss_extOffset(Nfilt:end);
        sss_AOffset = ss_AOffset(Nfilt:end);
        sss_BOffset = ss_BOffset(Nfilt:end);
        
        [ssss_extOffset, index] = unique(sss_extOffset);
        sss_AOffset = sss_AOffset(index);
        sss_BOffset = sss_BOffset(index);
        assignin('base','ssss_extOffset',ssss_extOffset);
        
        % Interpolate offset values corresponding to data values
        ssi_AOffset = interp1(ssss_extOffset, sss_AOffset, trapData.TrapSep,'linear','extrap');
        ssi_BOffset = interp1(ssss_extOffset, sss_BOffset, trapData.TrapSep,'linear','extrap');

%         assignin('base','ssi_AOffset',ssi_AOffset);
%         assignin('base','ssi_BOffset',ssi_BOffset);
        assignin('base','A_X',trapData.A_X);
        assignin('base','B_X',trapData.B_X);
        %     ssi_AOffset = interp1(ssss_extOffset, sss_AOffset, trapData.TrapSep,'linear','extrap');
        %     ssi_BOffset = interp1(ssss_extOffset, sss_BOffset, trapData.TrapSep,'linear','extrap');

%         figure()
%         plot(ssss_extOffset, sss_AOffset,ssss_extOffset, sss_BOffset)
%         hold on
%         plot(trapData.TrapSep, ssi_AOffset,trapData.TrapSep, ssi_BOffset)
%         hold on
%         plot(offsetData.TrapSep,offsetData.A_Y,offsetData.TrapSep,offsetData.B_Y)
%         legend('rawA','rawB','afterA','afterB','beforeA','beforeB')
    
        % Replace the raw values with force offset subtracted values
        if  trapData.scandir == 1
            trapData.A_X = trapData.A_X - ssi_AOffset;
            trapData.B_X = trapData.B_X - ssi_BOffset;
        elseif trapData.scandir == 0
            trapData.A_Y = trapData.A_Y - ssi_AOffset;
            trapData.B_Y = trapData.B_Y - ssi_BOffset;
        end
        
    else
        display('Warning: offset dir doesnt match trap data')
        display('no offset data should be subtracted')
    end
end

trapData = trap_data(trapData, calData);
end