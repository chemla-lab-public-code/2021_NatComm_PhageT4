% object = struct2class(structurename,classname);
function object = struct2class(structurename,classname)
   %converts structure s to an object of class classname.
   %assumes classname has a constructor which takes no arguments
   object = eval(classname);  %create object
   for fn = fieldnames(structurename)'    %enumerat fields
      try
          object.(fn{1}) = structurename.(fn{1});   %and copy
      catch
          warning('Could not copy field %s', fn{1});
      end
   end
end