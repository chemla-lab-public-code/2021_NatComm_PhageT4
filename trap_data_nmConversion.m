function trapData = trap_data_nmConversion(trapData)

flconv = 123; 
% otconvX = 891.5;% before 2019 spring
% otconvY = 505.7;% before 2019 spring

otconvX = 883.59; % after 2019 spring
otconvY = 508.93; % after 2019 spring

if trapData.oldtrap == 0
        trapData.TrapSep_X = (trapData.trappos2 - trapData.trappos1)*flconv; 
        trapData.TrapSep_Y = zeros(1,length(trapData.TrapSep_X)); % comment on 180808: it might be true.
        trapData.TrapSep = trapData.TrapSep_X; 
elseif trapData.oldtrap == 1
%         trapData.TrapSep = sqrt(((trapData.trappos1 - trapData.t2x)*otconvX).^2 + ((trapData.trappos2 - trapData.t2y)*otconvY).^2);
        trapData.TrapSep_X = (trapData.trappos1 - trapData.t2x)*otconvX;
        trapData.TrapSep_Y = (trapData.trappos2 - trapData.t2y)*otconvY;
        trapData.TrapSep = sqrt(trapData.TrapSep_X.^2+trapData.TrapSep_Y.^2);
end

end