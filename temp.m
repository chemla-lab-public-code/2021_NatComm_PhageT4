
%% 190422
filen = [103 117 131 149];
caln  = [098 112 129 141];
offn  = caln + 1;

for i = 1:length(filen)
    data = load_trapData(datadirectory,190422,calparams,filen(i),caln(i),offn(i));
%     plot_TrapSep_bp_F_vs_Time(data,'T4',0);
    plot_force_extension(data,'T4',[]);
end

%% 190423
filen = [150 156 172 181 185 204 212];
caln  = [144 154 167 177 183 202 210];
offn  = caln + 1;

for i = 1:length(filen)
    data = load_trapData(datadirectory,190423,calparams,filen(i),caln(i),offn(i));
%     plot_TrapSep_bp_F_vs_Time(data,'T4',0);
    plot_force_extension(data,'T4',[]);
end

%% 190427
filen = [6 16 25 36 44 048 57 68 79 85 95 105];
caln  = [4 14 20 31 42 047 54 62 75 81 90 102];
offn  = [5 15 21 32 43 nan 55 63 76 82 91 103];

for i = 1:length(filen)
    data = load_trapData(datadirectory,190427,calparams,filen(i),caln(i),offn(i));
%     plot_TrapSep_bp_F_vs_Time(data,'T4',0);
    plot_force_extension(data,'T4',[]);
end
